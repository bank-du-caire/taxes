package com.bdc.from41.core.enums;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum TransactionStatusEnum implements EnumClass<String> {

    SAVED("saved"),
    VALIDATED("Validated");

    private String id;

    TransactionStatusEnum(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static TransactionStatusEnum fromId(String id) {
        for (TransactionStatusEnum at : TransactionStatusEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}