package com.bdc.from41.core.enums;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum QuartersEnum implements EnumClass<Integer> {

    FRIST(1),
    SECOND(2),
    THIRD(3),
    FOURTH(4);

    private Integer id;

    QuartersEnum(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static QuartersEnum fromId(Integer id) {
        for (QuartersEnum at : QuartersEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}