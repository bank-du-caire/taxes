package com.bdc.from41.web.actions.investor;


import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.cuba.core.global.MetadataTools;
import com.haulmont.cuba.gui.ComponentsHelper;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.gui.components.ActionType;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.ListComponent;
import com.haulmont.cuba.gui.components.actions.BaseAction;
import com.haulmont.cuba.gui.components.actions.CreateAction;

import javax.inject.Inject;

@ActionType("openInvestorEditor")
public class OpenInevstorEditor extends CreateAction {

    @Inject
    private MetadataTools metadataTools;

    public OpenInevstorEditor(ListComponent target, WindowManager.OpenType openType, String id) {
        super(target, openType, id);
        setCaption("Open Investor Editor");
    }

    public OpenInevstorEditor(ListComponent target, WindowManager.OpenType openType) {
        super(target, openType);
        setCaption("Open Investor Editor");
    }

    public OpenInevstorEditor(ListComponent target) {
        super(target);
        setCaption("Open Investor Editor");

    }


    @Override
    public void actionPerform(Component component) {


            Notifications notifications = ComponentsHelper.getScreenContext(target).getNotifications();
            notifications.create()
                    .withType(Notifications.NotificationType.TRAY)
                    .withCaption("iam here ")
                    .show();

    }

}
