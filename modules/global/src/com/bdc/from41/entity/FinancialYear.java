package com.bdc.from41.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.*;
import java.util.Date;

@Table(name = "FINANCIAL_YEARS")
@Entity(name = "FinancialYear")
@NamePattern("%s|name")
public class FinancialYear extends BaseIntegerIdEntity {
    private static final long serialVersionUID = 8938242746390907857L;

    @Column(name = "FIN_YEAR_CODE")
    private String finYearCode;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ACTIVE", columnDefinition = "char default ('0')")
    private Boolean active;

    @Temporal(TemporalType.DATE)
    @Column(name = "FROM_DATE")
    private Date fromDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "TO_DATE")
    private Date toDate;

    public String getFinYearCode() {
        return finYearCode;
    }

    public void setFinYearCode(String finYearCode) {
        this.finYearCode = finYearCode;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getActive() {
        return active;
    }


    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}