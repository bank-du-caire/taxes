package com.bdc.from41.entity.embedded;

import com.haulmont.chile.core.annotations.MetaClass;
import com.haulmont.cuba.core.entity.EmbeddableEntity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

@MetaClass(name = "InvestorAnnualSummaryId")
@Embeddable
public class InvestorAnnualSummaryId extends EmbeddableEntity {
    private static final long serialVersionUID = 4293950914967063011L;

    @Column(name = "Fin_year_id")
    private Integer financialYearId;

    @Column(name = "investor_Id")
    private Integer investorId;

    @Override
    public int hashCode() {
        return Objects.hash(investorId, financialYearId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvestorAnnualSummaryId entity = (InvestorAnnualSummaryId) o;
        return Objects.equals(this.investorId, entity.investorId) &&
                Objects.equals(this.financialYearId, entity.financialYearId);
    }

    public Integer getInvestorId() {
        return investorId;
    }

    public void setInvestorId(Integer investorId) {
        this.investorId = investorId;
    }

    public Integer getFinancialYearId() {
        return financialYearId;
    }

    public void setFinancialYearId(Integer financialYearId) {
        this.financialYearId = financialYearId;
    }
}