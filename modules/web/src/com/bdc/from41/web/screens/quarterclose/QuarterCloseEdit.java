package com.bdc.from41.web.screens.quarterclose;

import com.bdc.from41.entity.FinancialYear;
import com.bdc.from41.core.enums.QuartersEnum;
import com.bdc.from41.entity.embedded.QuarterCloseId;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.QuarterClose;

import javax.inject.Inject;

@UiController("QuarterClose.edit")
@UiDescriptor("quarter-close-edit.xml")
@EditedEntityContainer("quarterCloseDc")
@LoadDataBeforeShow
public class QuarterCloseEdit extends StandardEditor<QuarterClose> {
    @Inject
    private DataContext dataContext;
    @Inject
    private LookupField<FinancialYear> financialYearField;
    @Inject
    private LookupField<QuartersEnum> quarterField;

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {
        QuarterCloseId quarterCloseId=new QuarterCloseId();
        quarterCloseId.setFinancialYearId(financialYearField.getValue().getId());
        quarterCloseId.setQuarterId(quarterField.getValue().getId());
        this.getEditedEntity().setId(quarterCloseId);
    }






}