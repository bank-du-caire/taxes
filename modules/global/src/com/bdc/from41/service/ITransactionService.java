package com.bdc.from41.service;

import com.bdc.from41.core.enums.UserTypeEnum;
import com.bdc.from41.entity.FinancialYear;
import com.bdc.from41.core.enums.TransactionStatusEnum;
import com.haulmont.cuba.core.global.ValueLoadContext;
import com.haulmont.cuba.core.global.View;

import java.math.BigDecimal;
import java.util.List;

public interface ITransactionService {
    String NAME = "TransactionService";




    List getSuggestedQuarters();


    FinancialYear getActiveFinancialYears();

    void restFinancialYears(int id);

    List getTransaction(int finYearId, int branchCodeId, int quarterId, TransactionStatusEnum statusId);


    BigDecimal getTotalTransactionsOFQuarter(int quarterId, int financialYearId);

    int validateQuartertransactions(int finId, int quarter, UserTypeEnum usertype, String branchCode, Long userAccountNumber);

    public BigDecimal getQuarterTransactionsBlance(int finId, int quarter, UserTypeEnum usertype, String branchCode, Long userAccountNumber);

    public void changeTransactionToValidatedStatus(int finId, int quarter, UserTypeEnum usertype, String branchCode, Long userAccountNumber);

      BigDecimal getQuarterGLBlance(int finId, int quarter, UserTypeEnum usertype, String branchCode, Long userAccountNumber);

    List getQuarterMonthes(int quarterId);

    List getTransactions(int finId, int quarter, TransactionStatusEnum status, UserTypeEnum usertype, String branchCode, Long userAccountNumber, View view);

    List getUserBranchTransaction(int finId, int quarter, TransactionStatusEnum status, String branchCode, View view);

    List getUserHeadTransaction(int finId, int quarter, TransactionStatusEnum status, Long userAccountNumber, View view);

    List getReportTransactions(int finId, int quarter, UserTypeEnum usertype, String branchCode, Long userAccountNumber, View view);

    List getInvestorCertficate(ValueLoadContext valueLoadContext, int financialYearId, String branchId, int investorId);
}