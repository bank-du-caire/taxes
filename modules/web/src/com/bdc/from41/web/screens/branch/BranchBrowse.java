package com.bdc.from41.web.screens.branch;

import com.haulmont.cuba.gui.components.Filter;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.Branch;

import javax.inject.Inject;

@UiController("Branch.browse")
@UiDescriptor("branch-browse.xml")
@LookupComponent("branchesTable")
@LoadDataBeforeShow
public class BranchBrowse extends StandardLookup<Branch> {
    @Inject
    private Filter filter;

    @Subscribe
    public void onInit(InitEvent event) {
        filter.createLayout();

    }

}