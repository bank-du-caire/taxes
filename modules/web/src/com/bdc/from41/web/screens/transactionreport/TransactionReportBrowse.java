package com.bdc.from41.web.screens.transactionreport;

import com.bdc.from41.core.enums.QuartersEnum;
import com.bdc.from41.entity.*;
import com.bdc.from41.service.ITransactionService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@UiController("TransactionReport.browse")
@UiDescriptor("transaction-report-browse.xml")
@LookupComponent("transactionReportsTable")
@LoadDataBeforeShow
public class TransactionReportBrowse extends StandardLookup<TransactionReport> {

    @Inject
    private Button searchBtn;
    @Inject
    private LookupField<QuartersEnum> quarterDDL;

    @Inject
    private CollectionContainer<TransactionReport> transactionReportsDc;
    @Inject
    private DataManager dataManager;

    @Inject
    private Notifications notifications;
    @Inject
    private Messages messages;
    @Inject
    private LookupField<FinancialYear> financialYearDDL;
    @Inject
    private Button excelBtn;
    @Inject
    private CollectionLoader<TransactionReport> transactionReportsDl;
    @Inject
    private ITransactionService iTransactionService;


    private TaxesUser user;
    @Inject
    private UserSession userSession;


    @Subscribe
    public void onInit(InitEvent event) {

          user = (TaxesUser)  userSession.getUser();
        transactionReportsDc.getMutableItems().clear();
    }

    @Install(to = "transactionReportsDl", target = Target.DATA_LOADER)
    private List<TransactionReport> transactionReportsDlLoadDelegate(LoadContext<TransactionReport> loadContext) {
        transactionReportsDl.setMaxResults(50);
        LoadContext.Query query = loadContext.getQuery();
        Optional<FinancialYear> finOpt = Optional.ofNullable(financialYearDDL.getValue());
        Optional<QuartersEnum> quarterOpt = Optional.ofNullable(quarterDDL.getValue());

        if (quarterOpt.isPresent() && finOpt.isPresent()) {
            query.setQueryString("select e from TransactionReport e where e.quarterNum = :quarter and e.financialYearId = :finId  and e.branchCode = :branch ")
                    .setParameter("finId", finOpt.get().getId())
                    .setParameter("quarter",   quarterOpt.get().getId() )
                    .setParameter("branch",this.user.getBranch().getId());
                //    .setParameter("status", TransactionStatusEnum.SAVED.getId());

            List result= dataManager.loadList(loadContext);
            if(result.size()==0)
                notifications.create().withType(Notifications.NotificationType.WARNING).withCaption(messages.getMainMessage("msg.noResult")).show();
            return result;
        }

        return new ArrayList<>();
    }







    public void onSearchBtnClick() {

        Optional<QuartersEnum> quarterOptional = Optional.ofNullable(quarterDDL.getValue());
        Optional<FinancialYear> financialYearOptional = Optional.ofNullable(financialYearDDL.getValue());
        if (!quarterOptional.isPresent()) {
            notifications.create().withCaption(messages.getMainMessage("msg.error.quarter.required")).withHideDelayMs(100).show();
            return;
        }

        if (!financialYearOptional.isPresent()) {
            notifications.create().withCaption(messages.getMainMessage("msg.error.financialYear.required")).withHideDelayMs(100).show();

            return;
        }


        enableSearchForm(false);
        transactionReportsDl.load();

    }

    public void OnCancelBtnClick() {
        enableSearchForm(true);
    }

    private void enableSearchForm(boolean flag) {
        quarterDDL.setEnabled(flag);
         financialYearDDL.setEnabled(flag);
        searchBtn.setEnabled(flag);
        excelBtn.setEnabled(!flag);


    }
}