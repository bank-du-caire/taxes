package com.bdc.from41.entity.embedded;

import com.haulmont.chile.core.annotations.MetaClass;
import com.haulmont.cuba.core.entity.EmbeddableEntity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@MetaClass(name = "QuarterCloseId")
@Embeddable
public class QuarterCloseId extends EmbeddableEntity {
    private static final long serialVersionUID = -2716138254520704509L;


    @Column(name = "FINANCIAL_YEAR_ID", nullable = false)
    @NotNull
    private Integer financialYearId;


    @Column(name = "QUARTER_ID", nullable = false)
    @NotNull
    private Integer quarterId;

    public void setQuarterId(Integer quarterId) {
        this.quarterId = quarterId;
    }

    public void setFinancialYearId(Integer financialYearId) {
        this.financialYearId = financialYearId;
    }

    public Integer getFinancialYearId() {
        return financialYearId;
    }

    public Integer getQuarter() {
        return quarterId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(financialYearId, quarterId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuarterCloseId entity = (QuarterCloseId) o;
        return Objects.equals(this.financialYearId, entity.financialYearId) &&
                Objects.equals(this.quarterId, entity.quarterId);
    }


}