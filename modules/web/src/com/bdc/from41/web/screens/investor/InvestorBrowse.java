package com.bdc.from41.web.screens.investor;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.Investor;
import org.slf4j.Logger;

import javax.inject.Inject;

@UiController("from41_Investor.browse")
@UiDescriptor("investor-browse.xml")
@LookupComponent("investorsTable")
@LoadDataBeforeShow
public class InvestorBrowse extends StandardLookup<Investor> {


    @Inject
    private DataManager dataManager;
    @Inject
    private Logger log;

    @Subscribe
    public void onInit(InitEvent event) {

    }


}