package com.bdc.from41.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseStringIdEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Table(name = "BDC_BRANCHES")
@Entity(name = "Branch")
@NamePattern("%s %s|code,branchLocation")

public class Branch extends BaseStringIdEntity {
    private static final long serialVersionUID = -4591801954925520119L;

    @Id
    @Column(name = "BRANCH_CODE", nullable = false, unique = true, length = 15)
    @NotNull
    private String id;

    @Column(name = "BRANCH_CODE", insertable = false, updatable = false)
    @NotNull
    private String code;

    @Column(name = "BRANCH_LOCATION", length = 1024)
    private String branchLocation;

    @Column(name = "BRANCH_TYPE")
    private String branchType;

    @Column(name = "BRANCH_ADDRESS", length = 1024)
    private String branchAddress;

    @Column(name = "BRANCH_ADDRESS2", length = 1024)
    private String branchAddress2;

    @Column(name = "BRANCH_ADDRESS3", length = 1024)
    private String branchAddress3;

    @Column(name = "BRANCH_STATUS")
    private String branchStatus;

    @Column(name = "BRANCH_COUNTRY")
    private String branchCountry;

    @ManyToOne
    @JoinColumn(name = "GL_ACCOUNT_ID")
    private GLAccount glAccount;


    public void setCode(String code) {
        this.code = code;
    }

    public void setBranchLocation(String branchLocation) {
        this.branchLocation = branchLocation;
    }

    public void setBranchType(String branchType) {
        this.branchType = branchType;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public void setBranchAddress2(String branchAddress2) {
        this.branchAddress2 = branchAddress2;
    }

    public void setBranchAddress3(String branchAddress3) {
        this.branchAddress3 = branchAddress3;
    }

    public void setBranchStatus(String branchStatus) {
        this.branchStatus = branchStatus;
    }

    public GLAccount getGlAccount() {
        return glAccount;
    }

    public void setGlAccount(GLAccount glAccount) {
        this.glAccount = glAccount;
    }

    public String getBranchCountry() {
        return branchCountry;
    }

    public void setBranchCountry(String branchCountry) {
        this.branchCountry = branchCountry;
    }


    public String getBranchStatus() {
        return branchStatus;
    }

    public String getBranchAddress3() {
        return branchAddress3;
    }

    public String getBranchAddress2() {
        return branchAddress2;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public String getBranchType() {
        return branchType;
    }

    public String getBranchLocation() {
        return branchLocation;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id=id;
    }
}