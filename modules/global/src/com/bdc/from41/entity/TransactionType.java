package com.bdc.from41.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "TRANSACTION_TYPE")
@Entity(name = "from41_TransactionType")
@NamePattern("%s|name")
public class TransactionType extends BaseIntegerIdEntity {
    private static final long serialVersionUID = 374617596774350909L;

    @Column(name = "CODE")
    private String code;

    @Column(name = "MINMUM_AMOUNT")
    private Double minmumAmount;

    @Column(name = "NAME")
    private String name;

    @Column(name = "COMMISSION")
    private Double commission;

    public Double getMinmumAmount() {
        return minmumAmount;
    }

    public void setMinmumAmount(Double minmumAmount) {
        this.minmumAmount = minmumAmount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}