package com.bdc.from41.web.screens.investor;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.PersistenceHelper;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.Investor;

import javax.inject.Inject;
import java.util.List;

@UiController("from41_Investor.edit")
@UiDescriptor("investor-edit.xml")
@EditedEntityContainer("investorDc")
@LoadDataBeforeShow
public class InvestorEdit extends StandardEditor<Investor> {
    @Inject
    private DataManager dataManager;
    @Inject
    private Notifications notifications;
    @Inject
    private Messages messages;

    @Subscribe("nameField")
    public void onNameFieldValueChange(HasValue.ValueChangeEvent<String> event) {
        if (PersistenceHelper.isNew(this.getEditedEntity()))
        {
        try{

         List result=   dataManager.load(Investor.class).query("e.name like ?1",this.getEditedEntity().getName()).list();

           if(result!=null &&result.size()>0) {
               notifications.create().withCaption(messages.getMainMessage("err.investor.name.exist")).withType(Notifications.NotificationType.WARNING).show();
           event.getSource().clear();
           }

        }catch (Exception e){


        }

        }







    }

    @Subscribe("nationalIdField")
    public void onNationalIdFieldValueChange(HasValue.ValueChangeEvent<Long> event) {
        {
            if (PersistenceHelper.isNew(this.getEditedEntity()))
            {
                try{

                    List result=   dataManager.load(Investor.class).query("e.nationalId = ?1",this.getEditedEntity().getNationalId()).list();

                    if(result!=null &&result.size()>0) {
                        notifications.create().withCaption(messages.getMainMessage("err.investor.nationalId.exist")).withType(Notifications.NotificationType.WARNING).show();
                        event.getSource().clear();
                    }

                }catch (Exception e){


                }

            }







        }
    }

    @Subscribe("taxesNumberField")
    public void onTaxesNumberFieldValueChange(HasValue.ValueChangeEvent<Integer> event) {
        {
            if (PersistenceHelper.isNew(this.getEditedEntity()))
            {
                try{

                    List result=   dataManager.load(Investor.class).query("e.taxesNumber = ?1",this.getEditedEntity().getTaxesNumber()).list();

                    if(result!=null &&result.size()>0) {
                        notifications.create().withCaption(messages.getMainMessage("err.investor.taxesNumber.exist")).withType(Notifications.NotificationType.WARNING).show();
                        event.getSource().clear();
                    }

                }catch (Exception e){


                }

            }







        }
    }













}