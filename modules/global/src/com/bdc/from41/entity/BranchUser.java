package com.bdc.from41.entity;

import com.bdc.from41.entity.embedded.BranchUsersId;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseGenericIdEntity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "branches_accounts")
@Entity(name = "BranchAccount")
@NamePattern("%s %s|accountNO,userName")
public class BranchUser extends BaseGenericIdEntity<BranchUsersId> {
    private static final long serialVersionUID = 8540445256765132838L;

    @EmbeddedId
    private BranchUsersId id;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "ACCOUNT_NO")
    private String accountNO;

    public String getAccountNO() {
        return accountNO;
    }

    public void setAccountNO(String accountNO) {
        this.accountNO = accountNO;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    @Override
    public void setId(BranchUsersId id) {
        this.id = id;
    }

    @Override
    public BranchUsersId getId() {
        return id;
    }
}