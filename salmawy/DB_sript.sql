SET DEFINE OFF;
Insert into TRANSACTION_TYPE
   (ID, CODE, MINMUM_AMOUNT, NAME, COMMISSION)
 Values
   (102, '4', 301, 'خدمات', 0.03);
Insert into TRANSACTION_TYPE
   (ID, CODE, MINMUM_AMOUNT, NAME, COMMISSION)
 Values
   (103, '14', 100, 'اتعاب مهنية و سمسرة', 0.05);
Insert into TRANSACTION_TYPE
   (ID, CODE, MINMUM_AMOUNT, NAME, COMMISSION)
 Values
   (101, '1', 300, 'توريدات', 0.01);
COMMIT;


SET DEFINE OFF;
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8813, 0, 'دانياميك انترناشيونال ', '474353559');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8814, 0, 'حماية -HEMAYA', '467608466');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8815, 0, 'بي كول للتوريداتوالمقاولت الكهرومائية', '552306592');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8816, 0, 'حمدي كرم حسن كرم', '231788762');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8817, 0, 'شركه سيارات توفيق محمد', '217670695');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8818, 0, 'تريد فيرزا انترناشيونالللمعارض', '200167189');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8819, 0, 'هشام يحيي مرزوق مراد', '220016968');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8820, 0, 'abo auf', '349841675');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8821, 0, 'مركز الرحاب للاشعه المقطعية و الرنين المغناطيسي', '474190773');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8720, 0, 'شركة رؤية للتجارة ', '440670470');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8721, 0, 'اشرف احمد عرفه وشركاه', '217210783');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8722, 0, 'الشركة الاوروبية التجارية اوتوموتيف', '463384490');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8723, 0, 'اسلام سامي محمد السيد', '455453586');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8724, 0, 'Sport X Global ', '670303259');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8725, 0, 'محسن عبدالمنعم حسنعبدالرحمن', '726547662');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8726, 0, 'المصريه للتسويق والتوزيع', '200244418');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8727, 0, 'شركة النصر لصناعة الكرتون', '100321135');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8728, 0, 'جابر اسماعيل عبدالرحمنفراج', '494335963');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8729, 0, 'د / خالد محى الدين محمد عامر الكورانى', '452786045');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8730, 0, 'سامي اسحق ارمانيوس صالح', '210925183');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8731, 0, 'عمر فايز حسن ابراهيموشريكه', '674343794');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8732, 0, ' زكتروكس ', '310627842');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8733, 0, 'daghash dome', '411029592');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8734, 0, 'المصريه للاستعلام الائتماني', '220726213');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8735, 0, 'د / طارق عبدالمنعم هاشم', '437941264');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8736, 0, 'شركه سفنكس للدارهوالخدمات', '545802954');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8737, 0, 'ابو ذكرى للنقل - محمد ذكرياعبد الحميد', '379609533');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8738, 0, 'شركة انتجريتد كرييتف سوليوشنز', '455249709');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8739, 0, 'المصرية للإتصالات ', '100292895');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8740, 0, 'أوتو زد جروب Auto Z Group', '731528417');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8741, 0, 'شركة تراست موتورز       ', '218136544');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8742, 0, 'الشركه المصريه العربيهللطباعه والنشروالتوزيعوالصحافه', '416290302');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8743, 0, 'شركه ايفنتور لتنظيمالمعارض والمؤتمرات', '728559110');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8744, 0, 'المجموعة العربية لادارة المنشات اجيس', '210447842');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8745, 0, 'احمد شوقي احمد كلديشوشركاه', '679664505');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8746, 0, 'الغزال لصناعه الاظرفمحمد الباقر سيف الدينوشركاه', '205095194');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8747, 0, 'هبه صبرى محمد مرسى', '237370468');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8748, 0, 'الماسة لتأجير و معالجة المياه', '200162825');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8749, 0, 'شركة المنير للخدمات الطبية ', '533407710');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8750, 0, 'خالد عبد العظيم بيومي', '335212174');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8751, 0, 'المؤسة العربية للتجارة والتوريد ', '410495964');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8752, 0, 'سيد ابراهيم فرج', '415579163');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8753, 0, 'المركز الدولي للعلاج الطبيعي', '204930006');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8754, 0, 'م حلمي طلعت سليمان', '219861765');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8755, 0, 'امين عبدالحميد عبدالهادي', '324910258');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8756, 0, 'علامنا للحلول المتكاملة ', '371330100');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8757, 0, 'حازم عطيه ابراهيم معطيه', '374233586');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8758, 0, 'يونيلكس للنظم الارشاديه', '200172018');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8759, 0, 'وليد احمد عزت وشركاه', '229609376');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8760, 0, 'ايه سي جي اي تي افللمعارض الدوليه المتخصصه', '308314506');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8761, 0, 'رامي غطاس صليب وشركاه', '210436352');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8762, 0, 'دعاء قنديل محمود حسين', '313422796');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8763, 0, 'ارامكس انترناشيونال للخدمات الجوية', '200157264');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8764, 0, 'فالكون لخدمات نقل الاموال ', '350042608');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8765, 0, 'مركز الزاهد للعلاج الطبيعي', '219783225');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8766, 0, 'بنتاميد مصر للخدمات الطبية واداره المستشفيات ( الجولف الدولي)', '540876976');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8767, 0, 'المروة', '200014846');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8768, 0, 'Banlastic', '584791003');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8769, 0, 'الشركة المصرية للانظمة المتطورة ', '200227777');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8770, 0, 'مكة لتوريدات', '273813188');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8771, 0, 'جلوبال ابريزال تك للتقييم العقارى', '350010420');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8772, 0, 'محمد هنداوي ابراهيم عليعطاالله', '415843685');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8773, 0, 'الشركة المصرية لخدمات العلاج الطبيعى', '731383508');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8774, 0, 'مجدي نابيل رزق و شركاهامريكان هوم', '291062199');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8775, 0, 'مينا اسحاق عجيب - مؤسسهرسنز', '540413844');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8776, 0, 'الشرق الوسط لخدماتتكنولوجيا المعلومات', '220362394');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8777, 0, 'سوزان بطرس جودة سوريال ( لوك اب )', '249740567');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8778, 0, 'جون مينا سامي مسيحهوشركاه', '279307748');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8779, 0, 'شركة سلوجان ايجيبت كارد', '411100920');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8780, 0, 'بلتون للوساطه في السندات', '276336739');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8781, 0, 'د- محمد عاطف عبد الله الصفناوي (سيرين لايف)', '254813658');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8782, 0, 'احمد محمود عبدالوهاب احمدوشريكته', '254879136');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8783, 0, 'ماجد عبدالمقصود عبدالحميد باشا - مركز الشروق للعلاج الطبيعى', '379620197');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8784, 0, 'مؤسسة الازهرى التجارية', '244063192');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8785, 0, 'ممدوح السيد جمال الدينالبهائي', '385980477');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8786, 0, ' كوبر ميلت للاغذية ', '516115634');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8787, 0, 'ممدوح سالم مأمون شركه الكرنك لتجاة السيارات ', '645339172');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8788, 0, 'محمد ربيع محمود محمدخاطر وشركاه', '217856144');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8789, 0, 'علاء محمد احمد عثمان', '266032893');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8790, 0, 'TECHNOLOGIES SHARE ', '376998326');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8791, 0, 'ايهاب احمد محمد عبدالرحمنوشريكته برفيو', '256728860');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8792, 0, 'الشركة المصرية للاتصالات', '222588436');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8793, 0, 'مروه محمد محمود عيد', '374328862');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8794, 0, 'يحي زكريا عبدالحميدوشركاه', '244618194');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8795, 0, 'سيد علي حسن جوده', '374723206');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8796, 0, 'مجموعة المهندسين المتحدين', '200007599');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8797, 0, 'محمد حامد علي عبدالصادقشهاب الدين وشركاه', '301432430');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8798, 0, 'مستشفى الدكتور عثمان ', '200006568');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8799, 0, 'جامعة 6اكتوبر', '200221469');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8800, 0, 'شركه الفا لاب مصر', '204936624');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8801, 0, 'شركــــــــــه الزمــــــــــــرسيرفيس', '850179471');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8802, 0, 'د - شريف أحمد فرغلى الغزالى', '257004807');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8803, 0, 'المصري اليوم للخدمات الاعلانية  My Media', '721069053');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8804, 0, ' شركة نيولاند للاعمال الزراعية و اللاندسكيب ', '369687582');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8805, 0, 'شركه المشهد للصحافهوالطباعه والنشر', '374071705');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8806, 0, 'عصام جلال محمد عمر', '259764086');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8807, 0, 'تى كمبيوترز', '100284434');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8808, 0, 'معمل ساريدار', '205138039');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8809, 0, 'ad republic', '452615178');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8810, 0, 'INFO TECH SYSTEMS', '535292684');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8811, 0, 'مستشفي جراحة المخ و الاعصاب و العمود الفقري و الجراحات المتخصصة (نيوروسباين)', '371731305');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8812, 0, 'مستشفي العين محمود عليراجح وشركاه', '308494202');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8623, 0, 'سامية مصيلحي ', '713623179');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8624, 0, 'شركة ايفنتور لتنظيم المعارض ', '768559110');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8625, 0, 'الشركة الدولية للخدمات البريد -ايجي سيرف', '202698610');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8626, 0, 'بست باي لنظم المعلومات ', '220631298');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8627, 0, 'محسن الفيومي', '100298117');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8628, 0, 'ليلي عزيز كيرلس بباوي', '288222903');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8629, 0, 'سفنكس سكويرهوتيل لدارهالفنادق', '252094352');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8630, 0, 'سوزان محمد حامد الغوابي', '335585388');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8631, 0, 'وائل عبدالله محمد عليصابر', '691686769');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8632, 0, 'زكريا علي الجوهري', '410504513');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8633, 0, 'امبرايال ترافيل سنتر - Imperial travel center', '262273772');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8634, 0, 'احمد علي ابراهيم البسيوني', '288920716');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8635, 0, 'بزنس ميديا جروب للدعايهوالاعلان', '320685136');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8636, 0, 'محمد عبدالسلام سعد حسين الخيال', '596738307');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8637, 0, 'شركه مراكز امراض الكبدفيبرو سينس', '428538010');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8638, 0, 'جورج متى بشاى و شركاه', '200112678');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8639, 0, 'شركه الصباح للصحافه', '376774959');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8640, 0, 'مطبعة التحرير', '217869866');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8641, 0, 'مرام رمضان رياض ابراهيم ( انزيت Inzet )', '721495133');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8642, 0, ' البردي  لصناعة الورق  ', '100082394');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8643, 0, 'حسن مصطفى حسن صالح القرش', '381802833');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8644, 0, 'عبدالعاطي محمد احمد السيد', '335165605');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8645, 0, 'محمود سيد حسن المعايرجيوشريكته', '100353665');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8646, 0, 'اكرامى موتورز', '320184838');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8647, 0, 'الرواد للاستثمار والهندسه', '373851863');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8648, 0, 'مكتب حجازى', '328272043');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8649, 0, 'Dino', '333017684');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8650, 0, 'نيفين ياسين احمد عبداللطيف', '303341467');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8651, 0, 'وفيق وصفي حبيب وشركاه', '410837687');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8652, 0, 'الصفوة للمقاولات و نظم التكييف و التبريد ', '416345463');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8653, 0, 'شركة معامل البرج - البرج سكان ', '200015818');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8654, 0, 'المركز الطبي للجراحاتالمتخصصه والدقيقه الكوكب', '204954797');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8655, 0, 'ميديل إيست ', '376364122');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8656, 0, 'مستشفي برج الاطباءالتخصصي', '205086217');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8657, 0, 'هدي رأفت محمد عطيه (مجله عروبه مصر )', '531110745');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8658, 0, 'هناء عبدالحسيب ابوالمجدناجي ) الوطنية القتصادية', '511268823');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8659, 0, 'د / شريف أحمد محمد محمد بحر', '242711316');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8660, 0, 'شركة عالم المال للصحافة والطباعة والنشر', '298925680');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8661, 0, 'أحمد عبدالمطلب على ابوأحمد وشركاه - الأركان للتجارة والمقاولات', '100523595');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8662, 0, 'نيو ميجا للمقاولات العامهوالتوريدات / محمد مصطفي', '411599747');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8663, 0, 'معرض سيارات أحمد عبد العاطى   ', '354543563');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8664, 0, 'شركة المتحدة لتصنيع لافتات الاعلانية', '205101992');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8665, 0, 'معرض الفتح لتجاره السيارات    ', '506157849');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8666, 0, ' شلاق الدلتا للمقاولات ', '274442868');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8667, 0, 'جوبيتر كومز للاستشاراتالاعلانيه والتسويقيه', '547541074');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8668, 0, ' العلا للنظم الحديثة ', '100331874');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8669, 0, 'مستشفى الأمل والحياة', '301410593');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8670, 0, 'انترسكت', '274184133');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8671, 0, 'ياسر صالح عبدالمجيد حسن', '205426433');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8672, 0, 'جلوبال كير للخجمات الطبية ', '474792056');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8673, 0, 'هشام حسن م فوده', '462042472');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8674, 0, 'الانتماء المصرى ', '498799603');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8675, 0, 'مانسكو اوتوموتيف', '721293654');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8676, 0, 'سعيد عبدالحفيظ محمدالشريف وشركاه', '337748969');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8677, 0, 'مركز علاجات القلبوالاوعيه الدمويه هارت كيرسنتر', '230022847');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8678, 0, 'RAYA ELECTRONICS', '200196375');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8679, 0, 'د / مصطفى احمد محمود همام', '220094918');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8680, 0, 'محمد كمال وموريس رستموشركاهم', '713426357');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8681, 0, 'السيد محمد السيد عمر', '240035186');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8682, 0, 'مستشفى سان بيتر الدولى', '288376153');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8683, 0, 'صفوت نورالدين حسن ميس', '219825289');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8684, 0, 'الأندلس للتحاليل الطبية - معامل الحكمة', '545913233');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8685, 0, 'محمد انور عيد محمد حسن', '305237314');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8686, 0, 'رضا محمد عبدالفتاح حمزهمحمد رضا عبدالفتاح حمزه', '447067095');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8687, 0, 'محمد احمد السيد عبدالحميد ريان', '218277954');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8688, 0, 'فاروق فتحي حسب ا', '100461522');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8689, 0, 'هاي كير لخدمات الحراسه', '200176633');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8690, 0, 'احمد م احمد يوسف)ضيوف ', '278909639');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8691, 0, 'فايد احمد عبدالوهاب محمدعامر وشركاه', '325437378');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8692, 0, 'شركة كريتيف بابليشينج (محمد خيرى أحمد بركة)', '347769152');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8693, 0, 'ليلي سعد عبدالمطلب احمدعطيه', '235181102');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8694, 0, 'ايركون الهندسية', '336943520');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8695, 0, 'O.T.M', '315933003');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8696, 0, 'د / ممدوح مرسى عباس الحلوانى', '219833168');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8697, 0, 'شركه الغامدي للاستثمار', '494060824');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8698, 0, 'د / تامر محمد باتع ', '537612017');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8699, 0, 'سعيد المليجي العليمي ناصردرويش', '259523747');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8700, 0, 'د / احمد محمد حسين نافع', '305242792');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8701, 0, 'م فتحي فهمي اماممعوض', '315243929');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8702, 0, 'شركة ابوشادى للتحصيل', '208395490');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8703, 0, 'شركة تورنادو ( مؤمن طه محمد و شركاه )', '437278433');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8704, 0, 'مطابع الهرام التجاريه', '100138268');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8705, 0, 'مؤسسة فراج التجارية', '244052069');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8706, 0, 'at-auto', '445140488');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8707, 0, 'معمل رويال لاب', '367337592');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8611, 0, 'معامل جراند للتحاليل الطبية', '553182242');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8612, 0, 'م غريب م', '231855532');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8613, 0, 'مستشفى الرواد للعيون - شركة الرواد للخدمات الطبية', '213096080');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8614, 0, 'شركة الفاروق للمقاولات و التوريدات ', '371642744');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8615, 0, 'اي اس ايميجنج سوليوشن', '225081504');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8616, 0, 'الوطنيه للخدمات الطبيه', '200858637');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8617, 0, 'مصر للنظم الهندسية', '200194909');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8618, 0, 'سدره للحلويات الشرقيهوالغربيه', '200244280');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8619, 0, 'شركه ثلاثمائه وستونسوليوشن للاستشارات العلميه', '377176184');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8620, 0, 'شركة سوفت روزانترناشيونال', '100049001');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8621, 0, ' المجموعة الهندسية للتجارة (اجيتكتون) ', '100399487');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8622, 0, 'technowireless', '305866664');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8425, 0, 'مؤسسة التطوير العربى نوراةعبد الحى', '545879477');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8426, 0, 'دار الاهرام', '287534100');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8427, 0, 'مصر الدولية للانظمة', '200212524');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8428, 0, 'د / عمرو فتحى محمد كامل ', '410923427');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8429, 0, 'جو للتوريدات', '506694224');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8430, 0, 'شركة اكسلانت دي اند ان ( مجله اموال الغد)', '200150898');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8431, 0, 'حسام عبد القادر احمد محمد ', '228058252');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8433, 0, 'م الشحات م كحلهوشركاه', '557781728');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8434, 0, 'مركز مصر للقلب و القسطرة و الاوعية الدموية', '232521735');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8435, 0, 'د- اشرف اسماعيل مصطفي خليفة ', '230101399');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8436, 0, 'نور الحياة للعيون', '435391283');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8437, 0, 'شركه دجرند للمقاولاتوالتصميمات', '547428316');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8438, 0, 'م ابراهيم م عمروشركاه', '320162273');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8439, 0, 'فايز مهني الاسيوطي شركهالاتحادالعربيه للتجاره', '241639220');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8440, 0, 'صبحي جاد م احمد', '232447772');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8441, 0, 'المركز المصري للبحوث و المراجع( ياسر السيد محمد )', '572314019');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8442, 0, 'شركة الحاوى شعبان عبد الوهاب صديق', '420482253');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8443, 0, 'المصطفى للتجارة', '376455209');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8444, 0, 'فؤاد زكريا يوسف فانا', '305591819');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8445, 0, 'المكتب الدولى للاستشارات الهندسية ', '538254556');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8446, 0, 'ايمن عبدالرازق حامد خيره', '572476183');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8447, 0, 'ريفي مارت', '569952565');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8448, 0, 'فوري لتكنولوجيا البنوك و المدفوعات الالكترونية', '286723530');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8449, 0, 'هاني احمد رجاء الدينوشركاه', '411337920');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8450, 0, 'شركه ايجابي', '530963000');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8451, 0, 'ال واصف العالميه للفضيات', '200252348');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8452, 0, 'الطارق و اسلام اوتو للتجارة', '574668659');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8453, 0, 'lcd', '537643540');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8454, 0, 'فاليو ماركتينج ', '574940995');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8455, 0, 'محمد عبدالرحمن على محمود سلطان', '305112260');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8456, 0, 'محمد حسين مرسى على', '234869798');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8457, 0, 'د - محمد عبدالحميد زكى عبدالحميد - عيادة الفيلا', '298687585');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8458, 0, 'ايكونومى بلس', '538402814');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8459, 0, 'شركه بيزنيس بوردر لينز', '508898153');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8460, 0, 'كرز احمد لطفي عوضالجنزوري', '220156107');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8461, 0, 'كوكي سيرفيس', '411479679');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8463, 0, 'معتز حامد محمد علي عثمان', '411711946');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8464, 0, 'السيد عبدالمعطي عبدالحليمالمكاوي', '229929249');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8465, 0, 'think tank', '533612012');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8466, 0, 'برايم كابيتال', '200162810');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8467, 0, 'المكتب االمصرى للاعمال الهندسية ', '200201832');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8468, 0, 'هشام شكري علي محمد', '229958680');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8469, 0, 'عاطف السيد حميده الطحاوي', '219677557');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8470, 0, 'المصرية للخدمات و التحصيل ', '205721877');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8471, 0, 'مروة حسين محمد شحاته - شركة نيو لاين', '250041847');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8472, 0, 'نستله للمياه مصر', '100135838');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8473, 0, 'أرت سنتر للمقاولات', '676725694');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8474, 0, 'مجدي امين مبارك علي', '220226180');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8475, 0, 'el masa', '200163825');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8476, 0, 'يات لحلول التعليم', '455147760');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8477, 0, 'وائل مهدي م احمد', '276409396');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8478, 0, 'شركه امبيلفون الشرقالاوسط للاجهزه السمعيه', '205169163');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8479, 0, 'محمد مصطفي ابراهيم محمدو شريكه', '291433235');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8480, 0, 'محمد مجدي عبداللطيفصالح', '271208996');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8481, 0, 'الشرق للتوريدات الهندسية', '537991530');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8482, 0, 'عمادالدين محمد سعدعبدالجليل', '448110490');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8483, 0, 'المكتب الدولي للتجارهوالتوزيع خالد محمد محمودحسن', '244056633');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8484, 0, 'الشركة المتحدة للاعلان ', '209887222');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8485, 0, 'نابلكو لتجارة المتلزمات المكتبية', '437195422');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8486, 0, 'احمد عمر طوسون عبدالواحد', '219973040');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8487, 0, 'ايروسبورت للخدماتالرياضيه والترفيهيه', '220405816');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8488, 0, 'حسين سيد حسين عبدالله', '448063336');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8489, 0, 'محمد حامد محمود محمدزهيري', '224992546');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8490, 0, 'د / حسن محمود محمد أمين', '227983629');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8491, 0, 'شركة الحدث للصحافةوالطباعة والنشر(مجلةالحدث الا)', '530635925');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8492, 0, 'بيراميدز للتجاره والتوريدات', '100506054');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8493, 0, 'محمد احمد عبدالحميد هندي', '320511987');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8494, 0, 'شركة العروبة لصناعة البن', '205157157');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8496, 0, 'محمد مسعد محمد الشامي', '383701457');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8497, 0, 'د- سامر احمد ابراهيم محمد', '320167569');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8498, 0, 'شوقى جابر سليمان عبدالخالق وشركاه', '413864219');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8499, 0, 'شركة ايديا للاستيراد والتصدير ', '286217147');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8500, 0, 'شركه شنيدر الكتريك مصر', '100259944');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8501, 0, 'شركة يونيون كورير لخدمات البريد السريع ', '479844704');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8502, 0, 'شركة فالكون جروب ', '352222379');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8503, 0, 'عبدالوهاب عبدالغفار عبداللهالسيد و شريكه', '407692355');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8504, 0, 'د / نوران عاطف عبدالحميد البنا', '440388570');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8505, 0, 'مصر للمقاصه والايداع والقيدالمركزي', '200093363');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8506, 0, 'حسن منصور احمد حسانينوشريكه', '217255574');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8507, 0, 'الكندية المصرية', '320921530');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8508, 0, 'فايزه احمد عبدالفتاح احمد', '447891146');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8509, 0, 'احمد سمير م حسينوشركاه', '674571649');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8510, 0, 'امل م احمد سويلم', '230471463');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8511, 0, 'د / حازم عنتر فتح الله مشالى ', '330982516');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8512, 0, 'د / حنان ابراهيم السيد بلبع - مركز طيبة للعلاج الطبيعى ', '724327916');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8513, 0, 'بهاءالدين عبدالقادرعبدالرازق زهره', '437516180');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8514, 0, 'اشرف محمد رجب عبدالهادي وشركاه ', '411516167');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8515, 0, 'فودافون داتا', '205051456');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8516, 0, 'محمد مؤمن جمال الدين ', '437379035');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8517, 0, 'خدمات المعلوماتوالمعاملت الليكترونيه', '342813064');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8518, 0, ' هيملتس للمشروعات المتكاملة ', '565156896');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8519, 0, 'مني عبدالمحسن عثمانالشبراوي', '220173729');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8520, 0, 'ايناس راجح احمد راجح', '288834836');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8521, 0, 'مجلة إكتوبر ', '200024159');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8522, 0, 'شركه البشر سوفت', '331005557');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8523, 0, 'د- ايمن ربيع عبد القادر احمد (مصر سكوب )', '293534705');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8331, 0, 'ايماكو للمقاولت العامهواعمال التكييف طارق موشركاه', '200294334');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8332, 0, 'منصور ام جي اوتوموتيف', '726799858');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8333, 0, 'القرش تريد وجزان للتسويقالعقاري - محمد احمد محمد', '526182474');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8334, 0, 'مؤسسه شديد الهندسيه احمدمحمد السيد شديد', '217126219');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8335, 0, 'جمال عبدالدايم سليمان سيداحمد وشريكه', '332330117');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8336, 0, 'محمد عبدالعظيم عبدالجوادمحمد', '203548558');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8337, 0, ' طوخى مصر للطباعة ', '100291694');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8338, 0, 'رالي للتصميمات و التركيبات ', '288829093');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8339, 0, 'توب سكان لب للشعهوالتحاليل', '463241673');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8340, 0, 'شركة بلو تاتش ', '407428933');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8341, 0, 'مستشفيات و مراكز المغربي', '205078567');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8342, 0, 'شركة كايرو ويست للإعلان ', '331558165');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8343, 0, 'شركة خط احمر للنشر ', '726285188');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8344, 0, 'ابراهيم احمد محمد الياس', '220216436');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8345, 0, 'م عبده امين عثمانوشركاه', '231828160');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8346, 0, 'خالد عبد الحميد محمد (جريدة الوطن المصرى) ', '521433606');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8347, 0, 'السيد غباشي السيد رميح', '477217729');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8348, 0, 'سبيرتون للاستيراد والتصدير و التوكيلاتالتجاريه', '592306984');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8349, 0, 'ميلكومك ايجيبت للمصاعد والسللم المتحركه', '200177028');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8350, 0, 'د- معتز عصام غيث زكي', '677211554');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8351, 0, 'السبع اوتوموتيف', '200161679');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8352, 0, 'عبدالله محمد عبداللهعبدالرحمن', '286405717');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8353, 0, 'ادوار وصفي يني', '412897172');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8354, 0, 'اديو فيكس لتنظيم المؤتمراتوالتدريب', '552474576');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8355, 0, 'شركة اس اند إيه للتجارة ', '731227697');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8356, 0, 'Nasr Consultant', '220106746');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8357, 0, 'جيهان عمر شحاته عبدالدايم', '388396245');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8358, 0, 'محمود مرسي محمد (مجلةالاقتصاد والتمويل )', '499070100');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8359, 0, 'التوحيد للمستلزمات الطبية ', '327314966');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8360, 0, 'احمد السيد مصطفي محرم', '220128510');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8362, 0, 'شركه و مكتبات سمير و علي', '315986972');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8363, 0, 'مكتبة فؤاد', '217501516');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8364, 0, 'شركة اتصالات مصر للاتصالات', '235071579');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8365, 0, 'مانترا للسيارات', '200147072');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8366, 0, 'شركه عزمي عادل عزميوشركاه', '217228615');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8367, 0, 'مستشفي المقاولون العرب', '204892813');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8368, 0, 'بريموس', '202873854');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8369, 0, 'محمد عبدالمحسن عبدالعظيم وشركاه', '289050146');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8370, 0, 'محمد مختار عباس عثمان', '484121170');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8371, 0, 'الازهرى التجارية', '244063109');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8372, 0, 'محمد علي محمد علي', '332220117');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8373, 0, 'ميراتك', '411055062');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8374, 0, 'شهاب للتجارة ', '100364632');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8375, 0, 'نيو هارت ايجيبت للمنشآت الطبية المتخصصة', '557562724');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8376, 0, 'سمارت فون', '337723990');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8377, 0, 'هاله كمال متولي سليمان', '484158090');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8378, 0, 'بتروكٌما للهندسه والمقاولات ش.م.م', '200100076');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8379, 0, 'SHIELD ENVIRONMENTAL SOLUTIONS', '577329553');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8380, 0, 'شركه التوفيقيه للنشاءوالتعمير', '100507255');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8381, 0, 'المتحدة للخدمات المالية خالج ابراهيم الدسوقى', '741474115');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8382, 0, 'صبحى فرج السيد محمد', '403912873');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8383, 0, 'د / عبدالله ابراهيم عبدالله عبدالحى - لايف سمايل', '553224239');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8384, 0, 'المكتب الهندسى للتقييم و ادارة المشروعات-على عبد الرحمن', '381386805');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8385, 0, 'اسلام محمد السيد- المحيسن اوتو', '494429216');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8386, 0, 'شركه المستقبل للصحافهوالطباعه والنشر', '266962602');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8387, 0, 'حمدي مرسي فرحات شريف', '318149397');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8388, 0, 'شركه موبايل شوبللتوكيلات التجاريه', '640783007');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8389, 0, 'الكمال للشاش الطبى', '731042433');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8390, 0, 'جوب ماستر', '435822764');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8391, 0, 'لبني عبدالعزيز علي احمد', '376295090');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8392, 0, 'احمد عبدالفتاح احمد ابوريه', '311015255');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8393, 0, 'د -عبد العزيز علما ( عبد العزيز احمد فهر عبد العزيز علما)', '410486752');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8394, 0, 'عمرو محمود زايد محمود', '220128669');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8395, 0, 'شركة مستشفى الزهيرى ', '204898080');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8396, 0, 'احمد مصطفي كمال وشركاه', '410692468');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8398, 0, 'عمران م م عمرانمركز القاهره للمؤتمرات', '677490119');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8399, 0, 'د/ حازم مصطفى كمال السيد محمود', '379543435');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8400, 0, 'محمد محمود عويضه', '381996344');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8401, 0, 'محمد عبدالمحسن محمدعبدالرازق', '452432960');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8402, 0, 'د / هشام اسماعيل محمود السباعى ', '256435332');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8403, 0, 'الزمر سيرفيس', '471917850');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8404, 0, 'المصرية للإعلانات ', '100403913');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8405, 0, 'سامح هنداوي هندي هبل', '474188469');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8406, 0, 'عبير سعد زغلول محمد محمد العشرى', '572353634');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8407, 0, 'شركه اد فاكتوري', '547982356');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8408, 0, 'لشركة المصرية العربية للانشاءات ايجاك', '672812539');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8409, 0, 'سمارت كوم للخدمات التجارية ', '215740777');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8410, 0, 'الشركه العالميه للاستيرادوالتصدير والتوكيلاتالتجاريه', '437960285');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8708, 0, 'شركه بيتا تكنولوجي مصر', '323783260');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8709, 0, 'خالد محمد حافظ غازي', '239496302');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8710, 0, 'د / احمد حسن حلمى حامد', '219918767');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8711, 0, 'كارديو كاث ميديكال', '279376103');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8712, 0, 'شركه برايم اوتوموتيف', '560247966');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8713, 0, 'نايتس للدعاية والاعلان', '504874284');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8714, 0, 'ميديا سورس مواهب عبدالرحمن شريكها - جريدهالميزان', '518480356');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8715, 0, 'د / عمرو مصطفى علوان', '219903808');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8716, 0, 'المركز الاعلامى العربى ', '200140698');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8717, 0, 'المصريه لتصميم وتصنيعالنظم الالكترونيه وسام', '100408311');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8718, 0, 'مؤسسة أخبار اليوم - قطاع الإعلانات  ', '100396593');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8524, 0, 'حسام الدين غانم عبدالغفارالحصري', '220159092');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8525, 0, ' نوبل واكس برودكتس ', '100000517');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8526, 0, ' مدارات التقدم', '508790328');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8527, 0, 'شرك ام اوتو', '413660532');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8528, 0, 'د / محمد محمد عبدالرحمن الكردى', '220135444');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8529, 0, 'فرست سيرفيس  للخدمات', '200166573');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8530, 0, 'ايدج ستوديو ساندرا نيقولرزق ا', '735631719');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8531, 0, 'د / محمد احمد حسن احمد', '484084380');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8532, 0, 'مركز مدينتي للجراحه -', '674253752');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8533, 0, 'meccano advertising', '729144569');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8534, 0, 'المكتب الاستشارى بيسر للاستشارات- Pacer consultants', '200057308');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8535, 0, 'محمد عزت الكفراوى', '288325737');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8536, 0, 'د - محمد ماهر محمد الرمادى - مركز ميما للأسنان', '463319508');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8537, 0, 'الفرج جروب للتجارة', '200245619');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8538, 0, 'د- عمرو سعد محمد احمد فرج ( لوران دنتال كير)', '381863042');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8539, 0, 'ماجدة رمزى عزيز إسكندر - مركز ديانا ', '288134400');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8540, 0, 'الشركة السباعية للمشروعات السياحية - أبوشقرة ', '200185268');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8541, 0, 'شركة سيفتى بلس', '262306905');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8542, 0, 'شركه توب للاعلان', '200258893');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8543, 0, 'شركه يونويا للدعايهوالاعلان', '731312155');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8544, 0, 'المجد لاعمال النظافة', '428143008');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8545, 0, 'شركه تراكس بي ار', '418672857');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8546, 0, 'شركة النخبة للخدمات الطبية - مستشفى دار العيون', '205115977');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8547, 0, 'الشركه الالمانيه لخدمه السيارات', '200143077');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8548, 0, 'بدايات للتنمية العمرانية ', '484529889');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8549, 0, 'مؤسسة روز اليوسف', '100350755');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8550, 0, 'شركه الستشارات الماليه عنالوراق الماليه', '330941895');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8551, 0, 'الشركة الهندسية للتجارة والنظم', '452591899');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8552, 0, 'مني وهيب عريان', '214943437');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8553, 0, 'تاير برو', '269452852');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8554, 0, 'Catalyst', '418709904');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8555, 0, 'حسين رضوان فهمي رضوان', '548370044');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8556, 0, 'الشركة المصرية التجارية واتوموتيف', '558070100');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8557, 0, 'ليدز للمقاولات', '332820165');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8558, 0, 'د- خالد جميل هلال و شركاه بونساي للعلاج الطبيعي', '479816786');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8559, 0, 'Data Trust', '205000657');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8560, 0, 'شركه يونايتد جروب سيرفسسنتر', '254845215');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8561, 0, 'المتحدة للتقييم العقاري و التسويق', '611584387');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8562, 0, 'احمد السيد عبدالله حسين - شركة الجولف للتجارة', '487181727');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8563, 0, 'شركه ومكتبات سمير وعلي', '100022243');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8564, 0, 'اى تى سيستم', '714542547');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8565, 0, 'عامر  صبحي محمد عامر', '496808222');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8566, 0, 'شركة  باراجونز', '653138407');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8567, 0, 'بي اي سي للاستشارات', '735706832');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8568, 0, 'شركة البردى لصناعة الورقفاين', '100082349');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8569, 0, 'ولاء محمد طاهر صلاحالغراب', '489178693');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8571, 0, 'سما مصر انترناشونال - شريف أنور محمد  ', '377077828');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8572, 0, 'دالتكس', '100413102');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8573, 0, 'د / خالد عبدالمنعم زكى ', '219939136');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8574, 0, 'د- ابرام عدلي تزفيق خليل', '200398490');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8575, 0, 'منال منير زكى', '508380367');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8576, 0, 'المجموعة الاستشارية شاكر ', '465215734');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8577, 0, 'د/ مريم عبدالغني ابراهيم -مركز دينتال للسنان', '555376109');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8578, 0, 'وائل محمد محمد حسني سويلم', '691671214');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8579, 0, 'دار الدراسات الهندسية د.هدى فكرى محمد الجمل', '560487355');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8580, 0, 'ايجيليير ترافيل', '347698530');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8581, 0, 'طلعت علي حسن السمني', '268814759');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8582, 0, 'راية لخدمات مراكز  الاتصال', '205102549');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8583, 0, 'د / هشام احمد عيسى مصطفى وشركاه', '528191020');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8584, 0, 'الهلي للتسويق العقاريوالستشارات استس', '200149482');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8585, 0, 'صلاح رمضان زكي بركات ( سوبر ماركت هاني ) ', '493609326');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8586, 0, 'احمد محمد عبدالواحد', '217290019');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8587, 0, 'محمود علم الدين عبدالحافظمحمد حسنين', '252421329');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8588, 0, 'شركه توري أر أم', '381945030');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8589, 0, 'شركة كايرو تريد للانظمة المتكاملة', '518710742');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8590, 0, 'عبدالمنعم رمضان محمد', '223032417');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8591, 0, 'كمال الدين محمود رجبحمزه وشركاه', '100408710');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8592, 0, 'مختارحسين محمد العجاتي', '249690810');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8593, 0, 'كارديوتك للرعايه الطبيه', '331522640');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8594, 0, 'حراسات للامن والحراسة', '721398634');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8595, 0, 'شركة المركز العربي للصحافه ( جريدة البوابه )', '463253450');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8596, 0, 'اكسبريس مصر للتجاره    ', '535403550');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8597, 0, 'فالكون للخدمات العامه وادارهالمشروعات', '324024134');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8598, 0, 'مركز تفتييت الحصوات', '200135619');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8600, 0, 'هيثم جويفل سليمان جويفل', '367052296');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8601, 0, 'احمد السيد عبدالحميد ريانوشركاه', '290457076');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8602, 0, ' هيلثى لمنتجات الرعاية الصحية ', '735909245');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8603, 0, 'المتقدمة للخدمات المتكاملة ', '252173902');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8604, 0, 'م اسماعيل ماسماعيل', '267115504');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8606, 0, 'صفوت نور الدين للخبرة الاستشارية', '560468016');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8607, 0, 'شركه مصر لخدماتالمعلومات والتجاره', '200084739');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8608, 0, 'محمد اشرف صلاح الديناحمد', '290534054');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8609, 0, 'طارق رافت عبدالحليم حافظ', '232538379');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8610, 0, 'Creative Villa', '560023685');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8411, 0, 'د- حسام محمد منصور سليمان عيادات د- حسام منصور للتخصصات الطبية', '261311964');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8412, 0, 'الصفا للتجاره', '200190520');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8413, 0, 'الهلي لجهزه الحاسب اللي', '200122053');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8414, 0, 'المؤسسه الهندسيه محفوظللعماره والنشاء', '487137019');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8415, 0, 'شركة طيبة للاستثمار العقاري', '506693422');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8416, 0, 'مؤسسة دار التحرير للطبع والنشر والإعلانات المصرية ( الجمهورية )', '448117975');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8417, 0, 'اليابانية لتجارة الادواتالكهربائية', '669587125');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8418, 0, 'اوديو للتكنولوجيا', '100487114');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8419, 0, 'رؤوف محمد احمد مهران وشركاه - مركز جلورى للقسطرة', '376309253');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8420, 0, 'م شريف م عادلمراد', '320176029');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8421, 0, 'محمد محمد محمد محمد يسوشركاه', '435678108');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8422, 0, 'ميديا وورلد للدعايه والعلن', '724125256');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8423, 0, 'د- محمد علي محمد حداد عز العرب', '669399027');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8424, 0, 'شركة حابي للدعاية و الاعلان', '667366784');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8233, 0, 'د - مصطفى محمد الصايغ', '266172164');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8234, 0, 'مودرن موتورز', '100059732');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8235, 0, 'راجي انيس اسكندر و شركاه', '100402615');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8236, 0, 'وليد عبدالمنعم السيد عليمطر', '369449746');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8237, 0, 'هناء رافت نصيف زخاريوشريكها', '455197504');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8238, 0, 'اتي للخدمات', '584306156');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8239, 0, 'شركة الإعلام للصحافة والطباعة والنشر والتوزيع والدعاية', '374453152');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8240, 0, 'محمد عبدالعظيم عبدالجواد و شريكه', '320863190');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8241, 0, 'عطيه قاسم محمد قاسم', '205186017');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8242, 0, 'شركة المصرية لتطوير خدمات نقل الركاب', '730690350');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8243, 0, 'MAD Sulotion', '333177096');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8244, 0, 'د / ايمن محمد عثمان الكحكى ', '274197944');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8245, 0, 'اي فيجون ', '205053955');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8246, 0, 'عبدالحميد احمد حسانين', '217142222');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8247, 0, 'بايونير لخدمات المن', '279091702');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8248, 0, 'د / أحمد عادل محمد جمال المسيرى', '281821356');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8249, 0, 'فيبنى استشاريون في المال و الاعمال', '200145967');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8250, 0, 'ايجيبرو لادارة المشروعات و انتاج و توزيع الطاقه ايجيبرو - EgyPro', '455167885');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8252, 0, 'ثناء بسيوني ابراهيم محمد', '349971765');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8253, 0, 'شركة جلوبال للطباعة و النشر و الصحافة', '526011742');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8254, 0, 'ام جي ام للتجاره والتوريدات', '236733842');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8255, 0, 'البافاريه لتجاره السيارات', '200245732');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8256, 0, 'وائل محمود مرسي الكيلاني', '291225705');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8257, 0, 'تارجت للتسويق والتحصيل', '205808794');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8258, 0, 'ارييكا  ARIIKA', '518934489');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8259, 0, 'شركه المنصور شيفورليه (بيجو )', '200174072');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8260, 0, 'شركه الحسين الدوليه', '200017454');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8261, 0, 'د- باسم ابراهيم يوسف لاشين', '376607009');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8262, 0, 'Raya Networks Services', '205053572');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8263, 0, 'بهاءالدين سيد محمود مرسي', '220137625');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8264, 0, 'احمد رمضان عبدالتواب علي', '259293423');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8266, 0, 'صوت الأمة ', '200125842');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8267, 0, 'حاتم احمد طلعت محمودبرهام - بروباجندا ايجنسي', '487008480');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8268, 0, 'المهندس اوتوموتيف للتجارهوالتوزيع', '235085685');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8269, 0, 'سمير ماهر اسماعيل مهران وشريكه', '463065107');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8270, 0, 'مركز تكنولوجيا الاشعه تكنوسكان', '200105957');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8271, 0, 'شركة مياه الشرب ', '212543504');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8272, 0, 'سيجما سكان د / سابق إمام لبيب وشركاه ', '262306697');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8273, 0, 'كمبيوتر شوب للتوزيع', '330872443');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8274, 0, 'شركة معامل الشمس', '413730476');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8275, 0, 'شركة ميكروفيلم ايجيبت ', '100055842');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8276, 0, 'سبيشيال للمقاولات special costructing', '531093344');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8277, 0, 'شركه مركز مصر للاشعه', '205004814');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8278, 0, 'ستارز سيرفيس ', '560020813');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8279, 0, 'ايفاب ايجبت للصناعاتالهندسيه', '332627543');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8280, 0, 'المكتب الاستشارى / حسام رشوان ديزاينز', '281261822');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8281, 0, 'دي اي انترناشيونال ايجيبت DE international Egypt', '266778658');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8282, 0, 'ابو زياده لاداره الصيدلياتصيدليات ابو زياده', '382028473');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8283, 0, 'حسين م طاهر عبدالتواب', '207412375');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8284, 0, 'شركه الطارق للتجاره استيراد و توزيع السيارات الطارق', '200131745');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8285, 0, 'دلتا للتجارة', '200214047');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8286, 0, 'ايزى واى اوتوموتيف', '274408333');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8287, 0, 'تامر حسين عباس مشرفه', '225074117');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8288, 0, 'مريم فوزى عزيز ايوب - صيدلية مريم فوزى', '599632542');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8289, 0, 'الشركه الدوليه للتنميهالداريه والبشريه', '301040206');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8290, 0, 'ايمن عبدالفتاح احمد عطية - صيدلية ايمن', '410491438');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8291, 0, 'البرجسي للتجاره والتسويق', '239855957');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8292, 0, 'شركه اي اف جي للتجاره', '262128500');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8293, 0, 'حازم حسين رمزي', '692908722');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8294, 0, 'الدوليه للخدمات السريعه سبيدسيرفيس', '200159593');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8295, 0, 'مستشفي جمال ماضي ابو العزايم ', '220182507');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8296, 0, 'مجله العقاريه', '286135590');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8297, 0, 'شركه ذا كايرو انجيلز', '467581460');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8298, 0, 'د / كلثوم محمد محمد عبدالحميد', '674282167');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8299, 0, 'م ابراهيم حافظ شركهكادر صناع براويز', '713542845');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8300, 0, 'الايكونوميست المصرية', '219807884');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8301, 0, 'د- هشام احمد جاب الله الغزالي (الفا للاورام)', '206041446');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8302, 0, 'مكتب صلاح شعبان', '219827699');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8303, 0, 'محمد سعد محمد حسن', '295346558');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8304, 0, 'نيونك لخدمة واصلاح وصيانة السيارات ', '672435853');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8305, 0, 'تويوتا ايجيبت', '204931630');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8306, 0, 'جلوبال تكنيكال سيرفيس ', '538438622');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8307, 0, 'الصحوة الكبري ', '291284604');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8308, 0, 'شركه رايه للنظم', '200113186');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8309, 0, ' جريدة المساء ', '200127918');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8310, 0, 'طارق حسانين عبدالعزيز منصور ابوالنصر', '487210905');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8311, 0, 'شركة النصر للتجارة و المقاولات ', '374151083');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8312, 0, 'شركة السجان', '319833216');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8313, 0, 'الملك لتجاره السيارات', '200216600');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8314, 0, 'المصرية للمستشفيات', '463535529');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7745, 0, 'عيسي مراد شاكر', '275962296');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7746, 0, 'محمد عبدالعال احمد', '232886857');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7747, 0, 'النصر للامن والحراسه', '670025577');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7748, 0, 'شريف مراد جرجس سليمان', '509218849');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7749, 0, 'كمال محمد سعد حمدان', '100321798');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7750, 0, 'محمد فخرالدين احمد محمودمحمد', '369688902');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7751, 0, 'المركز الدولى للاشعة - مركز تشخيص المبكر لامراض القلب والشرايين كارديو سكان', '218098227');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7752, 0, 'محمود اسماعيل عبداللطيفجاويش', '533644879');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7753, 0, ' مونى ادد Mony add ', '518930416');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7754, 0, 'شركه الخير للمقاولات العامه', '504318296');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7755, 0, 'ايهاب م شريف فتحيعبدالحليم', '447985183');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7756, 0, 'Refintiv Limited Co', '200003232');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7757, 0, 'بافرية لصناعة السيارات ', '204984076');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7758, 0, 'المصريه للنشر العربيوالدولي', '200156985');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7759, 0, 'فريد ماركو وشركاه', '437276228');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7760, 0, 'شركة الانباء الدولية للإعلان والصحافة والنشر', '212581368');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7761, 0, 'مركز البرج للعلاج الطبيعى(د/هشام احمد الوصيف زيد)', '323732445');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7762, 0, 'فتحي محمد سعيد وشريكه', '214673642');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7763, 0, 'اسلام فتحي وشريكته اي تيسمارت نتورك', '721014445');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7768, 0, 'شركه دياب للجهزهالتعويضيه اللمانيه', '244132305');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7769, 0, 'تاير برو ايجبت', '369452852');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7770, 0, 'د / ناجى سامى جبران غطاس', '262130319');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7771, 0, 'م عبدالفضيل معبدربه وشركاء', '411768239');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7772, 0, 'مصر للطيران للسياحهالكرنك والاسواق الحره', '100399053');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7773, 0, 'شركة باك اوفيس', '281587132');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7774, 0, 'الموجز للصحافه والطباعهوالنشر', '225390132');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7775, 0, 'بروسيس للخدمات والحلول ', '281778183');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7776, 0, 'مكتب دهب للتوريدات', '692569219');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7777, 0, 'شركه سان اند سام', '504685473');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7778, 0, 'الجيزة الوطنية للسيارات', '200222112');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7779, 0, 'شركه ابليانس للحلولالتسويقيه', '518627397');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7780, 0, 'شريف محمد السيد محمد', '293572399');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7781, 0, 'محمد اشرف طه غباشيمصطفي', '200328824');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7782, 0, 'الخريستو للتجاره والتوزيع', '200138782');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7783, 0, 'رجب نصر عيد حامد و شريكه', '447997572');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7784, 0, 'شركه كواليتي للتوكيلاتالتجاريه', '200154257');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7785, 0, 'فندق بيروت اولد كيرلس عبدالشهيد', '100033164');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7786, 0, 'د/ إسلام عبدالحميد عبدالحميد إسماعيل', '481717536');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7787, 0, 'مستشفى أندلسية المعادى ', '205134807');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7788, 0, 'سري ديزاين', '256855706');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7789, 0, 'ادز بلس للدعاية و الاعلان', '440555027');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7790, 0, 'شركة دار الاخبارية للصحافة و الطباعه و النشر', '519001540');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7791, 0, 'اشرف خورشيد جميلوشريكه', '100536093');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7792, 0, 'محمد فارس جمعه محمد وشريكه سيف الدين محمدفارس جمعه', '433205393');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7793, 0, 'شمس الدين عبدالحافظ ابواليزيد احمد', '369850602');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7794, 0, 'مصطفي عبدالمنعم ابراهيمقرشي', '437382176');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7795, 0, 'سوق مصر للسياراتالمستعمله فبريكا', '455024014');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7796, 0, 'الشركه المصريه للبطاقات', '205131700');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7797, 0, 'شركة دلتا تكنولوجى', '269585427');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7798, 0, 'وحيد رشاد السيد ابو الدهب', '508542758');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7799, 0, 'الاهرام لتجاره الادويهوالاجهزه الطبيه', '200176986');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7800, 0, 'لاب ميد معامل للتحاليلالطبيه', '516015907');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7801, 0, 'شركة ماجيوم للانظمة الطبية و التعليمية', '325277923');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7802, 0, 'شركه المحلات العموميهللموسيقي', '200011669');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7803, 0, 'محمد محمد حمدي سيد مصطفي ( جوكلين )', '210807830');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7804, 0, 'شركه سيتي للتجارهوالاستشارات', '672630532');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7805, 0, 'khattab', '213296969');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7806, 0, 'تامر محمد سيد محمود', '208060855');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7807, 0, 'محمد سعد شعبان عبد الغفار- لاب تك', '538528265');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7808, 0, 'تاسك جروب', '337826706');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7809, 0, 'شركة الامين للاستيراد و التصدير', '335050433');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7810, 0, 'محمد صابر احمد السعدنيوشريكيه', '373971990');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7811, 0, 'مستشفي عمر بن الخطاب', '219837457');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7812, 0, 'مستشفي ابن سينا التخصصي', '204898633');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7813, 0, 'شركه مصاعد اوتيس بمصر', '100353053');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7814, 0, 'شركه الدهان للاستثمار', '528163175');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7815, 0, 'شركه كامبيتي ناتشن فتحيسعيد فتحي', '438255992');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7816, 0, 'تراست للمقاولات و الديكور', '584394055');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7817, 0, 'شركه الدلتا الصناعيه ايديال', '100033571');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7818, 0, 'م سيد رمضان علي', '412066750');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7819, 0, 'م م م عوضين', '504381296');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7820, 0, 'كايرو كمبيوتر سنتر', '499118987');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7821, 0, 'كواليتى كير للخدمات الطبيةوادارة المستشفيات', '484475304');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7822, 0, 'اتى', '284306156');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7823, 0, 'ممدوح حلمي عبداللطيف', '220160848');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7824, 0, 'دار ابوالعزايم للطب النفسى وعلاج الادمان د / احمد جمال ماضى ابوالعزايم', '219906807');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7825, 0, 'ورثه يوسف امين قناويوشركاه ', '100003478');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7826, 0, 'رانيا حاتم عبدالمنعم الجمالوشركاها', '220205620');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7827, 0, 'برفكت للخدمات المالية', '286877760');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7828, 0, 'عماد كامل ابراهيم جرجس', '266592260');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7829, 0, 'جريده الصباح نيوز', '548020469');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7830, 0, 'شركة محفوظ للعمارة', '728278103');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7831, 0, 'سوفت روز انترناشيوناللتحويل الورق والبلستيك', '100049257');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7832, 0, 'د / محمد صبرى محمد حسين', '735769702');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7833, 0, 'يونيتيد جروب مصرلتجهيزات المعارض والتجارة', '537880186');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7834, 0, 'الشركة الاسلامية للتجارة', '217452388');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7835, 0, 'رويده احمد علي سليمان', '407594515');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7836, 0, 'شركة الاميدا فاينانشيال ليمتد شركة اعمال دولية - مستشفى السلام الدولى', '298758970');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7837, 0, 'شركة انوفو', '381255298');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7838, 0, 'د- سمير كامل محمد عبده', '677128126');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7839, 0, 'ميخائيل حبيب باسيلى عبدالملك  وشريكته - افا ابرام جروب', '320949915');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7840, 0, 'المصرية للاتصالات  WE', '205127088');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7841, 0, 'المركز الدولى التخصصى للعيون', '200126482');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7842, 0, 'بيوند للخدمات الامنيهوالاستشارات والانظمهالامنيه', '369900839');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7843, 0, 'انفويس', '560297300');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7844, 0, 'مركز رؤية شبرا للعيون', '342651455');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7845, 0, 'ايمن احمد اسماعيل وشريكه', '527970735');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7846, 0, 'مسعد فوزي السيد ابراهيم', '342949373');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7847, 0, 'Springs', '476823722');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7848, 0, 'ميديا جولد', '557584167');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7849, 0, 'حسن ابراهيم كامل يوسف', '403895383');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7850, 0, 'المستشفى الدولى للكلى والمسالك البولية', '204951062');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7851, 0, 'الصفوة للمقاولات و نظم التكييف', '416245463');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7852, 0, 'دلتا اراس للتجاره راديو شاك', '200216783');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7853, 0, 'الخليفه للتجاره اتوماجد', '200181246');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7854, 0, 'جريدة الاموال', '323518559');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7855, 0, 'د- دينا طه علي رزق ', '528357875');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7856, 0, 'القاهره الوطنيه للسيارات', '200218905');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7857, 0, 'جلوبل ارت للطباعة و النشر و التغليف', '502763587');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7858, 0, 'شركة جي سي للاظرف و الطباعة ', '554982595');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7859, 0, 'جريدة الحصاد', '298625229');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7860, 0, 'جماعة المهندسين الاستشاريين ', '200066773');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7861, 0, 'شركة جملة ماركت ', '205354793');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7862, 0, 'ستارز سيرفيس طارق رشاد', '408734604');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7863, 0, 'خالد زكى محمود احمد هاشم وشركاه - الحياة سكان', '374078270');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7864, 0, 'ناصر م علي وشريكته', '374786690');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7865, 0, 'اماني سيد عبدالعال حسن', '281540543');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7866, 0, 'شركة بروفيشنال بابليشينج اند بروموشن ', '337742626');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7867, 0, 'مؤسسه سكاي', '257342435');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7868, 0, 'معرض / اوتو لتجارة السيارات', '455441537');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7869, 0, 'الشركة الدولية للتجارة والتسويق وتوكيلات التجارية -ايتامكو', '100072593');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7870, 0, 'سبيشيال للمقاولات', '552937290');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7871, 0, 'الايطالية للستائر و التوريدات', '421079924');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7872, 0, 'ايكو ليدرز للمقاولت', '728294516');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7873, 0, 'لاند ماسترز', '200149504');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7874, 0, 'الرواد هاى سيرفيس', '210308397');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7875, 0, 'سي ووتر الفرعونية لمعالجة المياه', '592076970');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7876, 0, 'هيثم صلاح الدين عبدالحافظخليل', '269665072');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7877, 0, 'فاديه نظير حبيب', '650427130');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7878, 0, 'لايف سيرفيس', '572317875');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7879, 0, 'سامي فؤاد عبدالنور سليمان', '325852707');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7880, 0, 'المركز الوطني للاشعه وطنيسكان محمود ابراهيم علي', '369896238');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7881, 0, 'it blocks', '567578429');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7882, 0, 'د- سامح السيد علي سالم', '305508245');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7883, 0, 'حسين احمد كمال الدين حسين', '494166193');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7884, 0, 'مستشفى بدراوى', '219674833');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7885, 0, 'الأطباء المتحدون للخدمات الطبية والعلاجية - مستشفى الجزيرة', '301151407');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7886, 0, 'المركز المصرى لتأهيل مرضى القلب والاوعية - مركز ايجيهارت الطبى', '205051863');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7887, 0, 'شركة بابيروس لتجويل الورق', '207346593');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7888, 0, 'مصر لمن المعلومات سكيورمصر', '279082568');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7889, 0, 'المعمل المصرى للتحاليل الطبية - كايرولاب', '437913260');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7890, 0, 'معرض اورجينال موتورز عمر يحيى ', '445388412');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7891, 0, 'الشركة الإقتصادية للطباعة والنشر', '237419580');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7892, 0, 'السعيد للاستشارات الماليه  و الهندسية', '219995885');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7893, 0, 'مينا فهمي صبحي زغلول', '447578677');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7894, 0, 'شركه كمبيو سوفت', '449924335');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7895, 0, 'كريم علاءالدين شحاتهعبدالغني', '431373698');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7896, 0, 'عماد م عبدالمنعم', '713604700');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7897, 0, 'Data Kraft', '561772606');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7898, 0, 'مركز ميجا سكان للأشعة ', '605818738');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7899, 0, 'شركه بيم ستورز', '413344312');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7900, 0, 'العثمانيون لتجارة السيارات والاستيراد و التصدير', '340341459');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7901, 0, 'د / اشرف مختار طه مختار طه ', '614121426');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7902, 0, 'GDE', '217323189');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7903, 0, 'مركز البستان الطبيلتشخيص امراض العين', '210343079');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7904, 0, 'دينا م عبدالرحمن ماحمد و شريكها', '276266420');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7905, 0, ' ـ المتحدة الخدمات العامة ــ قيمة اعمال ', '432600108');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7906, 0, 'وائل مجاهد وشركاه', '256945845');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7907, 0, 'سمير شاكر احمد بدوي', '290512042');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7908, 0, 'د- باسم احمد حسن حلمي حامد', '528794582');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7909, 0, 'جلال عبدالسلام محمد', '220117802');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7910, 0, 'نورالحياه للعيون وجراحاتالعيون والليزراسامهمحمدالسعيد', '212207032');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7911, 0, 'سماح علم الدين عبدالجوادعلم الدين', '254887406');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7912, 0, 'عباس ماهر عباس عافيه', '383570794');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7913, 0, 'راج للتجاره', '200010360');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7914, 0, 'عاطف عبدالله عبدالفتاح عبدالرحمن خلف', '420554475');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7915, 0, 'د / عمرو عبدالله محمد تهامى - مركز فوتون لاشعة الاسنان ', '435834800');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7916, 0, 'د- محمد سليم محمد سليم', '276451007');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7917, 0, 'فرج سمعان جوى', '232138656');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7918, 0, 'الميدا لب للتحاليل الطبيه', '374231656');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7919, 0, 'انتركوم انتربرايزس للحلوللتكنولوجية', '728352052');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7920, 0, 'العبدللاستثماروالتصنيعوتجاره الحلوي', '264613805');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7921, 0, 'شركه سيارات القرش', '463425596');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7922, 0, 'كويست ايجيبت سوفتويرلكتابه وتصميم البرامج', '303335572');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7923, 0, 'عكاوي للتجاره', '200252917');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7924, 0, 'علء عبداللطيف غريبالشرقاوي', '246515236');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7925, 0, 'القاهره لتوزيع وخدمهالسيارات', '200215655');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7926, 0, 'د/ احمد السعيد البسطويسى عبدالجواد', '347177743');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7927, 0, 'العربي للتجاره والصناعهالعربي', '200035223');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7928, 0, 'شركه الفصحاء', '552448907');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7929, 0, 'محمد ابراهيم امام محمدوشركاه', '516210310');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7930, 0, 'الجنة للرعاية الطبية', '545591627');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7931, 0, 'فبروسكوب للخدمات الطبية', '487244257');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7932, 0, 'محمد جمال عثمان جبريل', '337773777');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7933, 0, 'برناسوس للصناعه والتجارهوالمكتبات مكتبات برناسوس', '205072135');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7934, 0, 'شركة ميتالب للتحاليل الطبيه', '498708837');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7935, 0, 'شركة ايليت سيرفيس', '724303960');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7936, 0, 'HURGADA MARRIOTT HOTEL', '100512216');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7937, 0, 'محمود عبدالعاطي محمد وشريكه', '377046094');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7938, 0, 'ماجيك ميديا للبرمجيات و الدعاية و الإعلان ', '545464439');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7939, 0, 'مجمع الاستشاريين', '200063537');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7940, 0, 'شرين احمد خليل الخرزاتي', '676940323');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7941, 0, 'اسحاق روحى كرومر (جريدة منبر التحرير)', '376572736');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7942, 0, 'اباظه اوتوتريد', '254709257');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7943, 0, 'شركه فيكتوري لينك مصرللبرمجيات', '200646559');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7944, 0, 'مروه حسن حسين محمود', '540433985');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7945, 0, 'د- جاد ابو الحمد السيد احمد', '433130229');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7946, 0, 'عماد محمد عادل ابراهيم القاضي', '250077116');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7947, 0, 'صحه سكان', '421187875');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7948, 0, 'NOLA', '347447546');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7949, 0, 'ريب سوليوشن', '500690812');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7950, 0, 'دايموند للمن ونقل الموال', '333184882');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7951, 0, 'ايمرجنج مركتس بايمنتس (network international Africa)', '205119670');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7952, 0, 'شركه ديفولبرز للتدريبوالاستشارات', '279109512');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7953, 0, 'مكميلان وودز العالميه هشامجمال محمد ابراهيم', '548321434');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7954, 0, 'شركه جلوبال نولدج', '261991876');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7955, 0, 'شركة مطابع المقاولون العرب ', '205049427');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7956, 0, 'فبروسكوب للخدمات الطبيه', '478244257');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8315, 0, 'مستشفى هارت بليس لرعاية وعلاج امراض القلب', '377128880');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8316, 0, 'الشركة الدولية كيرمينا للنعاينات و التقييم و اعمال الخبرة', '483705896');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8317, 0, 'احمد صبري عبدالعزيز عليوشريكته', '337707162');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8318, 0, 'مركز المتحدون للاشعة التشخيصية', '450079643');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8319, 0, 'جامعة مصر للعلوم والتكنولوجيا', '200206877');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8320, 0, 'مركز الخبراء لتنظيمالمؤتمرات', '538003308');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8321, 0, 'مركز الشروق للأشعة', '315985216');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8322, 0, 'الخليج للتدريب ', '205167373');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8323, 0, 'ميدل است تريدنج ', '452801508');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8324, 0, ' شركة ليدز للمقاولات العمومية', '232820165');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8325, 0, 'محمد عبدالمنعم محمد عطيه', '509197876');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8326, 0, 'احمد م ابوالفتوح كامل', '592214206');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8327, 0, 'شركه بدر للاعلان', '200604619');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8328, 0, 'المين للنظمه التكنولوجيه', '200145525');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8329, 0, ' حمدى احمد السيد شلبي ', '200482572');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8330, 0, 'هيف لخدمات تكنولوجيا المعلومات ', '672816768');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8142, 0, 'جونسون كونترولز ايجيبتليمتد', '200143042');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8143, 0, 'الشركة المصرية للتكنولجيا المالية', '728745070');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8144, 0, 'شركه الصفاء للادواتالكهربائيه', '244135142');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8145, 0, 'محمود صلاح الدين صفوت المتحدون للمحماه', '489388167');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8146, 0, 'فوالا الفرنسيه لصناعهالشيوكولاته والحلويات', '467252963');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8147, 0, 'كي ستون', '377036684');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8148, 0, 'البصيرة للخدمات الطبية', '487209478');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8149, 0, 'المجموعه العربيه للتقييموالاستشارات اجاك', '200802453');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8150, 0, 'سمير عبدالفتاح بدر', '100402240');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8151, 0, 'الشركه الاسلاميه لتوريداتشبكات المياه محمد سيد', '244074526');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8152, 0, 'بى ال تى روسال هيلث لل+D13:D23رعاية الطبية وقسطرة القلب - مركز لايف كاث', '516263404');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8153, 0, 'محمود سعد عبدالكريم على', '286547406');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8154, 0, 'الشركة المصرية التجارية اتوموتيف', '100070558');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8155, 0, 'د/ مرفت عبدالرحمن محمد عثمان - مركز الرحمن للعلاج الطبيعى', '555307433');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8156, 0, 'رياضكو 2000', '339748192');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8157, 0, 'مصر كابيتال لتقييمالمشروعات واداره الاصول', '347141552');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8158, 0, 'فندق سونستا ', '100331793');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8159, 0, 'هدي فكري محمد الجمل', '503749613');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8160, 0, 'ايهاب عبدالرؤوف عبدالجوادعميره وشركاه', '100363113');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8161, 0, 'محمد حامد شاكر فرج- يو تو اوتو', '381946959');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8162, 0, 'رامز رضا محمود مصطفي', '281259917');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8163, 0, 'شركه المختبرات الطبيهالمصريه ايجي لاب', '205120830');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8164, 0, 'د - محمد على محمد رشاد حسن ', '418755485');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8165, 0, 'سورس ميديا (جريدة المصدر) ', '554712342');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8166, 0, 'egyptian computer environments', '100292771');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8167, 0, 'شركه سي اس ار ايجيبت', '493777075');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8168, 0, 'شركه موتور سيتى حسنى غريانى وشركاه', '100467075');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8169, 0, 'معرض / العصام موتورز - عصام عب', '245148507');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8170, 0, 'ايه جى سى اوتو للسيارات ', '542966344');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8171, 0, 'فندق بيروت اولاد كيرلسعبدالشهيد', '290495733');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8172, 0, 'طارق عبدالمنعم احمدالسروري وشركاه', '449950549');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8173, 0, 'شركة سيكونس للاتصالات ', '367044803');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8174, 0, 'انور عبدالمعطي محمودالجمال', '100008046');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8175, 0, 'اجرو جولد', '284210536');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8176, 0, 'مركز الحياة للعيون', '640680356');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8177, 0, 'معرض اوتو كروس', '438257707');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8178, 0, 'Tactics', '376978538');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8179, 0, 'شركه فرست لب', '205153550');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8180, 0, 'احمد محمد عبد الواحد', '999887778');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8181, 0, 'الطبية العربية الدولية - مستشفى دار الفؤاد م.نصر', '204900182');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8182, 0, 'لبوار للمحلت السياحيه', '100295584');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8183, 0, 'الشركة المصرية لتصنيع وسائل النقل -غبور مصر', '200016148');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8184, 0, 'لينوكس بلس لنظمالمعلومات', '200252844');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8185, 0, 'مركز نور العيون التخصصي', '318454696');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8186, 0, 'سمير فايز سالم ابوالنيلوشركاه', '100293271');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8187, 0, 'د- ابراهيم محمد الشحات جادو ابراهيم', '463657470');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8188, 0, 'د / عمرو محمد على عبدالعاطى عامر', '413606007');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8189, 0, 'منال محمد علي محمد', '667038086');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8190, 0, 'مركز الطاهره للاشعه', '220149798');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8191, 0, 'المجموعة الهندسية للاعمال الكهروميكانيكية', '472714805');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8192, 0, 'الاتحاد للتفتيش و اعمال الخبرة-يونسبكت ', '202488462');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8193, 0, 'Oracle', '200140116');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8194, 0, 'النبا الوطني للنشر', '200126350');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8195, 0, 'دار النظم والتكنولوجياالمتقدمه كويست', '212861298');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8196, 0, 'م محمود م سليم)سونستا ', '489256546');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8197, 0, 'سامح توفيق عبدالرحمن عليوشركاه', '286256630');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8198, 0, 'السبع للتجارة و التوكيلات', '367041332');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8199, 0, 'كورينوفا ميديكال سيتي كاثللخدمات الطبيه', '477189474');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8200, 0, 'مصطفى عبدالمنعم عبدالحميد وشريكته', '298621177');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8201, 0, 'Infotec Systems', '535292864');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8202, 0, 'بلاتفورم لإدارة المشروعات ', '552490008');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8203, 0, 'عبدالسلم م عبدالقادروشركاه', '288785029');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8204, 0, 'الهندسية لتكنولوجيا التيار الخفيف', '347604102');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8205, 0, 'سهير عبدالرحمن عبدالرحمنابوعوف', '479346984');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8206, 0, 'د/ ابراهيم ابراهيم ابراهيمبصيله', '486707563');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8207, 0, ' الخبير المثمن خالد بن الوليد ', '219973997');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8208, 0, 'مستشفى ويدج جروب للخدمات الطبية', '450354504');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8209, 0, 'ابراهيم سيد سيد منصورالغنام', '323424112');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8210, 0, 'د- محمد عبد الله كمال عبد المقصود غانم', '340547979');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8211, 0, 'البدر التخصصي', '200160435');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8212, 0, 'د / أحمد جمال على السيد', '411773070');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8213, 0, 'العلا لتجارة السيارات / اسماء ', '208073426');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8214, 0, 'اصواف مارينو طارق وحساماحمد عبدالقادر', '298504650');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8215, 0, 'الكمونى للالات الزراعيه و السيارات', '200203274');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8216, 0, 'شركة طيبة للاستشارات', '291454984');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8217, 0, 'احمد محمد عبدالعال جاويش ', '240177894');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8218, 0, 'الشرق الاوسط للإستثمار ', '262157446');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8219, 0, 'مختار محروس محمد', '413806723');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8220, 0, 'ورثة بدوي احمد ابو عيطة', '220176159');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8221, 0, 'شركة مستشفى النهار التخصصى ', '672636549');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8222, 0, 'محمد حسني حمزه ابراهيم', '219729654');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8223, 0, 'سمير السيد عبدالحميد الريان', '431567727');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8224, 0, 'مؤسسة الأهرام', '100534287');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8225, 0, 'محمد صلاح مصطفيوشريكه', '403890144');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8226, 0, 'د / جاد راغب عبدالباقى', '219693382');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8227, 0, 'د -إسماعيل إبراهيم إسماعيل إبراهيم الخشاب', '220033080');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8228, 0, 'العبور للخدمات الطبية والتعليمية - مستشفى فريد حبيب', '333057678');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8229, 0, 'مجدي كمال اسكندر متري', '220134979');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8230, 0, 'WIS - شركة دابليو اي اس وي انتجريت سوليوشنز', '537583319');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8231, 0, 'فيفيان سمير صليب عبدالنور - صيدلية زكرى', '291115845');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8232, 0, 'سوبريما لتكنولوجيا المعلومات ', '200250590');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8048, 0, 'العالمية لمهمات المكاتب ', '318304457');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8049, 0, 'مستشفي المركزالعربي الطبي', '200105434');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8050, 0, 'مؤسسه اخبار اليوم قطاعالاعلانات', '200015877');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8051, 0, 'نبيله محمود الفيض - مستشفى د/ عبدالقادر فهمى', '412666936');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8052, 0, 'شركة تراست للمقاولات والديكور', '462739744');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8053, 0, 'مستشفي العاصمه التخصصي', '261647598');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8054, 0, 'شركه الليثى اوتو جروب', '421159723');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8055, 0, 'امين كمال اسماعيل علي', '474234606');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8056, 0, 'مالك عبدالحميد نادي حسن', '484051970');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8057, 0, 'مركز النيل للاستشارات الفنية', '613723732');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8058, 0, 'تكنولوجيك للتجارة والتوكيلات ', '315200510');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8059, 0, 'د - مجدى زين العابدين على حسن', '295957069');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8060, 0, 'جريدة البرلمان الاقتصادى ', '247616222');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8061, 0, 'سمارت فيجن للتدريب', '339815523');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8062, 0, 'المختبر للتحاليل الطبية ', '205807054');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8063, 0, 'بزنس كاسيل للدعاية و الإعلان و تنظيم المعارض', '420968253');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8064, 0, 'شركة خدمات قيم الاعمال ', '567839028');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8065, 0, 'تطبيقات النظم المستقبليه', '735779708');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8066, 0, 'المنصور للسيارات منصور شيفورليه', '200146858');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8067, 0, 'اسيت تكنولوجي جروب', '212732005');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8068, 0, 'العاصمة للخدمات الطبية', '479865353');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8069, 0, 'مصارف', '542215446');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8070, 0, 'جاردنيا فلاورز', '723186707');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8071, 0, 'راية للخدمات الدولية ', '200114662');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8072, 0, 'كونسبت للشيكول والهداياباتشي', '200277847');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8073, 0, 'د- ياسر علي محمود احمد الشيخ  ', '219735832');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8074, 0, ' السيد على صديق حسن النجار - جريدة مال وأعمال  ', '481873260');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8075, 0, 'شركه جي تي ايفنتكس اندماركتنج سولوشنز', '523520115');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8076, 0, 'Sales experts  اسامة ابراهيم السيد ', '369822102');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8077, 0, 'المجموعة الفرنسية للخدمات الإستشارية ', '200293087');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8078, 0, 'حسني عبدالمحسن عليحسنين', '281307792');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8079, 0, 'ايجيبت للتوريدات العمومية EgyptFM', '450442853');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8080, 0, 'مركز سمارت سكان للاشعه', '474613410');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8081, 0, 'عزالدين ابراهيم حامد', '323761003');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8082, 0, 'شريف سعد عبدالعليم', '100412645');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8083, 0, 'جو للكمبيوتر ', '266858996');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8084, 0, 'تريد لاين ستورز', '254598064');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8085, 0, 'عصام الدين عبدالرحمنعبدالمجيد', '286504146');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8086, 0, 'مجلة اقتصاد بلدنا ', '540875996');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8087, 0, 'احمد ابوالعل عبدالعزيزحسن', '433347694');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8088, 0, 'عادل محمد كامل احمد', '677303904');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8089, 0, 'الترا فيجين للمشاريع الطبيه', '447944215');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8090, 0, 'برايم يونايتد', '290930162');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8091, 0, 'مؤسسه دار الهلال', '100350895');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8092, 0, 'د / ايناس سيد محمد السيد - مركز الجنزورى لامراض الكلى', '456127894');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8093, 0, 'ماهر يوسف متولي عبداللهالاسيوطي', '378731890');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8094, 0, ' اكواتك للمصاعد ', '506550427');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8095, 0, 'شركه اركيميد', '200082302');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8096, 0, 'جودة تكنولوجى', '518735435');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8097, 0, 'القطامية كلينك للخدمات الطبية ', '249766787');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8098, 0, 'شركه جوبزتلا للاستشاراتالعامه', '518663906');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8099, 0, 'وليد ابراهيم عبدالغنى عبدالمجيد سعد وشريكه', '523879814');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8100, 0, 'شركه بصمه للاستشارات', '640676456');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8101, 0, 'د- اشرف الخولي احمد محمد عثمان', '354141988');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8102, 0, 'د- ايناس سيد محمد السيد ( مركز الجنزوري لامراض الكلي)', '465127894');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8103, 0, 'دى سكويرز', '377146544');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8104, 0, 'شركه الشال للتجاره محمدالسيد يوسف الشال وشركاه', '203369580');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8105, 0, 'الفرج للاستيراد وتوريد ادوات النظافة', '217836259');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8106, 0, 'مركز العاصمة للجهاز الهضمى والكبد والمناظير - سامح محمد فخرى حنفى وشريكته', '674262468');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8107, 0, 'شركه Mnaria للتوريداتالعموميه', '305285685');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8108, 0, 'د- هشام صلاح الدين حمدي', '283778156');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8109, 0, 'شركة المنى لرعاية الطفولة - شركة المنى للخدمات الطبية ', '204900115');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8110, 0, 'شركه ليوناردذ جروب', '369418476');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8111, 0, 'د / رغدة سعيد عبدالحميد', '415969573');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8112, 0, 'فودافون', '205010725');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8113, 0, 'سيد فؤاد محمد عبد القوي و شريكه ( شركة ستارت )', '455132267');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8114, 0, 'احمد السيد حسين عوض', '494182210');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8115, 0, 'احمد م انور م', '209979372');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8116, 0, 'اشرف محمد محمد محمود', '215309324');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8117, 0, 'تويوتا مصر للتجارة ', '200214640');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8118, 0, 'جريدة الكترونى الاستثمار العربى الان ', '504533622');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8119, 0, 'مكتب الا ستشارات الفنيه', '200070096');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8120, 0, 'د - زايد عبدالرحيم محمد مهران ', '219967482');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8121, 0, 'سكاي نيوز', '252036077');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8122, 0, 'محمد دسوقى محمود', '411350749');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8123, 0, 'حابى للسياحة', '200164244');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8124, 0, 'هبه احمد لطفي محمد امامايكون كريشانز', '288833910');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8125, 0, 'امير خلف السيد اسماعيل', '674874005');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8126, 0, 'شركه سمارت هوم لترشيدالطاقه', '481708235');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8127, 0, 'اسماء محمد امام محمد', '401640884');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8128, 0, 'علامنا للحلول المتكاملة', '100330371');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8129, 0, 'شركه الجولف للفنادقوالمنشات السياحيه', '205002676');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8131, 0, 'محمود عبدالدايم معبدالحميد', '347015735');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8132, 0, 'شركة جنوب القاهرة لتوزيع الكهرباء', '210179457');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8133, 0, 'شركة فرانكلين كوفى مصر للتدريب', '450483959');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8134, 0, 'شركة ان ديزاين للاثاث المكتبي و الفندقي Indesign', '523885423');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8135, 0, 'محمد شريف شرف الدينوشركاه', '438212282');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8136, 0, 'حسام الدين فاروق ', '332606015');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8137, 0, 'شركه مور', '381453723');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8138, 0, 'شركه ميجا سوفت لتكنولوجياالمعلومات مصر', '205155626');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8139, 0, 'شمس الدين عبدالغفار م', '410352799');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8140, 0, 'دايركت اف ان لنشرالمعلومات عن الاوراقالماليه', '315378603');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8141, 0, 'مستشفى الصفوة التخصصى - المركز الطبى التخصصى', '262315513');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7957, 0, 'محمد طلعت احمد اسماعيل وشركاه راديو طلعت', '100273475');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7958, 0, 'د/ طارق حسانين عبد العزيزمنصور ابو النصر', '999887794');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7959, 0, 'DATA GEAR ', '257138862');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7960, 0, 'امام منصور امام سعيدوشركاه', '271773537');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7961, 0, 'العالمية للتجارة والاتصالات', '305621939');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7962, 0, 'د/ طه علي بيومي علي', '532474066');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7963, 0, 'city telecom سيتي تيليكوم', '328084247');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7964, 0, 'دار حورس للطباعة ', '679112618');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7965, 0, 'ايماك لتصنيع الحسابات', '210914467');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7966, 0, 'اعمار مصر للتنمية (Al Alamein Hotel)', '218287208');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7967, 0, 'المهندسون الاستشاريون العرب محرم باخوم ', '257184104');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7968, 0, 'شركة نجارة المبانى العصرية موبيكا', '212677780');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7969, 0, 'اس دابليو جروب ايجيبت - SW group Egypt', '535827393');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7970, 0, 'وائل منير جرجس مسيحه', '220214603');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7971, 0, 'رانيا طلعت محمد عفيفي', '463768108');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7972, 0, ' شركة بلدنا للمحتوي الالكتروني ( جريدة بلدنا نيوز الاقتصادية ) ', '533613108');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7973, 0, 'عزيزه نورالدين محمدوشركاه', '315376155');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7974, 0, 'شركه المركز الاستشاريللشبكيه و الجسم الزجاجي', '369667425');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7975, 0, 'محمد زكي محمد عطاالشريف', '322761255');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7976, 0, 'المصنع الفني ', '471559385');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7977, 0, 'عماد سيد سعد علي حجازي', '310968593');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7978, 0, 'شركة ميكروسوفت ايجيبت', '200143573');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7979, 0, 'د- ايمن محمد عبد العزيز حسن', '486635600');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7980, 0, 'ابراهيم احمد ابراهيم وشركاهالصفا والمروه للمقاولات', '100429491');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7981, 0, 'الشركه المصريه للشبكات', '205010393');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7982, 0, 'د / محمد اشرف لطفى الوكيل', '305240994');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7983, 0, 'د - هاله سعد حليم تادرس', '219558442');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7984, 0, 'كنترول للهندسة و التجارة ', '239960238');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7985, 0, 'رويال هاوس للتجاره', '200199315');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7986, 0, 'ريل هاندز اون', '506186644');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7987, 0, 'ايمان محمود محمد الواصلي (مجلة عالم الاقتصاد - عالم رجال الاعمال )', '325227330');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7988, 0, 'شركه دي ميديا ايجنسيللدعايه والعلن', '531029530');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7989, 0, 'المهندسون المتحدون للهندسة و التجارة ', '508719097');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7990, 0, 'سمير فهيم غبور مرجان وشركاه', '227547853');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7991, 0, ' مكتب إسماعيل محمد اسماعيل ', '408842210');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7992, 0, 'شركة جلوبال ارت ', '506763587');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7993, 0, 'شركه فرنشايز تيمللاستشارات', '528813080');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7994, 0, 'الشركه الاهليه للتوريداتالهندسيه فريد ابراهيم', '100056318');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7995, 0, 'برفكت للخدمات المالية ', '486877760');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7996, 0, 'شركة البطل الرومانى للتجارة والاستيراد', '489512976');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7997, 0, 'MAP Advertising', '200223259');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7998, 0, 'دوكسبرت للحلول المتكاملة', '562907815');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7999, 0, 'مجلة البيت الكبير(بيزنس كلاس)', '200005308');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8000, 0, 'زاوركس للمؤتمراتوالحفلات', '555373835');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8001, 0, 'استاند باي للدعايه والعلن', '553108557');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8002, 0, 'د - عمر أحمد على حسن', '464270103');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8003, 0, 'شركه النخيلي اخوان عيادنصرا ابراهيم وشركاه', '100399541');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8004, 0, 'سعيد لبيب سعد يوسف وشركاه ', '215424328');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8005, 0, 'محمود محمد محمد عطية رئيس مجلس إدارة مؤسسة دنيا العرب', '337356815');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8006, 0, 'ماستر سرفيس للخدمات البنكية', '726107455');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8007, 0, 'سى أتش جى للمستشفيات ', '569972663');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8008, 0, 'د / محمد تيسير محمد صادق', '283520426');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8009, 0, 'عاطف عجيب جورجي', '271212365');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8010, 0, 'الابيض اوتوموتيف لتجاره السيارات', '432950923');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8011, 0, 'Smart Technology', '611234998');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8012, 0, 'انتراكت', '259463906');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8013, 0, 'د/ اشرف لطفي عبداللطيف -عياده الجمل للسنان', '535885695');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8014, 0, 'جمال م عبدالوهابالسبكي', '259577855');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8015, 0, 'شركة رواد تصحيح الأبصار', '303274603');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8016, 0, 'VISION ARCHITECTS', '331900092');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8017, 0, 'زنكوغراف الحامولي', '231862954');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8018, 0, 'د / محمد ابراهيم محمد محمد حجازى', '448085178');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8019, 0, 'د / هشام محمد فرج  السيد التهامى', '252237544');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8020, 0, 'كايرو كابيتال - حسام حسممحمود الروبى وشريكه', '672869020');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8021, 0, 'المركز المصري للكليوالمسالك البوليه', '465048145');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8022, 0, 'شركة فرحات', '521179289');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8023, 0, 'سبارك تكنولوجى', '463409337');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8024, 0, 'ام جى لتنظيم المؤتمرات ', '484207369');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8025, 0, 'شريف محمود احمد عارف', '254958117');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8026, 0, 'اكرم عياد شفيق جبرائيل', '259630748');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8027, 0, 'حسين للتوريدات الكهربائية', '210923253');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8028, 0, 'شركه ماجيك للاعمالالهندسيه مجدي سيد حسنوشريكه', '412629100');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8029, 0, 'التوريدات العموميه العالميهجيسكو', '100273289');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8030, 0, 'احمد عبده عبده البقرى', '413430693');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8031, 0, 'عمرو احمد جاد احمد', '219988870');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8032, 0, 'طارق عبدالعزيز البلتاجي', '337167249');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8033, 0, 'الشركه المصريه للتجاره والتوكيلات أبو حتة مصر', '200006223');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8034, 0, 'مركز الدراسات التخطيطيه والمعماريه', '320121720');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8035, 0, 'د / عماد مصطفى جابر ابراهيم ', '486624692');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8036, 0, 'وحيد رشاد السيد أبو الدهب', '508452758');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8037, 0, 'جورج عزت بساده وشركاهشركه سان جورج للاستيرادوالتصدير', '239146638');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8038, 0, 'د- عصام عبدالعظيم عبدالرحمن جاد - مركز دلتا للعلاج الطبيعى', '547408307');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8039, 0, 'شركة الشال للتجارة محمد السيد', '596595662');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8040, 0, 'محمد رشاد عطيه ابراهيم', '286468689');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8041, 0, 'اوتو زد جروب', '711528417');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8042, 0, 'هشام عبدالله ابراهيم غانم', '413076180');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8043, 0, 'بى اوتو لتجارة السيارات', '286586495');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8044, 0, 'علاء عبدالعزيز بكري (مجلهاربيان بزنس مصر', '518654923');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8045, 0, 'مستشفى فيفا مدينة نصر - مستشفى فيفا العقاد', '413599302');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8046, 0, 'هناء جوده احمد مصطفي', '440539862');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (8047, 0, 'المؤسسة الفنية لمقاومة الافات', '331867516');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7674, 0, 'محمد عبد الرحمن محمد حسين و شركاه', '217126405');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7675, 0, 'خالد رمضان صالح دواد -مؤسسه كلاش فاشون', '508926505');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7764, 0, 'احمد ابراهيم عبدالباريعبدالباسط', '320683737');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7765, 0, 'ياسين احمد عبدالرحمنوشركاه', '410498173');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7766, 0, 'شكري جورجي حنينوشركاه', '286137909');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7767, 0, 'شركه الطارق اوتو تريد', '310317096');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7676, 0, 'شندي ومشاركوه', '200110179');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7677, 0, 'د- وليد السيد عبد العليم الشبراوي', '376838132');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7678, 0, ' المصرية الالمانية لمهمات المكاتب ', '100272827');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7679, 0, 'عمرو عبد العال السيد محمد( مجلة الإستدامه والتموي', '511617194');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7680, 0, 'بيكون كونسالتينج ', '547773617');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7681, 0, 'شركه كومكس للمنتجاتالورقيه', '542991985');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7682, 0, 'شركة الانصاري لاب', '374582556');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7683, 0, 'مؤسسة الشروق للتجارة و التوريدات', '215243929');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7684, 0, 'محمد احمد محمود حامد', '455226954');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7685, 0, 'كلاودز للحلول المتكاملة Clouds integrated solutions', '261748378');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7686, 0, 'كونتريد للمقاولات', '327595310');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7687, 0, 'هيثم عادل راتب حسن', '552429644');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7688, 0, 'مكتبة سامى', '217702155');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7689, 0, 'ان فورت', '205111696');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7690, 0, 'محمد محمود عبدالحكيمعبدالحميد', '378969056');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7691, 0, 'بريميم للتوريدات العمومية', '508617170');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7692, 0, 'احمد كمال محمد', '713525525');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7693, 0, 'محمد قطب ابراهيم محمد', '337012369');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7694, 0, 'محمد وهبه محمد حافظ', '100273777');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7695, 0, 'شركة بي او دي ايجيبت للدعاية والإعلان ', '499273028');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7696, 0, 'اوتو ماركت للتجاره الغلبان جروب', '435473506');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7697, 0, 'فاضل عمر فوزي حسن عمر', '437288714');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7698, 0, 'احمد سعدالدين محمد مراد', '342421530');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7699, 0, 'جريدة الاخبارية ', '237275627');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7700, 0, 'صبحى بشاى عفاشه - صيدلية المماليك', '319804755');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7701, 0, 'احمد السيد محمود هلل', '303495618');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7702, 0, 'شركه مستشفي الصفوه', '205168590');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7703, 0, 'غاده صلح زين العابدينجمعه', '220220999');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7704, 0, 'جمعيه المعلومات ودعمالقرار', '413689468');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7705, 0, 'إيست نتس ', '308477316');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7706, 0, 'شركة الكوثر الخليج ', '484191160');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7707, 0, 'ايليت اوتو -محمد توفيق احمدمحمد', '420944052');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7708, 0, 'اون تايم للاستشارات المالية والتحصيل', '552489956');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7709, 0, 'سمير محمد محمود علي', '219897832');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7710, 0, 'فرست رانك للتكنولوجيا المالية First Rank', '726342246');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7711, 0, 'ميديا جولد للدعاية والاعلان', '725759178');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7712, 0, 'علي امين عبدالحليم حسن', '274061708');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7713, 0, 'ياسر احمد غريب عبدالحميدوشركاه', '347233163');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7714, 0, 'المصرية السعودية للرعاية الصحية (السعودي الالماني)', '200495704');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7715, 0, 'داينامكس للتجاره عماد حلمى عزيز وشريكه', '288189507');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7716, 0, 'د / هانى يسى برسوم جرجس', '713516097');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7717, 0, 'شركة بناء للتشييد وادارة المشروعات', '227572114');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7718, 0, 'المجموعة الاستشارية كونكورد', '220060495');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7719, 0, 'الامين للإستيراد و التصدير ', '679415920');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7720, 0, 'احمد سعد عيد عبدالحفيظ', '473024128');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7721, 0, 'مكتب المهندس الاستشارى حسين صبور ', '219934908');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7722, 0, 'جريده الوفد', '200031880');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7723, 0, 'احمد خليل عبدالحميد احمدوشركاه', '411498908');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7724, 0, 'مكتبة حسام الدين', '244074542');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7725, 0, 'د / نور عبدالمقصود على عبدالله', '376504013');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7726, 0, 'نوران اسامه احمد محمدالعزيزي', '476232139');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7727, 0, 'المجموعة التجارية والصناعية السعود جروب ', '592020054');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7728, 0, 'شركة الشمول للتدريب', '730728412');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7729, 0, 'فادية نظير حبيب - صيدلية شقرا', '217838715');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7730, 0, 'الجلا لتشغيل المصوغات الذهبية ', '464959764');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7731, 0, 'جورج سعيد شنوده وشركاه', '100071139');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7732, 0, 'د- شريف محمد السيد العوضي', '350044023');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7733, 0, 'د/ احمد م عبد الرحيمالسبع', '499387716');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7734, 0, 'نبض الحياة لادارة المستشفيات والتوريدات', '433164220');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7735, 0, 'Africana Advertising', '411852159');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7736, 0, 'خير جروب ', '714522716');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7737, 0, 'علاء الدين عبدالفتاح محمد البرماوى ', '518860213');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7738, 0, 'مجموعه المهندسين المتحدين', '202550338');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7739, 0, 'معرض ريتشكار طارق رفاعى وشركاه', '567159116');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7740, 0, 'أحمد عادل محمد لطفى (شركة ثينك اكسبو)', '327387122');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7741, 0, 'مختار محروس محمد محفوظ', '349340692');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7742, 0, ' شركة اكسلانت دي اند ان  ', '723456003');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7743, 0, 'الشركة الدولية لاعمال التقييم و التحصيل', '239872894');
Insert into INVESTORS
   (ID, VERSION, NAME, TAXES_NUMBER)
 Values
   (7744, 0, 'طارق زاهر عبدالصمد محمد', '244073260');
COMMIT;


SET DEFINE OFF;
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('070', '56 شارع نزية خليفة مصر الجديدة,القاهرة,مصر', '3', '56 شارع نزية خليفة مصر الجديدة', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('300', '31 شارع الجمهوربة والفرات,بور سعيد,مصر', '3', '31 شارع الجمهوربة والفرات', 'بور سعيد', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('200', 'رقم 16 شارع سيزوستريس,الاسكندرية,مصر', '3', 'رقم 16 شارع سيزوستريس', 'الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('210', 'رقم 5 شارع صلاح سالم,الاسكندرية,مصر', '3', 'رقم 5 شارع صلاح سالم', 'الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('055', '20 شارع طلعت حرب,القاهرة,مصر', '3', '20 شارع طلعت حرب', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('100', '76 شارع شبرا,القاهرة,مصر', '3', '76 شارع شبرا', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('600', '9 شارع همدان من  مراد,الجيزة,مصر', '3', '9 شارع همدان من  مراد', 'الجيزة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('130', '17 ميدان الظاهر,القاهرة,مصر', '3', '17 ميدان الظاهر', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('090', '44 شارع رمسيس - رمسيس,القاهرة,مصر', '3', '44 شارع رمسيس - رمسيس', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('080', '271 شارع بورسعيد  - السيدة زنيب,القاهرة,مصر', '3', '271 شارع بورسعيد  - السيدة زنيب', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('110', '925 شارع كورنيش النيل,القاهرة,مصر', '3', '925 شارع كورنيش النيل', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('160', ' 112 شارع السبتية,القاهرة,مصر', '3', ' 112 شارع السبتية', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('140', '11 شارع على روبى - روكسى,القاهرة,مصر', '3', '11 شارع على روبى - روكسى', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('120', '6 شارع عزت بدوى حافظ - السواح,القاهرة,مصر', '3', '6 شارع عزت بدوى حافظ - السواح', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('190', ' ناصية شارعى سمير فرحات و,عبد الحميد لطفى -مدينة نصر,القاهرة -مصر', '3', ' ناصية شارعى سمير فرحات و', 'عبد الحميد لطفى -مدينة نصر', 
    'القاهرة -مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('180', '36 شارع مصطفى فهمى - حلوان,القاهرة,مصر', '3', '36 شارع مصطفى فهمى - حلوان', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('170', '41 شارع نوبار ناصية نوبار والشيخ,ريحان - نوبار - القاهرة,مصر', '3', '41 شارع نوبار ناصية نوبار والشيخ', 'ريحان - نوبار - القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('072', '13 شارع ابراهيم اللقانى مصر الجديدة,القاهرة,مصر', '3', '13 شارع ابراهيم اللقانى مصر الجديدة', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('073', '26 شارع ابراهيم اللقانى مصر الجديدة,القاهرة,مصر', '3', '26 شارع ابراهيم اللقانى مصر الجديدة', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('605', 'رقم 1شارع مراد,ميدان الجيزة - الجيزة,مصر', '3', 'رقم 1شارع مراد', 'ميدان الجيزة - الجيزة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('570', 'شارع مصطفى كامل,البحيرة,مصر', '3', 'شارع مصطفى كامل', 'البحيرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('481', 'شارع الجمهورية,البحيرة,مصر', '3', 'شارع الجمهورية', 'البحيرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('580', 'مدينة الشروق مدخل 2,بجوار شركة الكهرباء,القاهرة', '3', 'مدينة الشروق مدخل 2', 'بجوار شركة الكهرباء', 
    'القاهرة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('610', 'رقم 7 شارع الحرية - بندر الفيوم,الفيوم,مصر', '3', 'رقم 7 شارع الحرية - بندر الفيوم', 'الفيوم', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('655', 'ش صلاح سالم,طهطا,سوهاج', '3', 'ش صلاح سالم', 'طهطا', 
    'سوهاج', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('611', 'شارع ابوبكر الصديق,الفيوم,مصر', '3', 'شارع ابوبكر الصديق', 'الفيوم', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('585', 'المنطقة الصناعية   العبور,القاهرة,مصر', '3', 'المنطقة الصناعية   العبور', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('260', 'رقم 78شارع عبدالسلام عارف,جليم -الاسكندرية,مصر', '3', 'رقم 78شارع عبدالسلام عارف', 'جليم -الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('230', 'رقم 226 شارع بورسعيد سبورتنج,الاسكندرية,مصر', '3', 'رقم 226 شارع بورسعيد سبورتنج', 'الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('220', 'رقم 1شارع حجر النواتية,باكوس- الاسكندرية,مصر', '3', 'رقم 1شارع حجر النواتية', 'باكوس- الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('290', '16 شارع الاقبال      فكتوريا,الاسكندرية,مصر', '3', '16 شارع الاقبال      فكتوريا', 'الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('250', 'اول شارع البيطاش طريق مرسى مطروح,الاسكندرية,مصر', '3', 'اول شارع البيطاش طريق مرسى مطروح', 'الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('280', 'شارع ملك حنفى طريق ابوقير,المنتزة- الاسكندرية,مصر', '3', 'شارع ملك حنفى طريق ابوقير', 'المنتزة- الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('270', 'رقم 36 شارع عمانويل,سموحة - الاسكندرية,مصر', '3', 'رقم 36 شارع عمانويل', 'سموحة - الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('255', 'مدينة برج العرب منطقة البنوك,الاسكندرية,مصر', '3', 'مدينة برج العرب منطقة البنوك', 'الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('201', 'الورديان,الاسكندرية,مصر', '3', 'الورديان', 'الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('330', '17 شارع الجمهورية,بور فؤاد,مصر', '3', '17 شارع الجمهورية', 'بور فؤاد', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('320', '7 شارع الشهيدفريد ندا,الاسماعيلية,مصر', '3', '7 شارع الشهيدفريد ندا', 'الاسماعيلية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('560', 'السوق التجارى المجاورة 12,الشرقية,مصر', '3', 'السوق التجارى المجاورة 12', 'الشرقية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('310', '1  شارع التحرير,السويس,مصر', '3', '1  شارع التحرير', 'السويس', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('672', 'منطقة المنياء بجوار مبنى السنترال,البحر الاحمر,مصر', '3', 'منطقة المنياء بجوار مبنى السنترال', 'البحر الاحمر', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('730', 'طريق الشيراتون - المطار,البحر الاحمر,مصر', '3', 'طريق الشيراتون - المطار', 'البحر الاحمر', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('340', 'عمارة رقم46 منطقة ميناء نويبع,جنوب سيناء,مصر', '3', 'عمارة رقم46 منطقة ميناء نويبع', 'جنوب سيناء', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('101', '92 شارع خلوصى,القاهرة,مصر', '3', '92 شارع خلوصى', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('035', '3 شارع دانش - العباسية,القاهرة,مصر', '3', '3 شارع دانش - العباسية', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('111', '3 شارع 79 المعادى,المعادى,مصر', '3', '3 شارع 79 المعادى', 'المعادى', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('112', 'ناصية شارعى فلسطين و269 المتفرع من,شارع النصر- القاهرة,مصر', '3', 'ناصية شارعى فلسطين و269 المتفرع من', 'شارع النصر- القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('030', '19 شارع عدلى,القاهرة,مصر', '3', '19 شارع عدلى', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('415', 'شارع اللواء عبد العزيز على,الشرقية,مصر', '3', 'شارع اللواء عبد العزيز على', 'الشرقية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('592', 'شارع سعد زغلول,الشرقية,مصر', '3', 'شارع سعد زغلول', 'الشرقية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('625', '17 ش الابراهيمية,ببا,بنى سويف', '3', '17 ش الابراهيمية', 'ببا', 
    'بنى سويف', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('420', '79  شارع الجيش,الدقهلية,مصر', '3', '79  شارع الجيش', 'الدقهلية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('593', '221 شارعه الجمهورية,الدقهلية,مصر', '3', '221 شارعه الجمهورية', 'الدقهلية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('095', '37,26 OF JULY ST.,CAIRO,EGYPT', '3', '37,26 OF JULY ST.', 'CAIRO', 
    'EGYPT', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('430', 'شارع احمد عرابى,الدقهلية,مصر', '3', 'شارع احمد عرابى', 'الدقهلية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('422', 'مبنى مجمع المصالح  الحكومية,الدقهلية,مصر', '3', 'مبنى مجمع المصالح  الحكومية', 'الدقهلية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('424', 'شارع صلاح سالم,الدقهلية,مصر', '3', 'شارع صلاح سالم', 'الدقهلية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('423', '44 شارع الثورة,الدقهلية,مصر', '3', '44 شارع الثورة', 'الدقهلية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('520', 'شارع سامى شلباية,الدقهلية,مصر', '3', 'شارع سامى شلباية', 'الدقهلية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('421', 'شارع الجيش,الدقهلية,مصر', '3', 'شارع الجيش', 'الدقهلية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('440', 'شارع الجلاء,دمياط,مصر', '3', 'شارع الجلاء', 'دمياط', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('445', 'القطعة رقم 32,دمياط,مصر', '3', 'القطعة رقم 32', 'دمياط', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('450', '53  شارع الجيش,غربية,مصر', '3', '53  شارع الجيش', 'غربية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('460', '12 شارع 23 يوليو,غربية,مصر', '3', '12 شارع 23 يوليو', 'غربية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('452', 'شارع الخان,غربية,مصر', '3', 'شارع الخان', 'غربية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('451', 'ميدان المحطة,غربية,مصر', '3', 'ميدان المحطة', 'غربية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('360', 'هضبة ام السيد قطعة رقم9  منطقة البن,جنوب سيناء,مصر', '3', 'هضبة ام السيد قطعة رقم9  منطقة البن', 'جنوب سيناء', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('350', 'ناصية شارع الشباب والرياضة,شمال سيناء,مصر', '3', 'ناصية شارع الشباب والرياضة', 'شمال سيناء', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('400', '1شارع جميل,القليوبية, مصر', '3', '1شارع جميل', 'القليوبية', 
    ' مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('401', 'شارع العاشر من رمضان,القليوبية,مصر', '3', 'شارع العاشر من رمضان', 'القليوبية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('402', 'شارع جمال عبد الناصر,القليوبية,مصر', '3', 'شارع جمال عبد الناصر', 'القليوبية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('410', '5 شارع احمد عرابى,الشرقية,مصر', '3', '5 شارع احمد عرابى', 'الشرقية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('412', 'شارع بور سعيد     مجمع المصالح ا,الشرقية,مصر', '3', 'شارع بور سعيد     مجمع المصالح ا', 'الشرقية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('414', 'شارع الساحة الشعبية,الشرقية,مصر', '3', 'شارع الساحة الشعبية', 'الشرقية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('151', '18 شارع عمار بن ياسر,القاهرة,مصر', '3', '18 شارع عمار بن ياسر', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('602', 'رقم 72 شارع جامعة الدول العربية,المهندسين -الجيزة,مصر', '3', 'رقم 72 شارع جامعة الدول العربية', 'المهندسين -الجيزة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('020', 'قصرالنيل,القاهرة,مصر', '3', 'قصرالنيل', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('034', 'شارع وزارة الزراعة,الجيزة,مصر', '3', 'شارع وزارة الزراعة', 'الجيزة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('040', '6شارع بستان الدكة - الالفى,القاهرة,مصر', '3', '6شارع بستان الدكة - الالفى', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('045', '34شارع عبد الخالق ثروت,القاهرة,مصر', '3', '34شارع عبد الخالق ثروت', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('620', 'رقم 45شارع 23يوليو,بنىسويف,مصر', '3', 'رقم 45شارع 23يوليو', 'بنىسويف', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('150', 'مطار القاهرة الدولي,القاهرة,مصر', '3', 'مطار القاهرة الدولي', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('621', 'رقم 4شارع الشهيد ممدوح مصطفى,اهناسيا - بنى سويف,مصر', '3', 'رقم 4شارع الشهيد ممدوح مصطفى', 'اهناسيا - بنى سويف', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('740', 'شارع بورسعيد - سمسطا,بنى سويف,مصر', '3', 'شارع بورسعيد - سمسطا', 'بنى سويف', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('630', 'رقم 2 شارع سعد زغلول,المنيا,مصر', '3', 'رقم 2 شارع سعد زغلول', 'المنيا', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('635', 'شارع الجلاء - مبنى المهن الزراعية,ملوى - المنيا,مصر', '3', 'شارع الجلاء - مبنى المهن الزراعية', 'ملوى - المنيا', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('640', 'رقم 27 شارع الجمهورية - برج الفردوس,اسيوط,مصر', '3', 'رقم 27 شارع الجمهورية - برج الفردوس', 'اسيوط', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('650', 'رقم 26 شارع النيل,سوهاج,مصر', '3', 'رقم 26 شارع النيل', 'سوهاج', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('720', 'ناصية شارعى المحطة والسيدة خديجة,سوهاج,مصر', '3', 'ناصية شارعى المحطة والسيدة خديجة', 'سوهاج', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('670', 'شارع 23يوليو,قنا,مصر', '3', 'شارع 23يوليو', 'قنا', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('690', 'كورنيش النيل الاقصر,قنا,مصر', '3', 'كورنيش النيل الاقصر', 'قنا', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('540', 'شارع الجمهورية واالصاغة,غربية,مصر', '3', 'شارع الجمهورية واالصاغة', 'غربية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('490', '11 شارع الخلفاء الراشدين,كفر الشيخ,مصر', '3', '11 شارع الخلفاء الراشدين', 'كفر الشيخ', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('492', '1 شارع الخلفاء الراشدين,كفر الشيخ,مصر', '3', '1 شارع الخلفاء الراشدين', 'كفر الشيخ', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('491', 'شارع كورنيش النيل,كفر الشيخ,مصر', '3', 'شارع كورنيش النيل', 'كفر الشيخ', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('530', 'شارع جمال عبد  الناصر,كفر الشيخ,مصر', '3', 'شارع جمال عبد  الناصر', 'كفر الشيخ', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('510', 'شارع الجيش,المنوفية,مصر', '3', 'شارع الجيش', 'المنوفية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('472', 'العمارة السكنية مجلس مد ينة تلا,المنوفية,مصر', '3', 'العمارة السكنية مجلس مد ينة تلا', 'المنوفية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('471', 'شارع الكورنيش البر الغربى,المنوفية,مصر', '3', 'شارع الكورنيش البر الغربى', 'المنوفية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('591', 'شارع الجيش,المنطقة,مصر', '3', 'شارع الجيش', 'المنطقة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('155', 'طريق مطار القاهرة مبنى الشركة,المصرية للمطارات أمام مبنى وزارة ال,القاهرة', '3', 'طريق مطار القاهرة مبنى الشركة', 'المصرية للمطارات أمام مبنى وزارة ال', 
    'القاهرة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('074', '6 شارع محور طه حسين,النزهة الجديدة,القاهرة', '3', '6 شارع محور طه حسين', 'النزهة الجديدة', 
    'القاهرة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('656', 'مدينة مبارك الصناعية - حى الكوثر,سوهاج,مصر', '3', 'مدينة مبارك الصناعية - حى الكوثر', 'سوهاج', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('494', 'شارع الشهيد عبدالمنعم رياض,بيلا - كفر الشيخ,كفر الشيخ', '3', 'شارع الشهيد عبدالمنعم رياض', 'بيلا - كفر الشيخ', 
    'كفر الشيخ', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('188', '73 شارع سوريا,المهندسين,الجيزة', '3', '73 شارع سوريا', 'المهندسين', 
    'الجيزة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('010', 'رقم 6 ش مصطفي أبو زهرة مدينة نص, القاهرة,مصر', '3', 'رقم 6 ش مصطفي أبو زهرة مدينة نص', ' القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('325', 'الاسماعيلية,الاسماعيلية, مصر', '3', 'الاسماعيلية', 'الاسماعيلية', 
    ' مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('495', 'امام المطافى مدينةالحامول كفر الشيخ,الحامول,كفر الشيخ', '3', 'امام المطافى مدينةالحامول كفر الشيخ', 'الحامول', 
    'كفر الشيخ', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('595', 'المنطقة الصناعية الثالثة,مجمع مصر للطيران امام محطة كهرباء,شمال خلف جريش للزجاج العشر من رمضان', '3', 'المنطقة الصناعية الثالثة', 'مجمع مصر للطيران امام محطة كهرباء', 
    'شمال خلف جريش للزجاج العشر من رمضان', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('156', 'المبنى المطوربمجمع بضائع مصرللطيران,قرية البضائع خارج الدائرة الجمركية,مطار القاهرة - مصر', '3', 'المبنى المطوربمجمع بضائع مصرللطيران', 'قرية البضائع خارج الدائرة الجمركية', 
    'مطار القاهرة - مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('015', 'ناصية ش سمير فرحات وعبدالحميد لطفى,الحى الثامن - م نصر- القاهرة,مصر', '3', 'ناصية ش سمير فرحات وعبدالحميد لطفى', 'الحى الثامن - م نصر- القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('002', 'رقم 6 ش مصطفي أبو زهرة مدينة نص, القاهرة,مصر', '3', 'رقم 6 ش مصطفي أبو زهرة مدينة نص', ' القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('194', 'الوحدة 5و6 مول اميرالد تاون بلازا,منتجع النخيل امتداد زاكر حسين,التجمع الاول القاهرة', '3', 'الوحدة 5و6 مول اميرالد تاون بلازا', 'منتجع النخيل امتداد زاكر حسين', 
    'التجمع الاول القاهرة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('182', 'منطقة التوسعات الشرقية,خلف جولف بالم هيلز ب 6 اكتوبر,الجيزة', '3', 'منطقة التوسعات الشرقية', 'خلف جولف بالم هيلز ب 6 اكتوبر', 
    'الجيزة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('098', 'المحل التجارى رقم G1 بالطابق الارضى,مول ميدتاون م. المستثمرين الجنوبية,القاهرة الجديدة', '3', 'المحل التجارى رقم G1 بالطابق الارضى', 'مول ميدتاون م. المستثمرين الجنوبية', 
    'القاهرة الجديدة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('183', 'الوحدة 26 مول ميجا داندى مول الكيلو,28 طريق مصر الاسكندرية الصحراوى,مصر', '3', 'الوحدة 26 مول ميجا داندى مول الكيلو', '28 طريق مصر الاسكندرية الصحراوى', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('004', 'وحدة(G) رقم107مول مصر طريق الوا حات,امام مدينة الانتاج الاعلامي منطقة,التوسعات الشرقية الجيزة', '3', 'وحدة(G) رقم107مول مصر طريق الوا حات', 'امام مدينة الانتاج الاعلامي منطقة', 
    'التوسعات الشرقية الجيزة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('637', 'شارع الجلاء,القوصية,اسيوط', '3', 'شارع الجلاء', 'القوصية', 
    'اسيوط', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('614', 'ش التل الجديد من ش الجمهورية,ابشواى,الفيوم', '3', 'ش التل الجديد من ش الجمهورية', 'ابشواى', 
    'الفيوم', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('442', 'شارع التحرير,قسم اول دمياط,دمياط', '3', 'شارع التحرير', 'قسم اول دمياط', 
    'دمياط', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('425', 'شارع الثورة رياح المنصورة,مدينة اجا,الدقهلية', '3', 'شارع الثورة رياح المنصورة', 'مدينة اجا', 
    'الدقهلية', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('192', '44 ش عبدالرازق السنهورى,مدينة نصر اول,القاهرة', '3', '44 ش عبدالرازق السنهورى', 'مدينة نصر اول', 
    'القاهرة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('215', '554 تنظيم طريق الجيش سيدي بشر بحرى,المنتزة اول,الاسكندرية', '3', '554 تنظيم طريق الجيش سيدي بشر بحرى', 'المنتزة اول', 
    'الاسكندرية', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('036', '42 ش الرشيد - بولاق الدكرور,الجيزة,الجيزة', '3', '42 ش الرشيد - بولاق الدكرور', 'الجيزة', 
    'الجيزة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('645', 'قطعة رقم 32 كدستر بحوض ترعة,بنى حسين 11مكرر ش جمال عبد الناصر,منفلوط اسيوط', '3', 'قطعة رقم 32 كدستر بحوض ترعة', 'بنى حسين 11مكرر ش جمال عبد الناصر', 
    'منفلوط اسيوط', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('612', 'رقم 1 شارع بورسعيد - سنورس,الفيوم,مصر', '3', 'رقم 1 شارع بورسعيد - سنورس', 'الفيوم', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('615', '173 ش طراد النيل - مركز,مركز الواسطى,بنى سويف', '3', '173 ش طراد النيل - مركز', 'مركز الواسطى', 
    'بنى سويف', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('626', 'ش الجيش  مدينة الفشن,بنى سويف,بنى سويف', '3', 'ش الجيش  مدينة الفشن', 'بنى سويف', 
    'بنى سويف', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('616', '6 ش ابو بكر الصديق مركز ناصر,بنى سويف,بنى سويف', '3', '6 ش ابو بكر الصديق مركز ناصر', 'بنى سويف', 
    'بنى سويف', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('446', 'ش بورسعيد والجلاء, كفر سعد دمياط,دمياط', '3', 'ش بورسعيد والجلاء', ' كفر سعد دمياط', 
    'دمياط', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('619', 'مدينة بنى سويف الجديدة- الحى الأول,شرق النيل,بنى سويف', '3', 'مدينة بنى سويف الجديدة- الحى الأول', 'شرق النيل', 
    'بنى سويف', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('695', 'المنشية - ميدان صلاح الدين,مدينة الاقصر,الاقصر', '3', 'المنشية - ميدان صلاح الدين', 'مدينة الاقصر', 
    'الاقصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('078', '12 أ مدينة التوفيق,ش يوسف عباس م نصر,القاهرة', '3', '12 أ مدينة التوفيق', 'ش يوسف عباس م نصر', 
    'القاهرة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('305', '11 ش الجمهورية,تقاطع ش حافظ ابراهيم,بورسعيد', '3', '11 ش الجمهورية', 'تقاطع ش حافظ ابراهيم', 
    'بورسعيد', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('076', '79سابقا 81حاليا شارع المنتزة النزهة,مصر الجديدة القاهرة,مصر', '3', '79سابقا 81حاليا شارع المنتزة النزهة', 'مصر الجديدة القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('187', 'AUTO VILLE THE STRIP بمشروع A5,مدينة الشيخ زايد - 6 اكتوبر,الجيزة', '3', 'AUTO VILLE THE STRIP بمشروع A5', 'مدينة الشيخ زايد - 6 اكتوبر', 
    'الجيزة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('470', 'شارعى المدارس وبنك مصر,المنوفية,مصر', '3', 'شارعى المدارس وبنك مصر', 'المنوفية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('653', 'تقاطع ش التحرير مع عثمان بن عفان,البلينا,سوهاج', '3', 'تقاطع ش التحرير مع عثمان بن عفان', 'البلينا', 
    'سوهاج', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('355', 'جنوب سيناء,جنوب سيناء,جنوب سيناء', '3', 'جنوب سيناء', 'جنوب سيناء', 
    'جنوب سيناء', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('501', 'ش الاسكندرية - امام ش القسم,ميدان الغزالة-مدينة الحمام,مرسى مطروح', '3', 'ش الاسكندرية - امام ش القسم', 'ميدان الغزالة-مدينة الحمام', 
    'مرسى مطروح', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('265', '293 طريق الحرية اسبورتنج,قسم سيدى جابر حى شرق الاسكندرية,الاسكندرية', '3', '293 طريق الحرية اسبورتنج', 'قسم سيدى جابر حى شرق الاسكندرية', 
    'الاسكندرية', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('485', '4ش المحكمة تقاطع ش 22 برج الرضوان,امام الضرائب العقارية,البحيرة', '3', '4ش المحكمة تقاطع ش 22 برج الرضوان', 'امام الضرائب العقارية', 
    'البحيرة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('195', 'رقم 6 ش مصطفي أبو زهرة مدينة نص, القاهرة,مصر', '3', 'رقم 6 ش مصطفي أبو زهرة مدينة نص', ' القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('005', 'اسكندرية,اسكندرية,مصر', '3', 'اسكندرية', 'اسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('629', '9 ش الوحدة العربية,منشية المصرى-مغاغة,المنيا', '3', '9 ش الوحدة العربية', 'منشية المصرى-مغاغة', 
    'المنيا', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('663', 'شارع كورنيش النيل اسفل فندق فيلة,اسوان,مصر', '3', 'شارع كورنيش النيل اسفل فندق فيلة', 'اسوان', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('705', 'كورنيش النيل عمارة سلامة رشيدى,مدينة اسنا محافظة الاقصر,مصر', '3', 'كورنيش النيل عمارة سلامة رشيدى', 'مدينة اسنا محافظة الاقصر', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('193', 'مول سيتى ستارز الوحدة A151,مكرم عبيد م. نصر,القاهرة', '3', 'مول سيتى ستارز الوحدة A151', 'مكرم عبيد م. نصر', 
    'القاهرة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('657', 'ش السكة الحديدبجوار المزلقان البحرى,غرب محطة السكة الحديد مدخل المدينة,الناحية الغربية-سوهاج', '3', 'ش السكة الحديدبجوار المزلقان البحرى', 'غرب محطة السكة الحديد مدخل المدينة', 
    'الناحية الغربية-سوهاج', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('088', 'الوحدة108(G39) قطاعAالمنطقةالتجارية,السوق الشرقى الركن الشمالى الشرقى,امتداد الرحاب القاهرة الجديدة', '3', 'الوحدة108(G39) قطاعAالمنطقةالتجارية', 'السوق الشرقى الركن الشمالى الشرقى', 
    'امتداد الرحاب القاهرة الجديدة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('216', 'برج سرايا السيوف تقاطع محمدامين,حسونة مع ش عادل مصطفى السيوف,الاسكندرية', '3', 'برج سرايا السيوف تقاطع محمدامين', 'حسونة مع ش عادل مصطفى السيوف', 
    'الاسكندرية', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('404', 'محل 55/56/57 المركز التجارى,جيرولاند منطقة الخدمات الحى السابع,العبور - القليوبية', '3', 'محل 55/56/57 المركز التجارى', 'جيرولاند منطقة الخدمات الحى السابع', 
    'العبور - القليوبية', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('007', 'رقم 6 ش مصطفي أبو زهرة مدينة نص, القاهرة,مصر', '3', 'رقم 6 ش مصطفي أبو زهرة مدينة نص', ' القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('003', 'رقم 6 ش مصطفي أبو زهرة مدينة نص, القاهرة,مصر', '3', 'رقم 6 ش مصطفي أبو زهرة مدينة نص', ' القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('079', '78ش عبدالعزيز فهمى-قسم النزهه,سانت فاتيما مصر الجديدة,مصر', '3', '78ش عبدالعزيز فهمى-قسم النزهه', 'سانت فاتيما مصر الجديدة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('405', 'قليوب,القليوبية, مصر', '3', 'قليوب', 'القليوبية', 
    ' مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('473', '17 أ  شارع صلاح الدين الشرقى,مدينة اشمون,محافظة المنوفيه', '3', '17 أ  شارع صلاح الدين الشرقى', 'مدينة اشمون', 
    'محافظة المنوفيه', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('617', '55 شارع الحرية,ميدان السواقى,الفيوم', '3', '55 شارع الحرية', 'ميدان السواقى', 
    'الفيوم', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('071', 'مقر النادى الاهلى الشيخ زايد قطعة 1,الحى 17 مدينة الشيخ زايد,الجيزة', '3', 'مقر النادى الاهلى الشيخ زايد قطعة 1', 'الحى 17 مدينة الشيخ زايد', 
    'الجيزة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('186', 'المجمع التجاري مخرج 14المحورالمركزي,امام كمباوند جرينز مدينة الشيخ زايد,6 اكتوبر الجيزة', '3', 'المجمع التجاري مخرج 14المحورالمركزي', 'امام كمباوند جرينز مدينة الشيخ زايد', 
    '6 اكتوبر الجيزة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('370', 'السوق التجارى القديم الملحق التجاري,لفندق سيتى شارم,شرم الشيخ', '3', 'السوق التجارى القديم الملحق التجاري', 'لفندق سيتى شارم', 
    'شرم الشيخ', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('066', 'الوحدةEC02&EC03 المبنى E بالدور,الارضى المركز التجارى السياحى,الترفيهي مدينتى مول مدينتى ط السويس', '3', 'الوحدةEC02&EC03 المبنى E بالدور', 'الارضى المركز التجارى السياحى', 
    'الترفيهي مدينتى مول مدينتى ط السويس', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('067', 'محل تجارىرقم8Cبالدورالارضى المبنىC,منتجع ميراج ريزيدنس م الترفيهية,للتجمع الاول القاهرة الجديدة', '3', 'محل تجارىرقم8Cبالدورالارضى المبنىC', 'منتجع ميراج ريزيدنس م الترفيهية', 
    'للتجمع الاول القاهرة الجديدة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('197', 'محل رقم2بالدور الارضى العقار رقم2,تقسيم النصرللسيارات المنطقة السادسة,قسم مدينة نصر اول', '3', 'محل رقم2بالدور الارضى العقار رقم2', 'تقسيم النصرللسيارات المنطقة السادسة', 
    'قسم مدينة نصر اول', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('618', 'شارع 23 يوليو مدينة بنى سويف,محافظة بنى سويف,بنى سويف', '3', 'شارع 23 يوليو مدينة بنى سويف', 'محافظة بنى سويف', 
    'بنى سويف', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('594', 'محور الحى الثالث  مدينة السادات,المنوفية,مصر', '3', 'محور الحى الثالث  مدينة السادات', 'المنوفية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('413', '31 شارع مصطفى كامل,الشرقية,مصر', '3', '31 شارع مصطفى كامل', 'الشرقية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('411', 'شارع المركز   الحسينية,الشرقية,مصر', '3', 'شارع المركز   الحسينية', 'الشرقية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('550', 'ahvu hguv,fm,الشرقية,مصر', '3', 'ahvu hguv,fm', 'الشرقية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('590', 'مجمع البنوك الوطنية الاردنية,الشرقية,مصر', '3', 'مجمع البنوك الوطنية الاردنية', 'الشرقية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('001', '22, ADDLY STREET,CAIRO,EGYPT', '1', '22, ADDLY STREET', 'CAIRO', 
    'EGYPT', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('680', 'شارع حسنى مبارك,قنا,مصر', '3', 'شارع حسنى مبارك', 'قنا', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('606', 'المنطقة الصناعية الرابعة,مجمع البنوك -الجيزة,مصر', '3', 'المنطقة الصناعية الرابعة', 'مجمع البنوك -الجيزة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('607', 'رقم 5و7شارع حسن محمد متفرع,من شارع الهرم -الجيزة,مصر', '3', 'رقم 5و7شارع حسن محمد متفرع', 'من شارع الهرم -الجيزة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('085', 'ميدان السيدة زينب,القاهرة,مصر', '3', 'ميدان السيدة زينب', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('700', 'شارع المحطة - قوص,قنا,مصر', '3', 'شارع المحطة - قوص', 'قنا', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('660', 'رقم 95 شارع كورنيش النيل,اسوان,مصر', '3', 'رقم 95 شارع كورنيش النيل', 'اسوان', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('710', 'شارع معبد ادفو - ادفو,اسوان,مصر', '3', 'شارع معبد ادفو - ادفو', 'اسوان', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('750', 'الحى العاشر من رمضان - مدينةابوسمبل,اسوان,مصر', '3', 'الحى العاشر من رمضان - مدينةابوسمبل', 'اسوان', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('500', 'شارعى بور سعيد  والقاضى,مرسى مطروح,مصر', '3', 'شارعى بور سعيد  والقاضى', 'مرسى مطروح', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('641', 'شارع الزهور المتفرع من شارع,جمال عبد الناصر-مدينة الخارجة,مصر', '3', 'شارع الزهور المتفرع من شارع', 'جمال عبد الناصر-مدينة الخارجة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('760', 'شارع اسيوط - بحرى,اسيوط,مصر', '3', 'شارع اسيوط - بحرى', 'اسيوط', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('031', 'مبنى الاذاعة والتليفزيون - ماسبييرو,القاهرة,مصر', '3', 'مبنى الاذاعة والتليفزيون - ماسبييرو', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('231', 'رقم 49 شارع عمر لطفى -الابراهيمية,الاسكندرية,مصر', '3', 'رقم 49 شارع عمر لطفى -الابراهيمية', 'الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('608', 'مجمع ماجدة للفنون -المحور المركزى,قطعة 1.3مدينة 6اكتوبر,الجيزة - مصر', '3', 'مجمع ماجدة للفنون -المحور المركزى', 'قطعة 1.3مدينة 6اكتوبر', 
    'الجيزة - مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('601', '6اكتوبر,الجيزة, مصر', '3', '6اكتوبر', 'الجيزة', 
    ' مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('480', 'شارع الجمهورية,البحيرة,مصر', '3', 'شارع الجمهورية', 'البحيرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('033', ' 15 شارع الحسن - ناصية شارع يثرب,الجيزة,مصر', '3', ' 15 شارع الحسن - ناصية شارع يثرب', 'الجيزة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('285', '109 شارع الخديوى - باب سدرة,الاسكندرية,مصر', '3', '109 شارع الخديوى - باب سدرة', 'الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('483', 'شارع الجلاء,البحيرة,مصر', '3', 'شارع الجلاء', 'البحيرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('245', 'ميناء الدخيلة,الاسكندرية,مصر', '3', 'ميناء الدخيلة', 'الاسكندرية', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('065', 'فندق جراند حياة,كورنيش النيل جادرن سيتى,القاهرة', '3', 'فندق جراند حياة', 'كورنيش النيل جادرن سيتى', 
    'القاهرة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('191', '4 شارع اسماعيل القبانى,مدينة نصر,القاهرة', '3', '4 شارع اسماعيل القبانى', 'مدينة نصر', 
    'القاهرة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('196', 'نادى الزهور,شارع يوسف عباس مدينة نصر,القاهرة', '3', 'نادى الزهور', 'شارع يوسف عباس مدينة نصر', 
    'القاهرة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('604', '17 شارع عبد الحليم محمود,الحى المتميز المجاورة الرابعة,6 اكتوبر', '3', '17 شارع عبد الحليم محمود', 'الحى المتميز المجاورة الرابعة', 
    '6 اكتوبر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('609', 'تقاطع المريوطية عمارات,الفيصلية عمارة 1 فيصل,الجيزة', '3', 'تقاطع المريوطية عمارات', 'الفيصلية عمارة 1 فيصل', 
    'الجيزة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('403', 'كفر شكر,القليوبية, مصر', '3', 'كفر شكر', 'القليوبية', 
    ' مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('315', 'السويس,السويس, مصر', '3', 'السويس', 'السويس', 
    ' مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('365', 'شرم الشيخ,جنوب سيناء,مصر', '3', 'شرم الشيخ', 'جنوب سيناء', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS3, 
    BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('603', 'المنيل,,القاهرة', '3', 'المنيل', 'القاهرة', 
    'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('665', 'السد العالى,اسوان,مصر', '3', 'السد العالى', 'اسوان', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('627', 'شارع الثورة بجوار مدرسة الاميرية,بنى مزار,المنيا', '3', 'شارع الثورة بجوار مدرسة الاميرية', 'بنى مزار', 
    'المنيا', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('628', 'ناصية شارعى حسن حميدة والإمام البخا,سمالوط-المنيا,المنيا', '3', 'ناصية شارعى حسن حميدة والإمام البخا', 'سمالوط-المنيا', 
    'المنيا', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('636', 'تقاطع جمال عبد الناصر مع امهات المس,دير مواس,المنيا', '3', 'تقاطع جمال عبد الناصر مع امهات المس', 'دير مواس', 
    'المنيا', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('633', '68 ش بور سعيد والرسل,ابو قرقاص المنيا,المنيا', '3', '68 ش بور سعيد والرسل', 'ابو قرقاص المنيا', 
    'المنيا', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('205', 'ابو قير,ابو قير,الاسكندرية', '3', 'ابو قير', 'ابو قير', 
    'الاسكندرية', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('185', 'مدينة 6 اكتوبر,الجيزة,الجيزة', '3', 'مدينة 6 اكتوبر', 'الجيزة', 
    'الجيزة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('075', '16 ش بغداد,الكورية - مصر الجديدة,القاهره', '3', '16 ش بغداد', 'الكورية - مصر الجديدة', 
    'القاهره', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('770', 'حى السبعين أمام المخبز,كوم أمبو,كوم أمبو', '3', 'حى السبعين أمام المخبز', 'كوم أمبو', 
    'كوم أمبو', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('505', 'واحة سيوة,مشروع التكية,مدينة سيوة محافظة مطروح', '3', 'واحة سيوة', 'مشروع التكية', 
    'مدينة سيوة محافظة مطروح', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('654', 'ش احمد عرابى,طريق اسيوط سوهاج بجوار حديقة الزهور,سوهاج', '3', 'ش احمد عرابى', 'طريق اسيوط سوهاج بجوار حديقة الزهور', 
    'سوهاج', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('077', 'اخر شارع التسعين,مول مون فالى,القاهره', '3', 'اخر شارع التسعين', 'مول مون فالى', 
    'القاهره', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('068', 'القطامية هايتس التجمع الخامس,القاهرةالجديدة,القاهرة', '3', 'القطامية هايتس التجمع الخامس', 'القاهرةالجديدة', 
    'القاهرة', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('093', 'عمارة 1 14 سابقا عمارات العبور,طريق صلاح سالم-م الجديدة-القاهرة,مصر', '3', 'عمارة 1 14 سابقا عمارات العبور', 'طريق صلاح سالم-م الجديدة-القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('115', '6019 تقسيم المنطقة ج شياخة المقطم,قسم الخليفة - القاهرة,مصر', '3', '6019 تقسيم المنطقة ج شياخة المقطم', 'قسم الخليفة - القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('094', '36 شارع الجلاء,القاهرة,مصر', '3', '36 شارع الجلاء', 'القاهرة', 
    'مصر', 'O', 'EG', 1);
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY, GL_ACCOUNT_ID)
 Values
   ('096', 'المحل رقم 1 40أ (سابقا) 38 حاليا,شارع محمد مظهر - الزمالك,مصر', '3', 'المحل رقم 1 40أ (سابقا) 38 حاليا', 'شارع محمد مظهر - الزمالك', 
    'مصر', 'O', 'EG', 1);
COMMIT;


SET DEFINE OFF;
Insert into FINANCIAL_YEARS
   (ID, FIN_YEAR_CODE, NAME, ACTIVE, FROM_DATE, 
    TO_DATE)
 Values
   (1, 'FY2021', '2021', '1', TO_DATE('1/1/2021', 'MM/DD/YYYY'), 
    TO_DATE('12/31/2021', 'MM/DD/YYYY'));
Insert into FINANCIAL_YEARS
   (ID, FIN_YEAR_CODE, NAME, ACTIVE, FROM_DATE, 
    TO_DATE)
 Values
   (2, 'FY2020', '2020', '1', TO_DATE('1/1/2020', 'MM/DD/YYYY'), 
    TO_DATE('12/31/2020', 'MM/DD/YYYY'));
COMMIT;


SET DEFINE OFF;
Insert into GL_ACCOUNTS
   (ID, GL_ACCOUNT_NUMBER)
 Values
   (1, '242002303');
Insert into GL_ACCOUNTS
   (ID, GL_ACCOUNT_NUMBER)
 Values
   (2, '242002198');
COMMIT;
DROP VIEW TAXES.INVESTOR_ANNUAL_SUMMARY;

/* Formatted on 1/26/2021 4:05:59 PM (QP5 v5.326) */
CREATE OR REPLACE FORCE VIEW TAXES.INVESTOR_ANNUAL_SUMMARY
(
    TOTAL_PAID_AMOUNT,
    TOTAL_COMMISSION,
    SUPPLY_NO,
    QUARTER_ID,
    INVESTOR_ID,
    FIN_YEAR_ID
)
AS
      SELECT SUM (T.GROSS_AMOUNT)     total_paid_amount,
             SUM (T.COMMISSION)       total_commission,
             QCLOSE.SUPPLY_NO,
             QCLOSE.QUARTER_ID,
             INVESTOR.ID              AS investor_Id,
             FIN.ID                   AS Fin_year_id
        FROM transactions   t,
             investors      investor,
             financial_years fin,
             QUARTER_CLOSE  qClose
       WHERE     T.FINANCIAL_YEAR_ID = fin.id
             AND T.INVESTOR_ID = INVESTOR.ID
             AND QCLOSE.FINANCIAL_YEAR_ID = T.FINANCIAL_YEAR_ID
             AND QCLOSE.QUARTER_ID = T.QUARTER_ID
    GROUP BY QCLOSE.SUPPLY_NO,
             QCLOSE.QUARTER_ID,
             INVESTOR.ID,
             FIN.ID;


DROP VIEW TAXES.TRANSACTION_REPORT;

/* Formatted on 1/26/2021 4:05:59 PM (QP5 v5.326) */
CREATE OR REPLACE FORCE VIEW TAXES.TRANSACTION_REPORT
(
    ID,
    QUARTER,
    FIN_YEAR,
    REGISTERATION_NUMBER,
    FILE_NO,
    MASSION_CODE,
    TRANSACTION_DATE,
    CODE,
    GROSS_AMOUNT,
    DISCOUNT_TYPE,
    NET_AMOUNT,
    COMMISSION,
    TOTAL_TAX,
    INVESTOR_NAME,
    INVESTOR_ADDRESS,
    NATIONAL_ID,
    CURENCY,
    FIN_YEAR_ID
)
AS
    (SELECT TRX.ID                                    AS ID,
            trx.QUARTER_ID                            AS QUARTER,
            FN.NAME                                   AS FIN_YEAR,
            TRX.REGISTERATION_NUMBER,
            ''                                        AS FILE_NO,
            ''                                        AS MASSION_CODE,
            TRX.TRANSACTION_DATE,
            tr_Type.code,
            TRX.GROSS_AMOUNT,
            TRX.DISCOUNT_TYPE,
            TRX.NET_AMOUNT,
            tr_Type.COMMISSION,
            TRX.GROSS_AMOUNT * tr_Type.COMMISSION     AS TOTAL_TAX,
            INVESTOR.NAME                             AS INVESTOR_NAME,
            INVESTOR.ADDRESS                          AS INVESTOR_ADDRESS,
            INVESTOR.NATIONAL_ID,
            TRX.CURENCY,
            fn.ID                                     AS fin_year_id
       FROM transactions      trx,
            financial_years   fn,
            transaction_type  tr_Type,
            INVESTORS         INVESTOR
      WHERE     1 = 1
            AND TRX.FINANCIAL_YEAR_ID = fn.id
            AND TRX.TRANSACTION_TYPE_ID = tr_Type.id
            AND TRX.INVESTOR_ID = INVESTOR.ID(+));


DROP VIEW TAXES.V_GL_BALANCES;

/* Formatted on 1/26/2021 4:05:59 PM (QP5 v5.326) */
CREATE OR REPLACE FORCE VIEW TAXES.V_GL_BALANCES
(
    FIN_YEAR,
    PERIOD_CODE,
    BRANCH_CODE,
    CATEGORY,
    PARENT_GL,
    GL_CODE,
    LEAF,
    CCY_CODE,
    F_C,
    EQUIVALENT_BALANCE,
    EGP_BALANCE,
    BALANCE,
    GL_DESC,
    BRANCH_NAME
)
AS
    (SELECT F.FIN_YEAR
                AS FIN_YEAR,
            F.PERIOD_CODE
                AS PERIOD_CODE,
            F.BRANCH_CODE
                AS BRANCH_CODE,
            F.CATEGORY
                AS CATEGORY,
            F.PARENT_GL
                AS PARENT_GL,
            F.GL_CODE
                GL_CODE,
            F.LEAF
                AS LEAF,
            F.CCY_CODE
                AS CCY_CODE,
            (CASE
                 WHEN F.CATEGORY IN ('1', '3', '5')
                 THEN
                     (F.CR_BAL - F.DR_BAL) * -1
                 ELSE
                     (F.CR_BAL - F.DR_BAL) * -1
             END)
                F_C,
            (CASE
                 WHEN     F.CATEGORY IN ('1', '3', '5')
                      AND F.CCY_CODE NOT IN ('EGP')
                 THEN
                     (F.CR_BAL_LCY - F.DR_BAL_LCY) * -1
                 ELSE
                     (F.CR_BAL_LCY - F.DR_BAL_LCY) * 0
             END)
                Equivalent_balance,
            (CASE
                 WHEN     F.CATEGORY IN ('1', '3', '5')
                      AND F.CCY_CODE NOT IN ('EGP')
                 THEN
                     (F.CR_BAL_LCY - F.DR_BAL_LCY) * 0
                 ELSE
                     (F.CR_BAL_LCY - F.DR_BAL_LCY) * -1
             END)
                EGP_balance,
            (CASE
                 WHEN F.CATEGORY IN ('1', '3', '5')
                 THEN
                     (F.CR_BAL_LCY - F.DR_BAL_LCY) * -1
                 ELSE
                     (F.CR_BAL_LCY - F.DR_BAL_LCY) * -1
             END)
                Balance,
            D.V_GL_DESC,
            B.V_BRANCH_NAME
       --------------------------------------------------------------------------------------------------------***
       FROM bdcefccp.gltbs_gl_bal@FCCPROD.WORLD  F,
            DIM_GL@FCCPROD.WORLD                 D,
            dim_branch@FCCPROD.WORLD             B
      -------------------------------------------------------------------------------------------------------***
      WHERE     1 = 1
            --AND F.period_code = @variable('Period_Code')
            --and F.fin_year = @variable('Fin_Year')
            AND F.GL_CODE IN ('242002198', '242002303')
            AND (F.DR_BAL_LCY + F.CR_BAL_LCY) <> 0
            AND F.LEAF = 'Y'
            AND F.GL_CODE = D.V_GL_CODE
            AND F.BRANCH_CODE = B.V_BRANCH_CODE);
			
			
			
			
			
			
			
			
/* Formatted on 07/02/2021 22:39:39 (QP5 v5.185.11230.41888) */
CREATE OR REPLACE FORCE VIEW TAXES.BRANCHES_ACCOUNTS
(
   BRANCH_CODE,
   USER_ID,
   USER_NAME,
   ACCOUNT_NO
)
AS
   SELECT B.BRANCH_CODE,
          user_.id AS user_id,
          USER_.NAME user_name,
          USER_.ACCOUNT_NO
     FROM bdc_branches b, TAXES.SEC_USER user_
    WHERE     USER_.BRANCH_CODE LIKE B.BRANCH_CODE
          AND USER_.ACCOUNT_NO IS NOT NULL
          AND USER_.DELETED_BY IS NULL;











DROP TABLE TAXES.VIEW888 CASCADE CONSTRAINTS;

CREATE TABLE TAXES.VIEW888
(
  ID              NUMBER(10),
  LOV_DESC        VARCHAR2(315 BYTE),
  TRN_REF_NO      VARCHAR2(48 BYTE),
  AC_CCY          VARCHAR2(9 BYTE),
  VALUE_DT        DATE,
  AC_GL_NO        VARCHAR2(60 BYTE),
  AC_GL_DESC      VARCHAR2(315 BYTE),
  LCY_AMOUNT      NUMBER(22,3),
  FCY_AMOUNT      NUMBER(22,3),
  TRN_DT          DATE,
  FCY             NUMBER,
  LCY             NUMBER,
  ADDL_TEXT       VARCHAR2(765 BYTE),
  USER_ID         VARCHAR2(36 BYTE),
  CLG_BANK_NAME   VARCHAR2(315 BYTE),
  FIELD_NAME      VARCHAR2(315 BYTE),
  DESCE           VARCHAR2(315 BYTE),
  ACCOUNT_NUMBER  VARCHAR2(60 BYTE)
)
TABLESPACE SYSTEM
RESULT_CACHE (MODE DEFAULT)
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;




DROP VIEW TAXES.BRANCHES;

/* Formatted on 2/14/2021 4:22:48 PM (QP5 v5.185.11230.41888) */
CREATE OR REPLACE FORCE VIEW TAXES.BRANCHES
(
   BRANCH_CODE,
   BRANCH_LOCATION,
   BRANCH_ADDRESS1,
   BRANCH_ADDRESS2,
   BRANCH_ADDRESS3,
   BRANCH_STATUS,
   BRANCH_COUNTRY,
   BRANCH_TYPE
)
AS
   SELECT b.BRANCH_CODE,
          B.BRANCH_LOCATION,
          B.BRANCH_ADDRESS1,
          B.BRANCH_ADDRESS2,
          B.BRANCH_ADDRESS3,
          B.BRANCH_STATUS,
          B.BRANCH_COUNTRY,
          B.BRANCH_TYPE
     FROM BDC_BRANCHES b;


DROP VIEW TAXES.BRANCHES_ACCOUNTS;

/* Formatted on 2/14/2021 4:22:48 PM (QP5 v5.185.11230.41888) */
CREATE OR REPLACE FORCE VIEW TAXES.BRANCHES_ACCOUNTS
(
   BRANCH_CODE,
   USER_ID,
   USER_NAME,
   ACCOUNT_NO
)
AS
   SELECT B.BRANCH_CODE,
          user_.id AS user_id,
          USER_.NAME user_name,
          USER_.ACCOUNT_NO
     FROM bdc_branches b, TAXES.SEC_USER user_
    WHERE USER_.BRANCH_CODE LIKE B.BRANCH_CODE AND USER_.DELETED_BY IS NULL;


DROP VIEW TAXES.INVESTOR_ANNUAL_SUMMARY;

/* Formatted on 2/14/2021 4:22:48 PM (QP5 v5.185.11230.41888) */
CREATE OR REPLACE FORCE VIEW TAXES.INVESTOR_ANNUAL_SUMMARY
(
   TOTAL_PAID_AMOUNT,
   TOTAL_COMMISSION,
   SUPPLY_NO,
   QUARTER_ID,
   INVESTOR_ID,
   FIN_YEAR_ID
)
AS
     SELECT SUM (T.GROSS_AMOUNT) total_paid_amount,
            SUM (T.COMMISSION) total_commission,
            QCLOSE.SUPPLY_NO,
            QCLOSE.QUARTER_ID,
            INVESTOR.ID AS investor_Id,
            FIN.ID AS Fin_year_id
       FROM transactions t,
            investors investor,
            financial_years fin,
            QUARTER_CLOSE qClose
      WHERE     T.FINANCIAL_YEAR_ID = fin.id
            AND T.INVESTOR_ID = INVESTOR.ID
            AND QCLOSE.FINANCIAL_YEAR_ID = T.FINANCIAL_YEAR_ID
            AND QCLOSE.QUARTER_ID = T.QUARTER_ID
   GROUP BY QCLOSE.SUPPLY_NO,
            QCLOSE.QUARTER_ID,
            INVESTOR.ID,
            FIN.ID;


DROP VIEW TAXES.TRANSACTION_REPORT;

/* Formatted on 2/14/2021 7:12:28 PM (QP5 v5.185.11230.41888) */
CREATE OR REPLACE FORCE VIEW TAXES.TRANSACTION_REPORT
(
   ID,
   QUARTER,
   FIN_YEAR,
   REGISTERATION_NUMBER,
   FILE_NO,
   MASSION_CODE,
   TRANSACTION_DATE,
   CODE,
   GROSS_AMOUNT,
   DISCOUNT_TYPE,
   NET_AMOUNT,
   COMMISSION,
   TOTAL_TAX,
   INVESTOR_NAME,
   INVESTOR_ADDRESS,
   NATIONAL_ID,
   CURENCY,
   FIN_YEAR_ID,
   USER_ACCOUNT_NO,
   BRANCH_CODE,
   STATUS
)
AS
   (SELECT TRX.ID AS ID,
           trx.QUARTER_ID AS QUARTER,
           FN.NAME AS FIN_YEAR,
           TRX.REGISTERATION_NUMBER,
           '' AS FILE_NO,
           '' AS MASSION_CODE,
           TRX.TRANSACTION_DATE,
           tr_Type.code,
           TRX.GROSS_AMOUNT,
           TRX.DISCOUNT_TYPE,
           TRX.NET_AMOUNT,
           tr_Type.COMMISSION,
           TRX.GROSS_AMOUNT * tr_Type.COMMISSION AS TOTAL_TAX,
           INVESTOR.NAME AS INVESTOR_NAME,
           INVESTOR.ADDRESS AS INVESTOR_ADDRESS,
           INVESTOR.NATIONAL_ID,
           TRX.CURENCY,
           fn.ID AS fin_year_id,
           TRX.USER_ACCOUNT_NUMBER AS USER_ACCOUNT_NO,
           TRX.BRANCH_CODE_ID AS BRANCH_CODE,
           TRX.STATUS
      FROM transactions trx,
           financial_years fn,
           transaction_type tr_Type,
           INVESTORS INVESTOR
     WHERE     1 = 1
           AND TRX.FINANCIAL_YEAR_ID = fn.id
           AND TRX.TRANSACTION_TYPE_ID = tr_Type.id
           AND TRX.INVESTOR_ID = INVESTOR.ID(+));



SET DEFINE OFF;
Insert into TAXES.QUARTERS
   (ID, NAME, FROM_MONTH)
 Values
   (1, '1', 'JAN');
Insert into TAXES.QUARTERS
   (ID, NAME, FROM_MONTH)
 Values
   (2, '2', 'APR');
Insert into TAXES.QUARTERS
   (ID, NAME, FROM_MONTH)
 Values
   (3, '3', 'JUL');
Insert into TAXES.QUARTERS
   (ID, NAME, FROM_MONTH)
 Values
   (4, '4', 'OCT');
COMMIT;
