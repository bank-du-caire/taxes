package com.bdc.from41.web.screens.transaction;

import com.bdc.from41.core.enums.QuartersEnum;
import com.bdc.from41.core.enums.UserTypeEnum;
import com.bdc.from41.entity.*;
import com.bdc.from41.service.ITransactionService;
import com.bdc.from41.web.screens.investor.InvestorEdit;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.PersistenceHelper;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;
import com.haulmont.cuba.web.security.LoginProvider;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@UiController("Transaction.edit")
@UiDescriptor("transaction-edit.xml")
@EditedEntityContainer("transactionDc")
@LoadDataBeforeShow
public class TransactionEdit extends StandardEditor<Transaction> {
    @Inject
    DataContext dataContext;
    @Inject
    private LoginProvider loginProvider;
    @Inject
    private TextField<String> taxesCommission;
    @Inject
    private TextField<Double> netAmountField;
    @Inject
    private ITransactionService transactionService;
    @Inject
    private Notifications notifications;
    @Inject
    private Messages messages;
    @Inject
    private TextField<Integer> registerationNumberField;
    @Inject
    private PickerField<Investor> investorField;
    @Inject
    private LookupField<QuartersEnum> quarterField;
    @Inject
    private TextField<String> GLBlanceField;
    @Inject
    private TextField<String> totalTransactionsAmountField;
    @Inject
    private TextField<String> subtractField;
    @Inject
    private Logger log;
    @Inject
    private TextField<Double> grossAmountField;
    @Inject
    private TextField<String> taxesValue;
    @Inject
    private DataManager dataManager;
    @Inject
    private LookupField<FinancialYear> financialYearField;
    @Inject
    private LookupField<Branch> branchCodeField;
    @Inject
    private UserSession userSession;

    private UserTypeEnum userType;
    private TaxesUser user;
    @Inject
    private Actions actions;
    @Inject
    private Screens screens;
    @Inject
    private ScreenBuilders screenBuilders;


    private void valculateDiffGLTotalTransactions() {

        this.GLBlanceField.setValue("0.0");
        this.totalTransactionsAmountField.setValue("0.0");
        this.subtractField.setValue("0.0");


        Optional<FinancialYear> financialYearOptional = Optional.ofNullable(financialYearField.getValue());
        Optional<QuartersEnum> quarterMonthesOptional = Optional.ofNullable(quarterField.getValue());
        TaxesUser user = (TaxesUser) userSession.getUser();

        Optional<Branch> branchOptional = Optional.ofNullable(user.getBranch());

        if (financialYearOptional.isPresent() && quarterMonthesOptional.isPresent() && branchOptional.isPresent()) {


            BigDecimal GLBlance = transactionService.getQuarterGLBlance(financialYearOptional.get().getId(), quarterMonthesOptional.get().getId(), userType, branchOptional.get().getId(), user.getAccountNo());
            BigDecimal totalTransactions = transactionService.getQuarterTransactionsBlance(financialYearOptional.get().getId(), quarterMonthesOptional.get().getId(), userType, branchOptional.get().getId(), user.getAccountNo());
            GLBlanceField.setValue(String.valueOf(GLBlance));
            totalTransactionsAmountField.setValue(String.valueOf(totalTransactions));
            subtractField.setValue(String.valueOf(GLBlance.subtract(totalTransactions)));

        }


    }

    @Subscribe("financialYearField")
    public void onFinancialYearFieldValueChange(HasValue.ValueChangeEvent<FinancialYear> event) {
        valculateDiffGLTotalTransactions();
    }

    @Subscribe("quarterField")
    public void onQuarterFieldValueChange1(HasValue.ValueChangeEvent<QuartersEnum> event) {
        valculateDiffGLTotalTransactions();
    }

    @Subscribe("investorField")
    public void onInvestorFieldValueChange(HasValue.ValueChangeEvent<Investor> event) {

        Optional<Investor> investorOption = Optional.ofNullable(event.getValue());
        if (investorOption.isPresent()) {
            if (investorOption.get().getTaxesNumber() != null) {
                this.getEditedEntity().setRegisterationNumber(investorOption.get().getTaxesNumber());
                this.registerationNumberField.setEditable(false);

            } else {
                this.getEditedEntity().setRegisterationNumber(null);
                this.registerationNumberField.setEditable(true);

            }


        }


    }

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {


        this.getEditedEntity().setCommission(BigDecimal.valueOf(Double.parseDouble(taxesValue.getValue())));
        if (user.getAccountNo() != null) {
            this.getEditedEntity().setUserAccountNumber(user.getAccountNo());

        }

        Optional<Integer> taxesNO = Optional.ofNullable(registerationNumberField.getValue());
        Optional<Investor> investorOpt = Optional.ofNullable(investorField.getValue());


        if (taxesNO.isPresent() && investorOpt.isPresent()) {
            investorOpt.get().setTaxesNumber(taxesNO.get());
            // dataManager.commit(investorOpt.get());
            if (PersistenceHelper.isNew(investorOpt.get())) {
                Investor investor = dataContext.create(Investor.class);
                investor.setTaxesNumber(investorOpt.get().getTaxesNumber());
                investor.setAddress(investorOpt.get().getAddress());
                investor.setNationalId(investorOpt.get().getNationalId());
                investor.setName(investorOpt.get().getName());
                investor.setPhone(investorOpt.get().getPhone());
                dataContext.merge(investor);

            } else
                dataContext.merge(investorOpt.get());


        }

    }

    @Subscribe("transactionDateField")
    public void onTransactionDateFieldValueChange(HasValue.ValueChangeEvent<Date> event) {
        Date date = event.getValue();

        SimpleDateFormat sdf = new SimpleDateFormat("MM");

        if (date.compareTo(new Date()) == 1) {
            notifications.create().withCaption("date must be less or equal to today").withType(Notifications.NotificationType.ERROR).show();
            event.getComponent().setValue(new Date());
        }


    }

    @Subscribe("quarterField")
    public void onQuarterFieldValueChange(HasValue.ValueChangeEvent<MyQuarter__> event) {
        validateQurter();


    }

    @Install(to = "investorField", subject = "lookupSelectHandler")
    private void investorFieldLookupSelectHandler(Collection collection) {

    }



    private boolean validateQurter() {
        List<MyQuarter__> quarters = this.transactionService.getSuggestedQuarters();

        boolean quarterExist = false;
        for (MyQuarter__ q : quarters) {

            if (q.getId().equals(quarterField.getValue().getId()))
                quarterExist = true;
            break;
        }
        if (!quarterExist) {

            notifications.create().withType(Notifications.NotificationType.ERROR).withCaption(messages.getMainMessage("msg.should.sggestedQuarters") + quarters.toString()).show();
        }
        return quarterExist;
    }

    private boolean validateGrossAmount() {

        Optional<Double> grossAmountValue = Optional.of(grossAmountField.getValue());
        if (grossAmountValue.isPresent()) {


        } else {


        }


        List<MyQuarter__> quarters = this.transactionService.getSuggestedQuarters();

        boolean quarterExist = false;
        for (MyQuarter__ q : quarters) {

            if (q.getId() == quarterField.getValue().getId())
                quarterExist = true;
            break;
        }
        if (!quarterExist) {

            notifications.create().withType(Notifications.NotificationType.ERROR).withCaption(messages.getMainMessage("msg.should.sggestedQuarters") + quarters.toString()).show();
        }
        return quarterExist;
    }


    @Subscribe("transactionTypeField")
    public void onTransactionTypeFieldValueChange(HasValue.ValueChangeEvent<TransactionType> event) {
        log.info(event.getValue().getName());


        String s = event.getValue().getCommission().toString();
        taxesCommission.setValue(s);
    }


    @Subscribe
    public void onInitEntity(InitEntityEvent<Transaction> event) {
        TaxesUser user = (TaxesUser) userSession.getUser();
        userType = UserTypeEnum.fromId(user.getGroup().getName());
        if (user.getBranch() == null) {
            event.getEntity().setBranch(user.getBranch());
        }
    }


    @Subscribe
    public void onInit(InitEvent event) {

        user = (TaxesUser) userSession.getUser();
        userType = UserTypeEnum.fromId(user.getGroup().getName());


        List<Branch> branches = new ArrayList<>(Arrays.asList(user.getBranch()));
        if (user.getBranch() == null)
            branches = dataManager.load(Branch.class).list();

        branchCodeField.clear();
        branchCodeField.setOptionsList(branches);
        branchCodeField.setValue(user.getBranch());


        Transaction t = new Transaction();
        t.setBranch(user.getBranch());
        this.setEntityToEdit(t);

        //===========================================================================================================

        List<FinancialYear> fns = dataManager.load(FinancialYear.class).query("e.active='1'").list();
        financialYearField.clear();
        financialYearField.setOptionsList(fns);
//===========================================================================================================
        grossAmountField.addValidator(value -> {

            String msg = messages.getMainMessage("err.grossAmount");
            log.info("validator");
            if (value < 300)
                throw new ValidationException(msg);
        });
//===========================================================================================================

        registerationNumberField.addValidator(value -> {

            String msg = messages.getMainMessage("err.mustBeInteger");

            try {
                Integer i = (Integer) value;
                log.info("mustBeinteger");

            } catch (Exception e) {
                throw new ValidationException(msg);

            }
        });

        registerationNumberField.addTextChangeListener(stringValueChangeEvent -> {
            String msg = messages.getMainMessage("err.mustBeInteger");

            Integer valInteger = (Integer) stringValueChangeEvent.getSource().getValueSource().getValue();
            try {
                Integer i = Integer.parseInt(stringValueChangeEvent.getText());
                stringValueChangeEvent.getSource().setValue(i);


            } catch (Exception e) {

                /*throw new ValidationException(msg);  */
                stringValueChangeEvent.getSource().setValue(valInteger);


                System.out.println("mustBeinteger value ->" + valInteger);

            }
        });


        registerationNumberField.setTextChangeEventMode(TextInputField.TextChangeEventMode.LAZY);

    }

    @Subscribe("grossAmountField")
    public void onGrossAmountFieldValueChange(HasValue.ValueChangeEvent<Double> event) {
        log.info("onGrossAmountFieldValueChange");

        if (event.getValue() < 300) {

            return;
        }

        netAmountField.setValue(event.getValue());
        calculateTotalTaxes();
    }

    @Subscribe("taxesCommission")
    public void onTaxesCommissionValueChange(HasValue.ValueChangeEvent<String> event) {
        calculateTotalTaxes();
    }

    private void calculateTotalTaxes() {

        double commissionValue = (taxesCommission.getValue().isEmpty()) ? 0.0 : Double.parseDouble(taxesCommission.getRawValue());
        double grossAmountValue = (grossAmountField.getValue() != null) ? grossAmountField.getValue() : 0.0;

        this.taxesValue.setValue(String.valueOf(commissionValue * grossAmountValue));

    }


    @Install(to = "registerationNumberField", subject = "validator")
    private void registerationNumberFieldValidator(Integer integer) {
        log.info("FieldValidator =>" + integer);
    }

    @Subscribe("registerationNumberField")
    public void onRegisterationNumberFieldTextChange(TextInputField.TextChangeEvent event) {
        log.info("TextChange =>" + event.getText());


        try {
            Integer.parseInt(event.getText());

        } catch (Exception e) {


        }


    }


    public void addInvestorBtnAction() {
         screenBuilders.editor(Investor.class,this)
                .newEntity()
              /*  .withInitializer(investor -> {          // lambda to initialize new instance
                    investor.setName("New customer");
                })*/
                .withScreenClass(InvestorEdit.class)    // specific editor screen
                .withLaunchMode(OpenMode.DIALOG)
                 .withAfterCloseListener( closeEvent->{

                  Investor investor=   closeEvent.getScreen().getEditedEntity();
                     investorField.setValue(investor);

                 })
                .build()
                .show();




    }
}