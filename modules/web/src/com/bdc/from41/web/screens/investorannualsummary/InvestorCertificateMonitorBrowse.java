package com.bdc.from41.web.screens.investorannualsummary;

import com.bdc.from41.entity.Branch;
import com.bdc.from41.entity.FinancialYear;
import com.bdc.from41.entity.Investor;
import com.bdc.from41.service.ITransactionService;
import com.haulmont.cuba.core.entity.KeyValueEntity;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.ValueLoadContext;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.KeyValueCollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.InvestorAnnualSummary;
import com.haulmont.cuba.gui.screen.LookupComponent;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@UiController("InvestorCertificateMonitor.browse")
@UiDescriptor("inestor-certifaciate-monitor-browse.xml")
@LookupComponent("investorAnnualSummariesTable")
@LoadDataBeforeShow
public class InvestorCertificateMonitorBrowse extends StandardLookup<InvestorAnnualSummary> {

    @Inject
    private KeyValueCollectionLoader certifacteDL;
    @Inject
    private Button printReportBtn;
    @Inject
    private LookupPickerField<Branch> branchesLF;

    @Inject
    private LookupField<FinancialYear> financialYearDDL;
    @Inject
    private LookupPickerField<Investor> investorNameDLL;
    @Inject
    private Button searchBtn;
    @Inject
    private FlowBoxLayout searchParamtersPanel;
    @Inject

    private ITransactionService iTransactionService;
    @Inject
    private Notifications notifications;
    @Inject
    private Messages messages;

    @Install(to = "certifacteDL", target = Target.DATA_LOADER)
    private List<KeyValueEntity> certifacteDLLoadDelegate(ValueLoadContext valueLoadContext) {
        certifacteDL.setMaxResults(50);

        ValueLoadContext.Query query=valueLoadContext.getQuery();
        Optional<FinancialYear> financialYearOptional=Optional.ofNullable(financialYearDDL.getValue());
        Optional <Investor> investorOptional=Optional.ofNullable(investorNameDLL.getValue());
        Optional <Branch> branchOptional= Optional.ofNullable(branchesLF.getValue());

List result=new ArrayList();

        if(financialYearOptional.isPresent()&&financialYearOptional.isPresent()&&branchOptional.isPresent()){
            result=  iTransactionService.getInvestorCertficate(valueLoadContext,financialYearOptional.get().getId(),branchOptional.get().getId(),investorOptional.get().getId());
            if(result.size()==0)
                notifications.create().withType(Notifications.NotificationType.WARNING).withCaption(messages.getMainMessage("msg.noResult")).show();
            return result;


        }




        return new ArrayList<>();
    }



        public void onSearchBtnClick() {
            Optional<Investor> quarterOptional = Optional.ofNullable(investorNameDLL.getValue());
            Optional<FinancialYear> financialYearOptional = Optional.ofNullable(financialYearDDL.getValue());
            Optional <Branch> branchOptional=Optional.ofNullable(branchesLF.getValue());

            if (!quarterOptional.isPresent()) {
                notifications.create().withCaption(messages.getMainMessage("msg.error.investor.required")).withHideDelayMs(100).show();
                return;
            }

            if (!financialYearOptional.isPresent()) {
                notifications.create().withCaption(messages.getMainMessage("msg.error.financialYear.required")).withHideDelayMs(100).show();

                return;
            }

            if (!branchOptional.isPresent()) {
                notifications.create().withCaption(messages.getMainMessage("msg.error.branch.required")).withHideDelayMs(100).show();

                return;
            }
            enableSearchForm(false);
            certifacteDL.load();


        }


        private void enableSearchForm(boolean flag) {

            searchParamtersPanel.setEnabled(flag);
            printReportBtn.setEnabled(!flag);
            searchBtn.setEnabled(flag);

        }



    public void OnCancelBtnClick() {
        enableSearchForm(true);
    }
}