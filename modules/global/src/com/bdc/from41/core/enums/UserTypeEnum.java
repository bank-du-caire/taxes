package com.bdc.from41.core.enums;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum UserTypeEnum implements EnumClass<String> {

    HEAD("HEAD"),
    BRANCH("BRANCH"),
    LOCAL_ADMIN("LOCAL_ADMIN");

    private String id;

    UserTypeEnum(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static UserTypeEnum fromId(String id) {
        for (UserTypeEnum at : UserTypeEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}