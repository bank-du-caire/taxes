package com.bdc.from41.web.screens.glbalance;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.GLBalance;

@UiController("GLBalance.browse")
@UiDescriptor("gl-balance-browse.xml")
@LookupComponent("gLBalancesTable")
@LoadDataBeforeShow
public class GLBalanceBrowse extends StandardLookup<GLBalance> {
}