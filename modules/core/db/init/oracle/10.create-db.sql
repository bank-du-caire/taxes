-- begin TRANSACTIONS
create table TRANSACTIONS (
    ID number(10),
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    VERSION number(10) not null,
    --
    BRANCH_CODE_ID varchar2(15 char),
    USER_ACCOUNT_NUMBER number(19),
    COMMISSION number(19, 2),
    STATUS varchar2(50 char),
    FINANCIAL_YEAR_ID number(10) not null,
    QUARTER_ID integer not null,
    DISCOUNT_TYPE float,
    INVESTOR_ID number(10),
    TRANSACTION_TYPE_ID number(10) not null,
    TRANSACTION_DATE date not null,
    CURENCY number(10),
    GROSS_AMOUNT float not null,
    NET_AMOUNT float,
    REGISTERATION_NUMBER varchar(255),
    --
    primary key (ID)
)^
-- end TRANSACTIONS
-- begin FINANCIAL_YEARS
create table FINANCIAL_YEARS (
    ID number(10),
    --
    FIN_YEAR_CODE varchar2(255 char),
    NAME varchar2(255 char),
    ACTIVE char default ('0'),
    FROM_DATE date,
    TO_DATE date,
    --
    primary key (ID)
)^
-- end FINANCIAL_YEARS
-- begin TRANSACTION_TYPE
create table TRANSACTION_TYPE (
    ID number(10),
    --
    CODE varchar2(255 char),
    MINMUM_AMOUNT float,
    NAME varchar2(255 char),
    COMMISSION float,
    --
    primary key (ID)
)^
-- end TRANSACTION_TYPE
-- begin QUARTERS
create table QUARTERS (
    ID number(10),
    --
    NAME varchar2(255 char),
    FROM_MONTH varchar2(255 char),
    --
    primary key (ID)
)^
-- end QUARTERS
-- begin INVESTORS
create table INVESTORS (
    ID number(10),
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    VERSION number(10) not null,
    --
    NAME varchar2(255 char),
    PHONE varchar2(14 char),
    ADDRESS varchar2(1024 char),
    NATIONAL_ID varchar2(14) not null,
    TAXES_NUMBER varchar2(9),
    --
    primary key (ID)
)^
-- end INVESTORS

-- begin SEC_USER
alter table SEC_USER add ( BRANCH_CODE varchar2(15 char) ) ^
alter table SEC_USER add ( ACCOUNT_NO number(19) ) ^
alter table SEC_USER add ( DTYPE varchar2(31 char) ) ^
update SEC_USER set DTYPE = 'TaxesUser' where DTYPE is null ^
-- end SEC_USER

-- begin QUARTER_CLOSE
create table QUARTER_CLOSE (
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    VERSION number(10) not null,
    --
    FINANCIAL_YEAR_ID number(10) not null,
    QUARTER_ID number(10) not null,
    --
    SUPPLY_NO number(19),
    --
    primary key (FINANCIAL_YEAR_ID, QUARTER_ID)
)^
-- end QUARTER_CLOSE
-- begin GL_ACCOUNTS
create table GL_ACCOUNTS (
    ID number(10),
    --
    GL_Account_number VARCHAR(20),
    --
    primary key (ID)
)^
-- end GL_ACCOUNTS
-- begin BDC_BRANCHES
create table BDC_BRANCHES (
    BRANCH_CODE varchar2(15 char),
    --
    BRANCH_LOCATION varchar2(1024 char),
    BRANCH_TYPE varchar2(255 char),
    BRANCH_ADDRESS varchar2(1024 char),
    BRANCH_ADDRESS2 varchar2(1024 char),
    BRANCH_ADDRESS3 varchar2(1024 char),
    BRANCH_STATUS varchar2(255 char),
    BRANCH_COUNTRY varchar2(255 char),
    GL_ACCOUNT_ID number(10),
    --
    primary key (BRANCH_CODE)
)^
-- end BDC_BRANCHES
-- begin BRANCHES_ACCOUNTS
create table branches_accounts (
    BRANCH_CODE varchar2(255 char),
    USER_ID varchar2(32),
    --
    USER_NAME varchar2(255 char),
    ACCOUNT_NO varchar2(255 char),
    --
    primary key (BRANCH_CODE, USER_ID)
)^
-- end BRANCHES_ACCOUNTS
