package com.bdc.from41.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "GL_ACCOUNTS")
@Entity(name = "GLAccount")
@NamePattern("%s|glAccountNo")
public class GLAccount extends BaseIntegerIdEntity  {
    private static final long serialVersionUID = -4542978108068206942L;



    @Column(name = "GL_Account_number",columnDefinition = "VARCHAR(20)",unique = true)
    private Long glAccountNo;

    public Long getGlAccountNo() {
        return glAccountNo;
    }

    public void setGlAccountNo(Long glAccountNo) {
        this.glAccountNo = glAccountNo;
    }
}