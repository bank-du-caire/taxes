﻿SET DEFINE OFF;
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('070', '56 شارع نزية خليفة مصر الجديدة,القاهرة,مصر', '3', '56 شارع نزية خليفة مصر الجديدة', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('300', '31 شارع الجمهوربة والفرات,بور سعيد,مصر', '3', '31 شارع الجمهوربة والفرات', 'بور سعيد', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('200', 'رقم 16 شارع سيزوستريس,الاسكندرية,مصر', '3', 'رقم 16 شارع سيزوستريس', 'الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('210', 'رقم 5 شارع صلاح سالم,الاسكندرية,مصر', '3', 'رقم 5 شارع صلاح سالم', 'الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('055', '20 شارع طلعت حرب,القاهرة,مصر', '3', '20 شارع طلعت حرب', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('100', '76 شارع شبرا,القاهرة,مصر', '3', '76 شارع شبرا', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('600', '9 شارع همدان من  مراد,الجيزة,مصر', '3', '9 شارع همدان من  مراد', 'الجيزة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('130', '17 ميدان الظاهر,القاهرة,مصر', '3', '17 ميدان الظاهر', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('090', '44 شارع رمسيس - رمسيس,القاهرة,مصر', '3', '44 شارع رمسيس - رمسيس', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('080', '271 شارع بورسعيد  - السيدة زنيب,القاهرة,مصر', '3', '271 شارع بورسعيد  - السيدة زنيب', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('110', '925 شارع كورنيش النيل,القاهرة,مصر', '3', '925 شارع كورنيش النيل', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('160', ' 112 شارع السبتية,القاهرة,مصر', '3', ' 112 شارع السبتية', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('140', '11 شارع على روبى - روكسى,القاهرة,مصر', '3', '11 شارع على روبى - روكسى', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('120', '6 شارع عزت بدوى حافظ - السواح,القاهرة,مصر', '3', '6 شارع عزت بدوى حافظ - السواح', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('190', ' ناصية شارعى سمير فرحات و,عبد الحميد لطفى -مدينة نصر,القاهرة -مصر', '3', ' ناصية شارعى سمير فرحات و', 'عبد الحميد لطفى -مدينة نصر', 
    'القاهرة -مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('180', '36 شارع مصطفى فهمى - حلوان,القاهرة,مصر', '3', '36 شارع مصطفى فهمى - حلوان', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('170', '41 شارع نوبار ناصية نوبار والشيخ,ريحان - نوبار - القاهرة,مصر', '3', '41 شارع نوبار ناصية نوبار والشيخ', 'ريحان - نوبار - القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('072', '13 شارع ابراهيم اللقانى مصر الجديدة,القاهرة,مصر', '3', '13 شارع ابراهيم اللقانى مصر الجديدة', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('073', '26 شارع ابراهيم اللقانى مصر الجديدة,القاهرة,مصر', '3', '26 شارع ابراهيم اللقانى مصر الجديدة', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('605', 'رقم 1شارع مراد,ميدان الجيزة - الجيزة,مصر', '3', 'رقم 1شارع مراد', 'ميدان الجيزة - الجيزة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('570', 'شارع مصطفى كامل,البحيرة,مصر', '3', 'شارع مصطفى كامل', 'البحيرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('481', 'شارع الجمهورية,البحيرة,مصر', '3', 'شارع الجمهورية', 'البحيرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('580', 'مدينة الشروق مدخل 2,بجوار شركة الكهرباء,القاهرة', '3', 'مدينة الشروق مدخل 2', 'بجوار شركة الكهرباء', 
    'القاهرة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('610', 'رقم 7 شارع الحرية - بندر الفيوم,الفيوم,مصر', '3', 'رقم 7 شارع الحرية - بندر الفيوم', 'الفيوم', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('655', 'ش صلاح سالم,طهطا,سوهاج', '3', 'ش صلاح سالم', 'طهطا', 
    'سوهاج', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('611', 'شارع ابوبكر الصديق,الفيوم,مصر', '3', 'شارع ابوبكر الصديق', 'الفيوم', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('585', 'المنطقة الصناعية   العبور,القاهرة,مصر', '3', 'المنطقة الصناعية   العبور', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('260', 'رقم 78شارع عبدالسلام عارف,جليم -الاسكندرية,مصر', '3', 'رقم 78شارع عبدالسلام عارف', 'جليم -الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('230', 'رقم 226 شارع بورسعيد سبورتنج,الاسكندرية,مصر', '3', 'رقم 226 شارع بورسعيد سبورتنج', 'الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('220', 'رقم 1شارع حجر النواتية,باكوس- الاسكندرية,مصر', '3', 'رقم 1شارع حجر النواتية', 'باكوس- الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('290', '16 شارع الاقبال      فكتوريا,الاسكندرية,مصر', '3', '16 شارع الاقبال      فكتوريا', 'الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('250', 'اول شارع البيطاش طريق مرسى مطروح,الاسكندرية,مصر', '3', 'اول شارع البيطاش طريق مرسى مطروح', 'الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('280', 'شارع ملك حنفى طريق ابوقير,المنتزة- الاسكندرية,مصر', '3', 'شارع ملك حنفى طريق ابوقير', 'المنتزة- الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('270', 'رقم 36 شارع عمانويل,سموحة - الاسكندرية,مصر', '3', 'رقم 36 شارع عمانويل', 'سموحة - الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('255', 'مدينة برج العرب منطقة البنوك,الاسكندرية,مصر', '3', 'مدينة برج العرب منطقة البنوك', 'الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('201', 'الورديان,الاسكندرية,مصر', '3', 'الورديان', 'الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('330', '17 شارع الجمهورية,بور فؤاد,مصر', '3', '17 شارع الجمهورية', 'بور فؤاد', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('320', '7 شارع الشهيدفريد ندا,الاسماعيلية,مصر', '3', '7 شارع الشهيدفريد ندا', 'الاسماعيلية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('560', 'السوق التجارى المجاورة 12,الشرقية,مصر', '3', 'السوق التجارى المجاورة 12', 'الشرقية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('310', '1  شارع التحرير,السويس,مصر', '3', '1  شارع التحرير', 'السويس', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('672', 'منطقة المنياء بجوار مبنى السنترال,البحر الاحمر,مصر', '3', 'منطقة المنياء بجوار مبنى السنترال', 'البحر الاحمر', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('730', 'طريق الشيراتون - المطار,البحر الاحمر,مصر', '3', 'طريق الشيراتون - المطار', 'البحر الاحمر', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('340', 'عمارة رقم46 منطقة ميناء نويبع,جنوب سيناء,مصر', '3', 'عمارة رقم46 منطقة ميناء نويبع', 'جنوب سيناء', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('101', '92 شارع خلوصى,القاهرة,مصر', '3', '92 شارع خلوصى', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('035', '3 شارع دانش - العباسية,القاهرة,مصر', '3', '3 شارع دانش - العباسية', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('111', '3 شارع 79 المعادى,المعادى,مصر', '3', '3 شارع 79 المعادى', 'المعادى', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('112', 'ناصية شارعى فلسطين و269 المتفرع من,شارع النصر- القاهرة,مصر', '3', 'ناصية شارعى فلسطين و269 المتفرع من', 'شارع النصر- القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('030', '19 شارع عدلى,القاهرة,مصر', '3', '19 شارع عدلى', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('415', 'شارع اللواء عبد العزيز على,الشرقية,مصر', '3', 'شارع اللواء عبد العزيز على', 'الشرقية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('592', 'شارع سعد زغلول,الشرقية,مصر', '3', 'شارع سعد زغلول', 'الشرقية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('625', '17 ش الابراهيمية,ببا,بنى سويف', '3', '17 ش الابراهيمية', 'ببا', 
    'بنى سويف', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('420', '79  شارع الجيش,الدقهلية,مصر', '3', '79  شارع الجيش', 'الدقهلية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('593', '221 شارعه الجمهورية,الدقهلية,مصر', '3', '221 شارعه الجمهورية', 'الدقهلية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('095', '37,26 OF JULY ST.,CAIRO,EGYPT', '3', '37,26 OF JULY ST.', 'CAIRO', 
    'EGYPT', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('430', 'شارع احمد عرابى,الدقهلية,مصر', '3', 'شارع احمد عرابى', 'الدقهلية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('422', 'مبنى مجمع المصالح  الحكومية,الدقهلية,مصر', '3', 'مبنى مجمع المصالح  الحكومية', 'الدقهلية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('424', 'شارع صلاح سالم,الدقهلية,مصر', '3', 'شارع صلاح سالم', 'الدقهلية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('423', '44 شارع الثورة,الدقهلية,مصر', '3', '44 شارع الثورة', 'الدقهلية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('520', 'شارع سامى شلباية,الدقهلية,مصر', '3', 'شارع سامى شلباية', 'الدقهلية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('421', 'شارع الجيش,الدقهلية,مصر', '3', 'شارع الجيش', 'الدقهلية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('440', 'شارع الجلاء,دمياط,مصر', '3', 'شارع الجلاء', 'دمياط', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('445', 'القطعة رقم 32,دمياط,مصر', '3', 'القطعة رقم 32', 'دمياط', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('450', '53  شارع الجيش,غربية,مصر', '3', '53  شارع الجيش', 'غربية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('460', '12 شارع 23 يوليو,غربية,مصر', '3', '12 شارع 23 يوليو', 'غربية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('452', 'شارع الخان,غربية,مصر', '3', 'شارع الخان', 'غربية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('451', 'ميدان المحطة,غربية,مصر', '3', 'ميدان المحطة', 'غربية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('360', 'هضبة ام السيد قطعة رقم9  منطقة البن,جنوب سيناء,مصر', '3', 'هضبة ام السيد قطعة رقم9  منطقة البن', 'جنوب سيناء', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('350', 'ناصية شارع الشباب والرياضة,شمال سيناء,مصر', '3', 'ناصية شارع الشباب والرياضة', 'شمال سيناء', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('400', '1شارع جميل,القليوبية, مصر', '3', '1شارع جميل', 'القليوبية', 
    ' مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('401', 'شارع العاشر من رمضان,القليوبية,مصر', '3', 'شارع العاشر من رمضان', 'القليوبية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('402', 'شارع جمال عبد الناصر,القليوبية,مصر', '3', 'شارع جمال عبد الناصر', 'القليوبية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('410', '5 شارع احمد عرابى,الشرقية,مصر', '3', '5 شارع احمد عرابى', 'الشرقية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('412', 'شارع بور سعيد     مجمع المصالح ا,الشرقية,مصر', '3', 'شارع بور سعيد     مجمع المصالح ا', 'الشرقية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('414', 'شارع الساحة الشعبية,الشرقية,مصر', '3', 'شارع الساحة الشعبية', 'الشرقية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('151', '18 شارع عمار بن ياسر,القاهرة,مصر', '3', '18 شارع عمار بن ياسر', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('602', 'رقم 72 شارع جامعة الدول العربية,المهندسين -الجيزة,مصر', '3', 'رقم 72 شارع جامعة الدول العربية', 'المهندسين -الجيزة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('020', 'قصرالنيل,القاهرة,مصر', '3', 'قصرالنيل', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('034', 'شارع وزارة الزراعة,الجيزة,مصر', '3', 'شارع وزارة الزراعة', 'الجيزة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('040', '6شارع بستان الدكة - الالفى,القاهرة,مصر', '3', '6شارع بستان الدكة - الالفى', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('045', '34شارع عبد الخالق ثروت,القاهرة,مصر', '3', '34شارع عبد الخالق ثروت', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('620', 'رقم 45شارع 23يوليو,بنىسويف,مصر', '3', 'رقم 45شارع 23يوليو', 'بنىسويف', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('150', 'مطار القاهرة الدولي,القاهرة,مصر', '3', 'مطار القاهرة الدولي', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('621', 'رقم 4شارع الشهيد ممدوح مصطفى,اهناسيا - بنى سويف,مصر', '3', 'رقم 4شارع الشهيد ممدوح مصطفى', 'اهناسيا - بنى سويف', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('740', 'شارع بورسعيد - سمسطا,بنى سويف,مصر', '3', 'شارع بورسعيد - سمسطا', 'بنى سويف', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('630', 'رقم 2 شارع سعد زغلول,المنيا,مصر', '3', 'رقم 2 شارع سعد زغلول', 'المنيا', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('635', 'شارع الجلاء - مبنى المهن الزراعية,ملوى - المنيا,مصر', '3', 'شارع الجلاء - مبنى المهن الزراعية', 'ملوى - المنيا', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('640', 'رقم 27 شارع الجمهورية - برج الفردوس,اسيوط,مصر', '3', 'رقم 27 شارع الجمهورية - برج الفردوس', 'اسيوط', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('650', 'رقم 26 شارع النيل,سوهاج,مصر', '3', 'رقم 26 شارع النيل', 'سوهاج', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('720', 'ناصية شارعى المحطة والسيدة خديجة,سوهاج,مصر', '3', 'ناصية شارعى المحطة والسيدة خديجة', 'سوهاج', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('670', 'شارع 23يوليو,قنا,مصر', '3', 'شارع 23يوليو', 'قنا', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('690', 'كورنيش النيل الاقصر,قنا,مصر', '3', 'كورنيش النيل الاقصر', 'قنا', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('540', 'شارع الجمهورية واالصاغة,غربية,مصر', '3', 'شارع الجمهورية واالصاغة', 'غربية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('490', '11 شارع الخلفاء الراشدين,كفر الشيخ,مصر', '3', '11 شارع الخلفاء الراشدين', 'كفر الشيخ', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('492', '1 شارع الخلفاء الراشدين,كفر الشيخ,مصر', '3', '1 شارع الخلفاء الراشدين', 'كفر الشيخ', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('491', 'شارع كورنيش النيل,كفر الشيخ,مصر', '3', 'شارع كورنيش النيل', 'كفر الشيخ', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('530', 'شارع جمال عبد  الناصر,كفر الشيخ,مصر', '3', 'شارع جمال عبد  الناصر', 'كفر الشيخ', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('510', 'شارع الجيش,المنوفية,مصر', '3', 'شارع الجيش', 'المنوفية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('472', 'العمارة السكنية مجلس مد ينة تلا,المنوفية,مصر', '3', 'العمارة السكنية مجلس مد ينة تلا', 'المنوفية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('471', 'شارع الكورنيش البر الغربى,المنوفية,مصر', '3', 'شارع الكورنيش البر الغربى', 'المنوفية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('591', 'شارع الجيش,المنطقة,مصر', '3', 'شارع الجيش', 'المنطقة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('155', 'طريق مطار القاهرة مبنى الشركة,المصرية للمطارات أمام مبنى وزارة ال,القاهرة', '3', 'طريق مطار القاهرة مبنى الشركة', 'المصرية للمطارات أمام مبنى وزارة ال', 
    'القاهرة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('074', '6 شارع محور طه حسين,النزهة الجديدة,القاهرة', '3', '6 شارع محور طه حسين', 'النزهة الجديدة', 
    'القاهرة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('656', 'مدينة مبارك الصناعية - حى الكوثر,سوهاج,مصر', '3', 'مدينة مبارك الصناعية - حى الكوثر', 'سوهاج', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('494', 'شارع الشهيد عبدالمنعم رياض,بيلا - كفر الشيخ,كفر الشيخ', '3', 'شارع الشهيد عبدالمنعم رياض', 'بيلا - كفر الشيخ', 
    'كفر الشيخ', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('188', '73 شارع سوريا,المهندسين,الجيزة', '3', '73 شارع سوريا', 'المهندسين', 
    'الجيزة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('010', 'رقم 6 ش مصطفي أبو زهرة مدينة نص, القاهرة,مصر', '3', 'رقم 6 ش مصطفي أبو زهرة مدينة نص', ' القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('325', 'الاسماعيلية,الاسماعيلية, مصر', '3', 'الاسماعيلية', 'الاسماعيلية', 
    ' مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('495', 'امام المطافى مدينةالحامول كفر الشيخ,الحامول,كفر الشيخ', '3', 'امام المطافى مدينةالحامول كفر الشيخ', 'الحامول', 
    'كفر الشيخ', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('595', 'المنطقة الصناعية الثالثة,مجمع مصر للطيران امام محطة كهرباء,شمال خلف جريش للزجاج العشر من رمضان', '3', 'المنطقة الصناعية الثالثة', 'مجمع مصر للطيران امام محطة كهرباء', 
    'شمال خلف جريش للزجاج العشر من رمضان', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('156', 'المبنى المطوربمجمع بضائع مصرللطيران,قرية البضائع خارج الدائرة الجمركية,مطار القاهرة - مصر', '3', 'المبنى المطوربمجمع بضائع مصرللطيران', 'قرية البضائع خارج الدائرة الجمركية', 
    'مطار القاهرة - مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('015', 'ناصية ش سمير فرحات وعبدالحميد لطفى,الحى الثامن - م نصر- القاهرة,مصر', '3', 'ناصية ش سمير فرحات وعبدالحميد لطفى', 'الحى الثامن - م نصر- القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('002', 'رقم 6 ش مصطفي أبو زهرة مدينة نص, القاهرة,مصر', '3', 'رقم 6 ش مصطفي أبو زهرة مدينة نص', ' القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('194', 'الوحدة 5و6 مول اميرالد تاون بلازا,منتجع النخيل امتداد زاكر حسين,التجمع الاول القاهرة', '3', 'الوحدة 5و6 مول اميرالد تاون بلازا', 'منتجع النخيل امتداد زاكر حسين', 
    'التجمع الاول القاهرة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('182', 'منطقة التوسعات الشرقية,خلف جولف بالم هيلز ب 6 اكتوبر,الجيزة', '3', 'منطقة التوسعات الشرقية', 'خلف جولف بالم هيلز ب 6 اكتوبر', 
    'الجيزة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('098', 'المحل التجارى رقم G1 بالطابق الارضى,مول ميدتاون م. المستثمرين الجنوبية,القاهرة الجديدة', '3', 'المحل التجارى رقم G1 بالطابق الارضى', 'مول ميدتاون م. المستثمرين الجنوبية', 
    'القاهرة الجديدة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('183', 'الوحدة 26 مول ميجا داندى مول الكيلو,28 طريق مصر الاسكندرية الصحراوى,مصر', '3', 'الوحدة 26 مول ميجا داندى مول الكيلو', '28 طريق مصر الاسكندرية الصحراوى', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('004', 'وحدة(G) رقم107مول مصر طريق الوا حات,امام مدينة الانتاج الاعلامي منطقة,التوسعات الشرقية الجيزة', '3', 'وحدة(G) رقم107مول مصر طريق الوا حات', 'امام مدينة الانتاج الاعلامي منطقة', 
    'التوسعات الشرقية الجيزة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('637', 'شارع الجلاء,القوصية,اسيوط', '3', 'شارع الجلاء', 'القوصية', 
    'اسيوط', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('614', 'ش التل الجديد من ش الجمهورية,ابشواى,الفيوم', '3', 'ش التل الجديد من ش الجمهورية', 'ابشواى', 
    'الفيوم', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('442', 'شارع التحرير,قسم اول دمياط,دمياط', '3', 'شارع التحرير', 'قسم اول دمياط', 
    'دمياط', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('425', 'شارع الثورة رياح المنصورة,مدينة اجا,الدقهلية', '3', 'شارع الثورة رياح المنصورة', 'مدينة اجا', 
    'الدقهلية', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('192', '44 ش عبدالرازق السنهورى,مدينة نصر اول,القاهرة', '3', '44 ش عبدالرازق السنهورى', 'مدينة نصر اول', 
    'القاهرة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('215', '554 تنظيم طريق الجيش سيدي بشر بحرى,المنتزة اول,الاسكندرية', '3', '554 تنظيم طريق الجيش سيدي بشر بحرى', 'المنتزة اول', 
    'الاسكندرية', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('036', '42 ش الرشيد - بولاق الدكرور,الجيزة,الجيزة', '3', '42 ش الرشيد - بولاق الدكرور', 'الجيزة', 
    'الجيزة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('645', 'قطعة رقم 32 كدستر بحوض ترعة,بنى حسين 11مكرر ش جمال عبد الناصر,منفلوط اسيوط', '3', 'قطعة رقم 32 كدستر بحوض ترعة', 'بنى حسين 11مكرر ش جمال عبد الناصر', 
    'منفلوط اسيوط', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('612', 'رقم 1 شارع بورسعيد - سنورس,الفيوم,مصر', '3', 'رقم 1 شارع بورسعيد - سنورس', 'الفيوم', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('615', '173 ش طراد النيل - مركز,مركز الواسطى,بنى سويف', '3', '173 ش طراد النيل - مركز', 'مركز الواسطى', 
    'بنى سويف', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('626', 'ش الجيش  مدينة الفشن,بنى سويف,بنى سويف', '3', 'ش الجيش  مدينة الفشن', 'بنى سويف', 
    'بنى سويف', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('616', '6 ش ابو بكر الصديق مركز ناصر,بنى سويف,بنى سويف', '3', '6 ش ابو بكر الصديق مركز ناصر', 'بنى سويف', 
    'بنى سويف', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('446', 'ش بورسعيد والجلاء, كفر سعد دمياط,دمياط', '3', 'ش بورسعيد والجلاء', ' كفر سعد دمياط', 
    'دمياط', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('619', 'مدينة بنى سويف الجديدة- الحى الأول,شرق النيل,بنى سويف', '3', 'مدينة بنى سويف الجديدة- الحى الأول', 'شرق النيل', 
    'بنى سويف', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('695', 'المنشية - ميدان صلاح الدين,مدينة الاقصر,الاقصر', '3', 'المنشية - ميدان صلاح الدين', 'مدينة الاقصر', 
    'الاقصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('078', '12 أ مدينة التوفيق,ش يوسف عباس م نصر,القاهرة', '3', '12 أ مدينة التوفيق', 'ش يوسف عباس م نصر', 
    'القاهرة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('305', '11 ش الجمهورية,تقاطع ش حافظ ابراهيم,بورسعيد', '3', '11 ش الجمهورية', 'تقاطع ش حافظ ابراهيم', 
    'بورسعيد', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('076', '79سابقا 81حاليا شارع المنتزة النزهة,مصر الجديدة القاهرة,مصر', '3', '79سابقا 81حاليا شارع المنتزة النزهة', 'مصر الجديدة القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('187', 'AUTO VILLE THE STRIP بمشروع A5,مدينة الشيخ زايد - 6 اكتوبر,الجيزة', '3', 'AUTO VILLE THE STRIP بمشروع A5', 'مدينة الشيخ زايد - 6 اكتوبر', 
    'الجيزة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('470', 'شارعى المدارس وبنك مصر,المنوفية,مصر', '3', 'شارعى المدارس وبنك مصر', 'المنوفية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('653', 'تقاطع ش التحرير مع عثمان بن عفان,البلينا,سوهاج', '3', 'تقاطع ش التحرير مع عثمان بن عفان', 'البلينا', 
    'سوهاج', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('355', 'جنوب سيناء,جنوب سيناء,جنوب سيناء', '3', 'جنوب سيناء', 'جنوب سيناء', 
    'جنوب سيناء', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('501', 'ش الاسكندرية - امام ش القسم,ميدان الغزالة-مدينة الحمام,مرسى مطروح', '3', 'ش الاسكندرية - امام ش القسم', 'ميدان الغزالة-مدينة الحمام', 
    'مرسى مطروح', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('265', '293 طريق الحرية اسبورتنج,قسم سيدى جابر حى شرق الاسكندرية,الاسكندرية', '3', '293 طريق الحرية اسبورتنج', 'قسم سيدى جابر حى شرق الاسكندرية', 
    'الاسكندرية', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('485', '4ش المحكمة تقاطع ش 22 برج الرضوان,امام الضرائب العقارية,البحيرة', '3', '4ش المحكمة تقاطع ش 22 برج الرضوان', 'امام الضرائب العقارية', 
    'البحيرة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('195', 'رقم 6 ش مصطفي أبو زهرة مدينة نص, القاهرة,مصر', '3', 'رقم 6 ش مصطفي أبو زهرة مدينة نص', ' القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('005', 'اسكندرية,اسكندرية,مصر', '3', 'اسكندرية', 'اسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('629', '9 ش الوحدة العربية,منشية المصرى-مغاغة,المنيا', '3', '9 ش الوحدة العربية', 'منشية المصرى-مغاغة', 
    'المنيا', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('663', 'شارع كورنيش النيل اسفل فندق فيلة,اسوان,مصر', '3', 'شارع كورنيش النيل اسفل فندق فيلة', 'اسوان', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('705', 'كورنيش النيل عمارة سلامة رشيدى,مدينة اسنا محافظة الاقصر,مصر', '3', 'كورنيش النيل عمارة سلامة رشيدى', 'مدينة اسنا محافظة الاقصر', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('193', 'مول سيتى ستارز الوحدة A151,مكرم عبيد م. نصر,القاهرة', '3', 'مول سيتى ستارز الوحدة A151', 'مكرم عبيد م. نصر', 
    'القاهرة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('657', 'ش السكة الحديدبجوار المزلقان البحرى,غرب محطة السكة الحديد مدخل المدينة,الناحية الغربية-سوهاج', '3', 'ش السكة الحديدبجوار المزلقان البحرى', 'غرب محطة السكة الحديد مدخل المدينة', 
    'الناحية الغربية-سوهاج', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('088', 'الوحدة108(G39) قطاعAالمنطقةالتجارية,السوق الشرقى الركن الشمالى الشرقى,امتداد الرحاب القاهرة الجديدة', '3', 'الوحدة108(G39) قطاعAالمنطقةالتجارية', 'السوق الشرقى الركن الشمالى الشرقى', 
    'امتداد الرحاب القاهرة الجديدة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('216', 'برج سرايا السيوف تقاطع محمدامين,حسونة مع ش عادل مصطفى السيوف,الاسكندرية', '3', 'برج سرايا السيوف تقاطع محمدامين', 'حسونة مع ش عادل مصطفى السيوف', 
    'الاسكندرية', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('404', 'محل 55/56/57 المركز التجارى,جيرولاند منطقة الخدمات الحى السابع,العبور - القليوبية', '3', 'محل 55/56/57 المركز التجارى', 'جيرولاند منطقة الخدمات الحى السابع', 
    'العبور - القليوبية', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('007', 'رقم 6 ش مصطفي أبو زهرة مدينة نص, القاهرة,مصر', '3', 'رقم 6 ش مصطفي أبو زهرة مدينة نص', ' القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('003', 'رقم 6 ش مصطفي أبو زهرة مدينة نص, القاهرة,مصر', '3', 'رقم 6 ش مصطفي أبو زهرة مدينة نص', ' القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('079', '78ش عبدالعزيز فهمى-قسم النزهه,سانت فاتيما مصر الجديدة,مصر', '3', '78ش عبدالعزيز فهمى-قسم النزهه', 'سانت فاتيما مصر الجديدة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('405', 'قليوب,القليوبية, مصر', '3', 'قليوب', 'القليوبية', 
    ' مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('473', '17 أ  شارع صلاح الدين الشرقى,مدينة اشمون,محافظة المنوفيه', '3', '17 أ  شارع صلاح الدين الشرقى', 'مدينة اشمون', 
    'محافظة المنوفيه', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('617', '55 شارع الحرية,ميدان السواقى,الفيوم', '3', '55 شارع الحرية', 'ميدان السواقى', 
    'الفيوم', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('071', 'مقر النادى الاهلى الشيخ زايد قطعة 1,الحى 17 مدينة الشيخ زايد,الجيزة', '3', 'مقر النادى الاهلى الشيخ زايد قطعة 1', 'الحى 17 مدينة الشيخ زايد', 
    'الجيزة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('186', 'المجمع التجاري مخرج 14المحورالمركزي,امام كمباوند جرينز مدينة الشيخ زايد,6 اكتوبر الجيزة', '3', 'المجمع التجاري مخرج 14المحورالمركزي', 'امام كمباوند جرينز مدينة الشيخ زايد', 
    '6 اكتوبر الجيزة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('370', 'السوق التجارى القديم الملحق التجاري,لفندق سيتى شارم,شرم الشيخ', '3', 'السوق التجارى القديم الملحق التجاري', 'لفندق سيتى شارم', 
    'شرم الشيخ', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('066', 'الوحدةEC02&EC03 المبنى E بالدور,الارضى المركز التجارى السياحى,الترفيهي مدينتى مول مدينتى ط السويس', '3', 'الوحدةEC02&EC03 المبنى E بالدور', 'الارضى المركز التجارى السياحى', 
    'الترفيهي مدينتى مول مدينتى ط السويس', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('067', 'محل تجارىرقم8Cبالدورالارضى المبنىC,منتجع ميراج ريزيدنس م الترفيهية,للتجمع الاول القاهرة الجديدة', '3', 'محل تجارىرقم8Cبالدورالارضى المبنىC', 'منتجع ميراج ريزيدنس م الترفيهية', 
    'للتجمع الاول القاهرة الجديدة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('197', 'محل رقم2بالدور الارضى العقار رقم2,تقسيم النصرللسيارات المنطقة السادسة,قسم مدينة نصر اول', '3', 'محل رقم2بالدور الارضى العقار رقم2', 'تقسيم النصرللسيارات المنطقة السادسة', 
    'قسم مدينة نصر اول', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('618', 'شارع 23 يوليو مدينة بنى سويف,محافظة بنى سويف,بنى سويف', '3', 'شارع 23 يوليو مدينة بنى سويف', 'محافظة بنى سويف', 
    'بنى سويف', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('594', 'محور الحى الثالث  مدينة السادات,المنوفية,مصر', '3', 'محور الحى الثالث  مدينة السادات', 'المنوفية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('413', '31 شارع مصطفى كامل,الشرقية,مصر', '3', '31 شارع مصطفى كامل', 'الشرقية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('411', 'شارع المركز   الحسينية,الشرقية,مصر', '3', 'شارع المركز   الحسينية', 'الشرقية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('550', 'ahvu hguv,fm,الشرقية,مصر', '3', 'ahvu hguv,fm', 'الشرقية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('590', 'مجمع البنوك الوطنية الاردنية,الشرقية,مصر', '3', 'مجمع البنوك الوطنية الاردنية', 'الشرقية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('001', '22, ADDLY STREET,CAIRO,EGYPT', '1', '22, ADDLY STREET', 'CAIRO', 
    'EGYPT', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('680', 'شارع حسنى مبارك,قنا,مصر', '3', 'شارع حسنى مبارك', 'قنا', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('606', 'المنطقة الصناعية الرابعة,مجمع البنوك -الجيزة,مصر', '3', 'المنطقة الصناعية الرابعة', 'مجمع البنوك -الجيزة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('607', 'رقم 5و7شارع حسن محمد متفرع,من شارع الهرم -الجيزة,مصر', '3', 'رقم 5و7شارع حسن محمد متفرع', 'من شارع الهرم -الجيزة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('085', 'ميدان السيدة زينب,القاهرة,مصر', '3', 'ميدان السيدة زينب', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('700', 'شارع المحطة - قوص,قنا,مصر', '3', 'شارع المحطة - قوص', 'قنا', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('660', 'رقم 95 شارع كورنيش النيل,اسوان,مصر', '3', 'رقم 95 شارع كورنيش النيل', 'اسوان', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('710', 'شارع معبد ادفو - ادفو,اسوان,مصر', '3', 'شارع معبد ادفو - ادفو', 'اسوان', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('750', 'الحى العاشر من رمضان - مدينةابوسمبل,اسوان,مصر', '3', 'الحى العاشر من رمضان - مدينةابوسمبل', 'اسوان', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('500', 'شارعى بور سعيد  والقاضى,مرسى مطروح,مصر', '3', 'شارعى بور سعيد  والقاضى', 'مرسى مطروح', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('641', 'شارع الزهور المتفرع من شارع,جمال عبد الناصر-مدينة الخارجة,مصر', '3', 'شارع الزهور المتفرع من شارع', 'جمال عبد الناصر-مدينة الخارجة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('760', 'شارع اسيوط - بحرى,اسيوط,مصر', '3', 'شارع اسيوط - بحرى', 'اسيوط', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('031', 'مبنى الاذاعة والتليفزيون - ماسبييرو,القاهرة,مصر', '3', 'مبنى الاذاعة والتليفزيون - ماسبييرو', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('231', 'رقم 49 شارع عمر لطفى -الابراهيمية,الاسكندرية,مصر', '3', 'رقم 49 شارع عمر لطفى -الابراهيمية', 'الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('608', 'مجمع ماجدة للفنون -المحور المركزى,قطعة 1.3مدينة 6اكتوبر,الجيزة - مصر', '3', 'مجمع ماجدة للفنون -المحور المركزى', 'قطعة 1.3مدينة 6اكتوبر', 
    'الجيزة - مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('601', '6اكتوبر,الجيزة, مصر', '3', '6اكتوبر', 'الجيزة', 
    ' مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('480', 'شارع الجمهورية,البحيرة,مصر', '3', 'شارع الجمهورية', 'البحيرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('033', ' 15 شارع الحسن - ناصية شارع يثرب,الجيزة,مصر', '3', ' 15 شارع الحسن - ناصية شارع يثرب', 'الجيزة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('285', '109 شارع الخديوى - باب سدرة,الاسكندرية,مصر', '3', '109 شارع الخديوى - باب سدرة', 'الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('483', 'شارع الجلاء,البحيرة,مصر', '3', 'شارع الجلاء', 'البحيرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('245', 'ميناء الدخيلة,الاسكندرية,مصر', '3', 'ميناء الدخيلة', 'الاسكندرية', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('065', 'فندق جراند حياة,كورنيش النيل جادرن سيتى,القاهرة', '3', 'فندق جراند حياة', 'كورنيش النيل جادرن سيتى', 
    'القاهرة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('191', '4 شارع اسماعيل القبانى,مدينة نصر,القاهرة', '3', '4 شارع اسماعيل القبانى', 'مدينة نصر', 
    'القاهرة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('196', 'نادى الزهور,شارع يوسف عباس مدينة نصر,القاهرة', '3', 'نادى الزهور', 'شارع يوسف عباس مدينة نصر', 
    'القاهرة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('604', '17 شارع عبد الحليم محمود,الحى المتميز المجاورة الرابعة,6 اكتوبر', '3', '17 شارع عبد الحليم محمود', 'الحى المتميز المجاورة الرابعة', 
    '6 اكتوبر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('609', 'تقاطع المريوطية عمارات,الفيصلية عمارة 1 فيصل,الجيزة', '3', 'تقاطع المريوطية عمارات', 'الفيصلية عمارة 1 فيصل', 
    'الجيزة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('403', 'كفر شكر,القليوبية, مصر', '3', 'كفر شكر', 'القليوبية', 
    ' مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('315', 'السويس,السويس, مصر', '3', 'السويس', 'السويس', 
    ' مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('365', 'شرم الشيخ,جنوب سيناء,مصر', '3', 'شرم الشيخ', 'جنوب سيناء', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS3, 
    BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('603', 'المنيل,,القاهرة', '3', 'المنيل', 'القاهرة', 
    'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('665', 'السد العالى,اسوان,مصر', '3', 'السد العالى', 'اسوان', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('627', 'شارع الثورة بجوار مدرسة الاميرية,بنى مزار,المنيا', '3', 'شارع الثورة بجوار مدرسة الاميرية', 'بنى مزار', 
    'المنيا', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('628', 'ناصية شارعى حسن حميدة والإمام البخا,سمالوط-المنيا,المنيا', '3', 'ناصية شارعى حسن حميدة والإمام البخا', 'سمالوط-المنيا', 
    'المنيا', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('636', 'تقاطع جمال عبد الناصر مع امهات المس,دير مواس,المنيا', '3', 'تقاطع جمال عبد الناصر مع امهات المس', 'دير مواس', 
    'المنيا', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('633', '68 ش بور سعيد والرسل,ابو قرقاص المنيا,المنيا', '3', '68 ش بور سعيد والرسل', 'ابو قرقاص المنيا', 
    'المنيا', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('205', 'ابو قير,ابو قير,الاسكندرية', '3', 'ابو قير', 'ابو قير', 
    'الاسكندرية', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('185', 'مدينة 6 اكتوبر,الجيزة,الجيزة', '3', 'مدينة 6 اكتوبر', 'الجيزة', 
    'الجيزة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('075', '16 ش بغداد,الكورية - مصر الجديدة,القاهره', '3', '16 ش بغداد', 'الكورية - مصر الجديدة', 
    'القاهره', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('770', 'حى السبعين أمام المخبز,كوم أمبو,كوم أمبو', '3', 'حى السبعين أمام المخبز', 'كوم أمبو', 
    'كوم أمبو', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('505', 'واحة سيوة,مشروع التكية,مدينة سيوة محافظة مطروح', '3', 'واحة سيوة', 'مشروع التكية', 
    'مدينة سيوة محافظة مطروح', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('654', 'ش احمد عرابى,طريق اسيوط سوهاج بجوار حديقة الزهور,سوهاج', '3', 'ش احمد عرابى', 'طريق اسيوط سوهاج بجوار حديقة الزهور', 
    'سوهاج', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('077', 'اخر شارع التسعين,مول مون فالى,القاهره', '3', 'اخر شارع التسعين', 'مول مون فالى', 
    'القاهره', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('068', 'القطامية هايتس التجمع الخامس,القاهرةالجديدة,القاهرة', '3', 'القطامية هايتس التجمع الخامس', 'القاهرةالجديدة', 
    'القاهرة', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('093', 'عمارة 1 14 سابقا عمارات العبور,طريق صلاح سالم-م الجديدة-القاهرة,مصر', '3', 'عمارة 1 14 سابقا عمارات العبور', 'طريق صلاح سالم-م الجديدة-القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('115', '6019 تقسيم المنطقة ج شياخة المقطم,قسم الخليفة - القاهرة,مصر', '3', '6019 تقسيم المنطقة ج شياخة المقطم', 'قسم الخليفة - القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('094', '36 شارع الجلاء,القاهرة,مصر', '3', '36 شارع الجلاء', 'القاهرة', 
    'مصر', 'O', 'EG');
Insert into BRANCHES
   (BRANCH_CODE, BRANCH_LOCATION, BRANCH_TYPE, BRANCH_ADDRESS, BRANCH_ADDRESS2, 
    BRANCH_ADDRESS3, BRANCH_STATUS, BRANCH_COUNTRY)
 Values
   ('096', 'المحل رقم 1 40أ (سابقا) 38 حاليا,شارع محمد مظهر - الزمالك,مصر', '3', 'المحل رقم 1 40أ (سابقا) 38 حاليا', 'شارع محمد مظهر - الزمالك', 
    'مصر', 'O', 'EG');
COMMIT;
