package com.bdc.from41.entity;

import com.bdc.from41.core.enums.QuartersEnum;
import com.bdc.from41.entity.embedded.QuarterCloseId;
import com.haulmont.cuba.core.entity.BaseGenericIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.Versioned;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Table(name = "QUARTER_CLOSE")
@Entity(name = "QuarterClose")
public class QuarterClose extends BaseGenericIdEntity<QuarterCloseId> implements Creatable, Updatable, Versioned {
    private static final long serialVersionUID = 2797977822759491704L;

    @EmbeddedId
    private QuarterCloseId id;

    @Column(name = "CREATE_TS")
    private Date createTs;

    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    @Column(name = "UPDATE_TS")
    private Date updateTs;

    @Column(name = "UPDATED_BY", length = 50)
    private String updatedBy;

    @Version
    @Column(name = "VERSION", nullable = false)
    private Integer version;

    @Column(name = "SUPPLY_NO")
    private Long supplyNo;

    @Column(name = "QUARTER_ID", nullable = false,insertable = false,updatable = false)
    @NotNull
    private Integer quarter;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "FINANCIAL_YEAR_ID", insertable = false, updatable = false)
    @NotNull
    private FinancialYear financialYear;

    public void setQuarter(QuartersEnum quarter) {
        this.quarter = quarter == null ? null : quarter.getId();
    }

    public QuartersEnum getQuarter() {
        return quarter == null ? null : QuartersEnum.fromId(quarter);
    }

    public void setFinancialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
    }

    public FinancialYear getFinancialYear() {
        return financialYear;
    }

    public Long getSupplyNo() {
        return supplyNo;
    }

    public void setSupplyNo(Long supplyNo) {
        this.supplyNo = supplyNo;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    @Override
    public void setId(QuarterCloseId id) {
        this.id = id;
    }

    @Override
    public QuarterCloseId getId() {
        return id;
    }
}