package com.bdc.from41.core.enums;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum TransactionTypeEnum implements EnumClass<Integer> {

    SUPPLIES(1),
    SERVICES(2),
    BROKERAGE_AND_FEES(3);

    private Integer id;

    TransactionTypeEnum(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static TransactionTypeEnum fromId(Integer id) {
        for (TransactionTypeEnum at : TransactionTypeEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}