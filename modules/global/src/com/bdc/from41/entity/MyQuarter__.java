package com.bdc.from41.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "QUARTERS")
@Entity(name = "MyQuarter__")
@NamePattern("%s|name")
public class MyQuarter__ extends BaseIntegerIdEntity {
    private static final long serialVersionUID = 5672336037923526371L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "FROM_MONTH")
    private String monthes;

    public String getMonthes() {
        return monthes;
    }

    public void setMonthes(String monthes) {
        this.monthes = monthes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}