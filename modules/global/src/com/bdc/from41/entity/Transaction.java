package com.bdc.from41.entity;

import com.bdc.from41.core.enums.QuartersEnum;
import com.bdc.from41.core.enums.TransactionStatusEnum;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.Versioned;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "TRANSACTIONS")
@Entity(name = "Transaction")
public class Transaction extends BaseIntegerIdEntity implements Creatable, Updatable, Versioned {
    private static final long serialVersionUID = 5283695278870978490L;


    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRANCH_CODE_ID")
    @NotNull
    private Branch branch;

    @Column(name = "USER_ACCOUNT_NUMBER")
    private Long userAccountNumber;

    @Column(name = "COMMISSION")
    private BigDecimal commission;

    @Column(name = "STATUS")
    private String status = TransactionStatusEnum.SAVED.getId();

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "FINANCIAL_YEAR_ID")
    private FinancialYear financialYear;

    @Column(name = "QUARTER_ID", nullable = false)
    @NotNull
    private Integer quarter;

    @Column(name = "DISCOUNT_TYPE")
    private Double discountType = 1.0;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVESTOR_ID")
    private Investor investor;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @ManyToOne(optional = false)
    @JoinColumn(name = "TRANSACTION_TYPE_ID")
    @NotNull
    private TransactionType transactionType;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "TRANSACTION_DATE", nullable = false)
    private Date transactionDate;

    @Column(name = "CURENCY")
    private Integer curency = 1;

    @Column(name = "GROSS_AMOUNT", nullable = false)
    @DecimalMin(message = "{msg://err.grossAmount.valueMustbe300orGreater}", value = "300")
    @NotNull
    private Double grossAmount;

    @Column(name = "NET_AMOUNT")
    private Double netAmount;

    @Column(name = "REGISTERATION_NUMBER", columnDefinition = "varchar(255)")
    @Min(message = "min length must is 9 digits", value = 9)
    private Integer registerationNumber;

    @Column(name = "CREATE_TS")
    private Date createTs;

    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    @Column(name = "UPDATE_TS")
    private Date updateTs;

    @Column(name = "UPDATED_BY", length = 50)
    private String updatedBy;

    @Version
    @Column(name = "VERSION", nullable = false)
    private Integer version;

    public Long getUserAccountNumber() {
        return userAccountNumber;
    }

    public void setUserAccountNumber(Long userAccountNumber) {
        this.userAccountNumber = userAccountNumber;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public void setQuarter(QuartersEnum quarter) {
        this.quarter = quarter == null ? null : quarter.getId();
    }

    public QuartersEnum getQuarter() {
        return quarter == null ? null : QuartersEnum.fromId(quarter);
    }

    public void setStatus(TransactionStatusEnum status) {
        this.status = status == null ? null : status.getId();
    }

    public TransactionStatusEnum getStatus() {
        return status == null ? null : TransactionStatusEnum.fromId(status);
    }


    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Branch getBranch() {
        return branch;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public Double getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(Double grossAmount) {
        this.grossAmount = grossAmount;
    }

    public Integer getCurency() {
        return curency;
    }

    public void setCurency(Integer curency) {
        this.curency = curency;
    }

    public Double getDiscountType() {
        return discountType;
    }

    public void setDiscountType(Double discountType) {
        this.discountType = discountType;
    }

    public Integer getRegisterationNumber() {
        return registerationNumber;
    }

    public void setRegisterationNumber(Integer registerationNumber) {
        this.registerationNumber = registerationNumber;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    public FinancialYear getFinancialYear() {
        return financialYear;
    }

    public void setFinancialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
    }



}