package com.bdc.from41.web.screens.transaction;

import com.bdc.from41.core.enums.QuartersEnum;
import com.bdc.from41.core.enums.TransactionStatusEnum;
import com.bdc.from41.core.enums.UserTypeEnum;
import com.bdc.from41.entity.*;
import com.bdc.from41.service.ITransactionService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.components.LookupPickerField;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@UiController("Transaction.Montior")
@UiDescriptor("transaction-monitor.xml")
@LookupComponent("transactionsTable")
@LoadDataBeforeShow
public class TransactionMontior extends StandardLookup<Transaction> {
    @Inject
    private LookupField<QuartersEnum> quarterDDL;
    @Inject
    private LookupField<FinancialYear> financialYearDDL;
    @Inject
    private LookupPickerField<Branch> branchesLF;
    @Inject
    private LookupField<TransactionStatusEnum> status;

    @Inject
    private DataManager dataManager;
    @Inject
    private CollectionLoader<Transaction> transactionsDl;
    @Inject
    private ITransactionService transactionService;
    @Inject
    private Messages messages;
    @Inject
    private Notifications notifications;

    @Inject
    private LookupField<TaxesUser> users;
    @Inject
    private CollectionLoader<TaxesUser> taxesUsersDl;
    @Inject
    private CollectionContainer<Transaction> transactionsDc;

    @Inject
    private Button cancelBtn;
    @Inject
    private Button searchBtn;
    @Inject
    private Logger log;
    @Inject
    private Button excelBtn;

    @Subscribe("branchesLF")
    public void onBranchesLFValueChange(HasValue.ValueChangeEvent<Branch> event) {
        taxesUsersDl.load();

    }

    @Install(to = "taxesUsersDl", target = Target.DATA_LOADER)
    private List<TaxesUser> taxesUsersDlLoadDelegate(LoadContext<TaxesUser> loadContext) {

        LoadContext.Query query = loadContext.getQuery();
        Branch branch = branchesLF.getValue();
        if (branch != null) {
            query.setQueryString(" select e from TaxesUser e where e.branch.id= :branchId")
                    .setParameter("branchId", branch.getId());
            return dataManager.loadList(loadContext);
        }
        return new ArrayList<>();
    }


    @Install(to = "transactionsDl", target = Target.DATA_LOADER)
    private List<Transaction> transactionsDlLoadDelegate(LoadContext<Transaction> loadContext) {


        Branch branch = branchesLF.getValue();
        TaxesUser user = users.getValue();
        Long accountNo=(user!=null)? user.getAccountNo():null;

        UserTypeEnum userType =(user!=null)?UserTypeEnum.valueOf(user.getGroup().getName()):UserTypeEnum.BRANCH;


        transactionsDl.setMaxResults(50);
        Optional<FinancialYear> finOpt = Optional.ofNullable(financialYearDDL.getValue());
        Optional<QuartersEnum> quarterOpt = Optional.ofNullable(quarterDDL.getValue());
        Optional<TransactionStatusEnum> statusEnumOptional = Optional.ofNullable(status.getValue());
        Optional<Branch>branchOptional=Optional.ofNullable(branchesLF.getValue());
        if (quarterOpt.isPresent() && finOpt.isPresent() && statusEnumOptional.isPresent()) {


            List result = transactionService.getTransactions(finOpt.get().getId(), quarterOpt.get().getId(), statusEnumOptional.get(), userType, (branchOptional.isPresent())?branch.getId():null, accountNo, transactionsDc.getView());

            if (result.size() == 0)
                notifications.create().withType(Notifications.NotificationType.WARNING).withCaption(messages.getMainMessage("msg.noResult")).show();
            return result;
        }

        return new ArrayList<>();

    }


    public void onSearchBtnClick() {
        Optional<QuartersEnum> quarterOptional = Optional.ofNullable(quarterDDL.getValue());
        Optional<FinancialYear> financialYearOptional = Optional.ofNullable(financialYearDDL.getValue());
        Optional<TransactionStatusEnum> statusEnumOptional = Optional.ofNullable(status.getValue());

        if (!quarterOptional.isPresent()) {
            notifications.create().withCaption(messages.getMainMessage("msg.error.quarter.required")).withHideDelayMs(100).show();
            return;
        }

        if (!financialYearOptional.isPresent()) {
            notifications.create().withCaption(messages.getMainMessage("msg.error.financialYear.required")).withHideDelayMs(100).show();

            return;
        }
        if (!statusEnumOptional.isPresent()) {
            notifications.create().withCaption(messages.getMainMessage("msg.error.transaction.status.required")).withHideDelayMs(100).show();

            return;
        }

        try {
            enableSearchForm(false);
            transactionsDl.load();
        } catch (Exception exception) {
exception.printStackTrace();
        }

    }

    public void OnCancelBtnClick() {
        enableSearchForm(true);
    }


    private void enableSearchForm(boolean flag) {
        quarterDDL.setEnabled(flag);
        financialYearDDL.setEnabled(flag);
        searchBtn.setEnabled(flag);
        status.setEnabled(flag);
        users.setEnabled(flag);
        excelBtn.setEnabled(!flag);
        branchesLF.setEnabled(flag);

    }


}