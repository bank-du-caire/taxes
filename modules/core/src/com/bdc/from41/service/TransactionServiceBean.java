package com.bdc.from41.service;

import com.bdc.from41.core.enums.QuarterMonthesEnum;
import com.bdc.from41.core.enums.TransactionStatusEnum;
import com.bdc.from41.core.enums.UserTypeEnum;
import com.bdc.from41.entity.*;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.TransactionalDataManager;
import com.haulmont.cuba.core.entity.KeyValueEntity;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.core.global.ValueLoadContext;
import com.haulmont.cuba.core.global.View;
import org.apache.groovy.metaclass.MetaClass;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service(ITransactionService.NAME)
public class TransactionServiceBean implements ITransactionService {
    @Inject
    private DataManager dataManager;
    @Inject
    private Persistence persistence;
    @Inject
    private TransactionalDataManager txDataManager;
    @Inject
    private Logger log;
    @Inject
    private UserSessionSource userSessionSource;

    @Override
    public List<MyQuarter__> getSuggestedQuarters() {


        Date currentDate = new Date();

        int currentQuarter = this.getCurrentQuarter(currentDate);
        int pastQuarter = this.getPastQuarter(currentDate);

        MyQuarter__ q = dataManager.load(MyQuarter__.class).id(currentQuarter).one();

        List quarters = new ArrayList(Arrays.asList(q));
        if (pastQuarter != 0) quarters.add(dataManager.load(MyQuarter__.class).id(pastQuarter).one());
        return quarters;
    }


    private int getCurrentQuarter(Date currentDate) {

        int[] quarters = {1, 4, 7, 10, 13};
        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        int currentMonth = Integer.parseInt(sdf.format(currentDate));

        int i;
        for (i = 0; i < quarters.length; i++) {
            if (currentMonth >= quarters[i] && currentMonth < quarters[i + 1]) {
                break;
            }
        }
        return i + 1;

    }


    private int getPastQuarter(Date currentDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        int currentMonth = Integer.parseInt(sdf.format(currentDate));

        if (currentMonth == 1) return 4;
        if (currentMonth == 4) return 1;
        if (currentMonth == 7) return 2;
        if (currentMonth == 10) return 3;
        return 0;
    }

    @Override
    public FinancialYear getActiveFinancialYears() {

        FinancialYear activeFinancialYear = this.dataManager.load(FinancialYear.class).query("e.active = '1'").one();
        return activeFinancialYear;

    }

    @Override
    public void restFinancialYears(int id) {

        List<FinancialYear> fns = this.dataManager.load(FinancialYear.class).query("e.active = '1' and e.id <> ?1", id).list();
        for (FinancialYear fn : fns) {
            fn.setActive(false);
            txDataManager.save(fn);
        }

    }

    @Override
    public List getTransaction(int finYearId, int branchCodeId, int quarterId, TransactionStatusEnum status) {


        StringBuilder query = new StringBuilder("select e from Transaction e where 1=1 ");
        if (finYearId != 0) {
            query.append(" and e.financialYear.id=" + finYearId);
        }

        if (branchCodeId != 0) {
            query.append("  and e.branchCode.id=" + branchCodeId);
        }

        if (quarterId != 0) {
            query.append("  and e.quarter.id=" + quarterId);
        }

        if (status != null) {
            query.append("  and e.status.id=" + status);
        }

        List transactions = dataManager.load(Transaction.class).query(query.toString()).view("transactionBrowseView").list();
        return transactions;
    }

    @Override
    public BigDecimal getTotalTransactionsOFQuarter(int quarterId, int financialYearId) {
        BigDecimal totalTransactions = BigDecimal.ZERO;
        String query = "select sum(e.commission) from Transaction e where e.quarter = :quarterId and e.financialYear.id = :finId";

        try {
            totalTransactions = dataManager.loadValue(query, BigDecimal.class)
                    .parameter("quarterId", quarterId)
                    .parameter("finId", financialYearId)
                    .list().get(0);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return (totalTransactions == null) ? BigDecimal.ZERO : totalTransactions;
    }

    @Override
    public int validateQuartertransactions(int finId, int quarter, UserTypeEnum usertype, String branchCode, Long userAccountNumber) {
        Optional<BigDecimal> GLBalance = Optional.of(getQuarterGLBlance(finId, quarter, usertype, branchCode, userAccountNumber));
        Optional<BigDecimal> TransactionBalance = Optional.of(getQuarterTransactionsBlance(finId, quarter, usertype, branchCode, userAccountNumber));
        TransactionBalance.orElse(new BigDecimal(0.0));
        GLBalance.orElse(new BigDecimal(0.0));


        log.info("GL Balance =>" + (GLBalance.get().abs().doubleValue()));


        log.info("Transaction Balance =>" + TransactionBalance.get().toString());
        int value = Double.compare(TransactionBalance.get().abs().doubleValue(), GLBalance.get().abs().doubleValue());
        if (value == 0) {


            changeTransactionToValidatedStatus(finId, quarter, usertype, branchCode, userAccountNumber);

        }
        return value;


    }

    @Override
    public BigDecimal getQuarterTransactionsBlance(int finId, int quarter, UserTypeEnum usertype, String branchCode, Long userAccountNumber) {
        BigDecimal result = BigDecimal.ZERO;
        switch (usertype) {


            case HEAD:
                result = getQuarterHeadTransactionsBlance(finId, quarter, userAccountNumber);
                break;
            case BRANCH:
                result = getQuarterBranchTransactionsBlance(finId, quarter, branchCode);
                break;
        }

        return result;
    }

    public BigDecimal getQuarterHeadTransactionsBlance(int finId, int quarter, Long userAccountNumber) {

        com.haulmont.cuba.core.Transaction tx = persistence.getTransaction();


        String query = "  select sum(e.commission)from Transaction e " +
                "where  e.financialYear.id = :year" +
                " and e.userAccountNumber = :userAccountNumber " +
                "and e.quarter = :quarterId";


        Query queryRunner = persistence.getEntityManager().createQuery(query);
        queryRunner.setParameter("year", finId);
        queryRunner.setParameter("userAccountNumber", userAccountNumber);
        queryRunner.setParameter("quarterId", quarter);
        BigDecimal balance = (BigDecimal) queryRunner.getSingleResult();

        tx.end();
        return (balance == null) ? BigDecimal.ZERO : balance;
    }

    public BigDecimal getQuarterBranchTransactionsBlance(int finId, int quarter, String branchCode) {

        com.haulmont.cuba.core.Transaction tx = persistence.getTransaction();


        String query = "  select sum(e.commission)from Transaction e " +
                "where  e.financialYear.id = :year" +
                " and e.branch.code like :branchCode " +
                "and e.quarter = :quarterId";


        Query queryRunner = persistence.getEntityManager().createQuery(query);
        queryRunner.setParameter("year", finId);
        queryRunner.setParameter("branchCode", branchCode);
        queryRunner.setParameter("quarterId", quarter);
        BigDecimal balance = (BigDecimal) queryRunner.getSingleResult();

        tx.end();
        return (balance == null) ? BigDecimal.ZERO : balance;
    }


    @Override
    @Transactional
    public void changeTransactionToValidatedStatus(int finId, int quarter, UserTypeEnum usertype, String branchCode, Long userAccountNumber) {


        switch (usertype) {


            case HEAD:
                changeHeadTransactionToValidatedStatus(finId, quarter, userAccountNumber);
                break;
            case BRANCH:
                changeBranchTransactionToValidatedStatus(finId, quarter, branchCode);
                break;
        }


    }


    @Transactional
    public void changeHeadTransactionToValidatedStatus(int finId, int quarter, Long userAccountNumber) {


//        ==========================================================================================================================
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("year", finId);
        params.put("userAccountNumber", userAccountNumber);
        params.put("quarterId", quarter);
        params.put("status", TransactionStatusEnum.SAVED.getId());
        String query = " e.financialYear.id = :year" +
                " and e.userAccountNumber = :userAccountNumber " +
                " and e.status.id like :status " +
                "and e.quarter = :quarterId";

        List<Transaction> savedTransactions = dataManager.load(Transaction.class).query(query).setParameters(params).view("transactionBrowseView").list();
        savedTransactions.stream().forEach(e -> e.setStatus(TransactionStatusEnum.VALIDATED));


    }

    @Transactional
    public void changeBranchTransactionToValidatedStatus(int finId, int quarter, String branchCode) {


//        ==========================================================================================================================
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("year", finId);
        params.put("branchCode", branchCode);
        params.put("quarterId", quarter);
        params.put("status", TransactionStatusEnum.SAVED.getId());
        String query = " e.financialYear.id like :year" +
                " and e.branch.code like :branchCode " +
                " and e.status.id like :status " +
                "and e.quarter = :quarterId";

        List<Transaction> savedTransactions = dataManager.load(Transaction.class).query(query).setParameters(params).view("transactionBrowseView").list();
        savedTransactions.stream().forEach(e -> e.setStatus(TransactionStatusEnum.VALIDATED));


    }


    @Override
    public BigDecimal getQuarterGLBlance(int finId, int quarter, UserTypeEnum usertype, String branchCode, Long userAccountNumber) {


        BigDecimal result = BigDecimal.ZERO;
        switch (usertype) {


            case HEAD:
                result = getQuarterHeadGLBlance(finId, quarter, userAccountNumber);
                break;
            case BRANCH:
                result = getQuarterBranchGLBlance(finId, quarter, branchCode);
                break;
        }

        return result;

    }


    public BigDecimal getQuarterHeadGLBlance(int finId, int quarter, Long userAccountNumber) {

        BigDecimal balance = BigDecimal.ZERO;

        FinancialYear financialYear = dataManager.load(FinancialYear.class).id(finId).one();
        Date fromDate = financialYear.getFromDate();
        Date toDate = financialYear.getToDate();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");

        switch (quarter) {
            case 1:
                c.setTime(fromDate);
                c.set(Calendar.DAY_OF_MONTH, 1);
                c.set(Calendar.YEAR, Integer.parseInt(yearFormat.format(financialYear.getFromDate())));
                c.set(Calendar.MONTH, 1);
                fromDate = c.getTime();

                c.setTime(toDate);
                c.set(Calendar.MONTH, 3);
                c.set(Calendar.DAY_OF_MONTH, 31);
                c.set(Calendar.YEAR, Integer.parseInt(yearFormat.format(financialYear.getFromDate())));
                toDate = c.getTime();


                break;
            case 2:
                c.setTime(fromDate);
                c.set(Calendar.DAY_OF_MONTH, 1);
                c.set(Calendar.YEAR, Integer.parseInt(yearFormat.format(financialYear.getFromDate())));
                c.set(Calendar.MONTH, 4);
                fromDate = c.getTime();

                c.setTime(toDate);
                c.set(Calendar.MONTH, 6);
                c.set(Calendar.DAY_OF_MONTH, 30);
                c.set(Calendar.YEAR, Integer.parseInt(yearFormat.format(financialYear.getFromDate())));
                toDate = c.getTime();
                break;
            case 3:
                c.setTime(fromDate);
                c.set(Calendar.DAY_OF_MONTH, 7);
                c.set(Calendar.YEAR, Integer.parseInt(yearFormat.format(financialYear.getFromDate())));
                c.set(Calendar.MONTH, 1);
                fromDate = c.getTime();

                c.setTime(toDate);
                c.set(Calendar.MONTH, 9);
                c.set(Calendar.DAY_OF_MONTH, 30);
                c.set(Calendar.YEAR, Integer.parseInt(yearFormat.format(financialYear.getFromDate())));
                toDate = c.getTime();
                break;
            case 4:
                c.setTime(fromDate);
                c.set(Calendar.DAY_OF_MONTH, 10);
                c.set(Calendar.YEAR, Integer.parseInt(yearFormat.format(financialYear.getFromDate())));
                c.set(Calendar.MONTH, 1);
                fromDate = c.getTime();

                c.setTime(toDate);
                c.set(Calendar.MONTH, 12);
                c.set(Calendar.DAY_OF_MONTH, 31);
                c.set(Calendar.YEAR, Integer.parseInt(yearFormat.format(financialYear.getFromDate())));
                toDate = c.getTime();
                break;

        }


        String query = "  select sum(e.amount) from View888 e where " +
                " e.accountNumber = :userAccountNo " +
                "and e.transactionDate between :fromDate and :toDate ";

        try {
            balance = dataManager.loadValue(query, BigDecimal.class)
                    .parameter("fromDate", fromDate)
                    .parameter("toDate", toDate)
                    .parameter("userAccountNo", String.valueOf(userAccountNumber))
                    .list().get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (balance == null) ? BigDecimal.ZERO : balance.abs();
    }

    public BigDecimal getQuarterBranchGLBlance(int finId, int quarter, String branchCode) {
        FinancialYear year = dataManager.load(FinancialYear.class).id(finId).one();

        BigDecimal balance = BigDecimal.ZERO;
        String[] monthes = null;
        switch (quarter) {
            case 1:
                monthes = QuarterMonthesEnum.fristQuarter[1].split(",");
                break;
            case 2:
                monthes = QuarterMonthesEnum.secondQuarter[1].split(",");
                break;
            case 3:
                monthes = QuarterMonthesEnum.thirdQuarter[1].split(",");
                break;
            case 4:
                monthes = QuarterMonthesEnum.fourthQuarter[1].split(",");
                break;

        }


        String query = "  select sum(e.balance) from GLBalance e where " +
                " e.id.finYear like :year and e.id.branchCode like :branchCode and e.id.periodCode in :periodCode" +
                " and e.glCode like :glAccountNo";

        try {
            balance = dataManager.loadValue(query, BigDecimal.class).
                    parameter("year", year.getFinYearCode())
                    .parameter("branchCode", branchCode)
                    .parameter("periodCode", Arrays.asList(monthes))
                    .parameter("glAccountNo", ((TaxesUser) userSessionSource.getUserSession().getUser()).getBranch().getGlAccount().getGlAccountNo().toString())
                    .list().get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (balance == null) ? BigDecimal.ZERO : balance.abs();
    }


    @Override
    public List getQuarterMonthes(int quarterId) {
        List monthes = new ArrayList();

        switch (quarterId) {
            case 1:
                monthes = Arrays.asList(QuarterMonthesEnum.fristQuarter[1].split(",").clone());
                break;
            case 2:
                monthes = Arrays.asList(QuarterMonthesEnum.secondQuarter[1].split(",").clone());
                break;
            case 3:
                monthes = Arrays.asList(QuarterMonthesEnum.thirdQuarter[1].split(",").clone());
                break;
            case 4:
                monthes = Arrays.asList(QuarterMonthesEnum.fourthQuarter[1].split(",").clone());
                break;

        }

        return monthes;
    }

    @Override
    public List getTransactions(int finId, int quarter, TransactionStatusEnum status, UserTypeEnum usertype, String branchCode, Long userAccountNumber, View view) {

        List result = new ArrayList();
        switch (usertype) {


            case HEAD:
                result = getUserHeadTransaction(finId, quarter, status, userAccountNumber, view);
                break;
            case BRANCH:
                result = getUserBranchTransaction(finId, quarter, status, branchCode, view);
                break;
        }

        return result;

    }

    @Override
    public List getUserBranchTransaction(int finId, int quarter, TransactionStatusEnum status, String branchCode, View view) {



        String queryString ="select e from Transaction e" +
                " where e.financialYear.id = :finId " +
                "and e.quarter= :quarter " +
                "and e.status=:status "
                ;


        Map<String, Object> params = new HashMap<>();
        if (branchCode != null) {
            queryString += " and e.branch.id = :branchId ";
            params.put("branchId", branchCode);
        }

        params.put("quarter", quarter);
        params.put("status", status.getId());
        params.put("finId", finId);
        return dataManager.load(Transaction.class).query(queryString).setParameters(params).view(view).list();
    }

    @Override
    public List getUserHeadTransaction(int finId, int quarter, TransactionStatusEnum status, Long userAccountNumber, View view) {


        return dataManager.load(Transaction.class).query("select e from Transaction e" +
                " where e.financialYear.id = :finId " +
                " and e.quarter= :quarter  " +
                " and  e.status=:status" +
                "  and e.userAccountNumber =:userAccountNumber  ")
                .parameter("finId", finId)
                .parameter("quarter", quarter)
                .parameter("status", status.getId())
                .parameter("userAccountNumber", userAccountNumber).view(view).list();
    }


    @Override
    public List getReportTransactions(int finId, int quarter, UserTypeEnum usertype, String branchCode, Long userAccountNumber, View view) {

        List result = new ArrayList();
        switch (usertype) {


            case HEAD:
                result = getReportUserTransaction(finId, quarter, TransactionStatusEnum.VALIDATED, userAccountNumber, view);
                break;
            case BRANCH:
                result = getReportBranchTransaction(finId, quarter, TransactionStatusEnum.VALIDATED, branchCode, view);
                break;
        }

        return result;

    }


    public List getReportBranchTransaction(int finId, int quarter, TransactionStatusEnum status, String branchCode, View view) {


        String query = "select e from TransactionReport e where  e.quarterNum = :quarter " +
                "and e.financialYearId = :finId " +
                "and e.status =:status";
        Map<String, Object> params = new HashMap<>();
        if (branchCode != null) {
            query += " and e.branchCode= :branchId";
            params.put("branchId", branchCode);
        }

        params.put("quarter", quarter);
        params.put("status", status.getId());
        params.put("finId", finId);
        return dataManager.load(TransactionReport.class).query(query).setParameters(params).view(view).list();

    }


    public List getReportUserTransaction(int finId, int quarter, TransactionStatusEnum status, Long userAccountNumber, View view) {


        Map<String, Object> params = new HashMap<>();
        String query = "select e from TransactionReport e" +
                " where e.financialYearId= :finId " +
                " and e.quarterNum= :quarter  " +
                " and  e.status=:status" +
                "  and e.userAccountNumber =:userAccountNumber ";

        params.put("quarter", quarter);
        params.put("status", status.getId());
        params.put("finId", finId);
        params.put("userAccountNumber", userAccountNumber);


        return dataManager.load(Transaction.class).query(query)
                .setParameters(params).view(view).list();
    }
@Override
    public List getInvestorCertficate(ValueLoadContext valueLoadContext, int financialYearId, String branchId, int investorId) {


        String sqlConditions = " and trx.financialYear.id= :financialYearId and trx.investor.id= :investorId  ";
        Map params = new HashMap();
        params.put("financialYearId", financialYearId);
        params.put("investorId", investorId);
        if (branchId != null && !branchId.isEmpty()) {
            sqlConditions += " and trx.branch.id= :branchId ";
            params.put("branchId", branchId);
        }


        String queryString = " select  trx.financialYear as financialYear,\n" +
                "                     trx.branch as branch ,\n" +
                "                     trx.quarter,\n" +
                "                     trx.investor as investor,\n" +
                "                      quarterClose.supplyNo as supplyNo ,\n" +
                "                     sum(trx.grossAmount) as totalAmount ,\n" +
                "                     sum(trx.commission) as totalTaxes\n" +
                "\n" +
                "\n" +
                "                      from Transaction trx  , QuarterClose quarterClose\n" +
                "                     where trx.financialYear.id=quarterClose.id.financialYearId\n" +
                "                       and trx.quarter=quarterClose.id.quarterId\n"
                + sqlConditions+
                "                     group by  trx.investor,trx.financialYear , trx.quarter, quarterClose.supplyNo ,  trx.branch";





    ValueLoadContext.Query query= valueLoadContext.getQuery();
    query.setQueryString(queryString);
    query.setParameters(params);
log.info("query =>>"+query.toString());
     return   dataManager.loadValues (valueLoadContext);






    }
}