package com.bdc.from41.core.enums;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public class QuarterMonthesEnum  {

    public static final String[] fristQuarter ={"1","JAN,FEB,MAR"};
    public static final String[] secondQuarter ={"2","APR,MAY,JUN"};
    public static final String[] thirdQuarter ={"3","JUL,AUG,SEP"};
    public static final String[] fourthQuarter ={"4","OCT,NOV,DEC"};

}