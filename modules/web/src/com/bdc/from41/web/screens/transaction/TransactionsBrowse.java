package com.bdc.from41.web.screens.transaction;

import com.bdc.from41.core.enums.QuartersEnum;
import com.bdc.from41.core.enums.TransactionStatusEnum;
import com.bdc.from41.core.enums.UserTypeEnum;
import com.bdc.from41.entity.*;
import com.bdc.from41.service.ITransactionService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.screen.LookupComponent;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@UiController("from41_Transactions.browse")
@UiDescriptor("transactions-browse.xml")
@LookupComponent("transactionsTable")
@LoadDataBeforeShow
public class TransactionsBrowse extends StandardLookup<Transaction> {
    @Inject
    private CollectionContainer<Transaction> transactionsDc;
    @Inject
    private Button validateBtn;
    @Inject
    private Logger log;
    @Inject
    private DataManager dataManager;
    @Inject
    private ITransactionService transactionService;
    @Inject
    private LookupField<QuartersEnum> quarterDDL;
    @Inject
    private UserSession userSession;
    @Inject
    private Notifications notifications;
    @Inject
    private MessageBundle messageBundle;
    @Inject
    private LookupField<FinancialYear> financialYearDDL;
    @Inject
    private CollectionLoader<Transaction> transactionsDl;
    @Inject
    private Messages messages;
    @Inject
    private Button searchSavedBtn;
    @Inject
    private CollectionLoader<Branch> branchesDl;
    @Inject
    private LookupPickerField<Branch> branchesLF;
    @Inject
    private LookupField<TransactionStatusEnum> status;
    TaxesUser user;
    UserTypeEnum userType;
    @Inject
    private GroupTable<Transaction> transactionsTable;

    @Subscribe
    public void onInit(InitEvent event) {
        user = ((TaxesUser) userSession.getUser());
        userType = UserTypeEnum.fromId(user.getGroup().getName());


    }

    @Subscribe(id = "branchesDl", target = Target.DATA_LOADER)
    public void onBranchesDlPostLoad(CollectionLoader.PostLoadEvent<Branch> event) {

        if (user.getBranch() != null) {
            Branch b = event.getLoadedEntities().get(0);
            branchesLF.setValue(b);
            branchesLF.setEnabled(false);
        }


    }


    @Install(to = "transactionsDl", target = Target.DATA_LOADER)
    private List<Transaction> transactionsDlLoadDelegate(LoadContext<Transaction> loadContext) {
        transactionsDl.setMaxResults(50);
        Optional<FinancialYear> finOpt = Optional.ofNullable(financialYearDDL.getValue());
        Optional<QuartersEnum> quarterOpt = Optional.ofNullable(quarterDDL.getValue());
        Optional<TransactionStatusEnum> statusEnumOptional = Optional.ofNullable(status.getValue());

        if (quarterOpt.isPresent() && finOpt.isPresent() && statusEnumOptional.isPresent()) {


            List result = transactionService.getTransactions(finOpt.get().getId(), quarterOpt.get().getId(), statusEnumOptional.get(), userType, user.getBranch().getId(), user.getAccountNo(), transactionsDc.getView());

            if (result.size() == 0)
                notifications.create().withType(Notifications.NotificationType.WARNING).withCaption(messageBundle.getMessage("msg.noResult")).show();
            return result;
        }

        return new ArrayList<>();
    }


    @Install(to = "branchesDl", target = Target.DATA_LOADER)
    private List<Branch> branchesDlLoadDelegate(LoadContext<Branch> loadContext) {
        branchesDl.setMaxResults(10);

        Optional<Branch> branchOptional = Optional.ofNullable(user.getBranch());

        if (branchOptional.isPresent()) {

            LoadContext.Query query = loadContext.getQuery();
            query.setQueryString(" select e from Branch e where e.id= :branchId").setParameter("branchId", branchOptional.get().getId());


        }

        return dataManager.loadList(loadContext);
    }


    @Subscribe("validateBtn")
    public void onValidateBtnClick(Button.ClickEvent event) {

        FinancialYear year = transactionService.getActiveFinancialYears();
        int quarterId = this.quarterDDL.getValue().getId();

        String branchCode = ((TaxesUser) userSession.getUser()).getBranch().getCode();

        int result = this.transactionService.validateQuartertransactions(year.getId(), quarterId, userType, branchCode, user.getAccountNo());

        switch (result) {
            case 1:
                notifications.create().withType(Notifications.NotificationType.ERROR).withCaption(messageBundle.getMessage("msg.TBGTGLB")).show();
                break;

            case -1:
                notifications.create().withType(Notifications.NotificationType.ERROR).withCaption(messageBundle.getMessage("msg.TBLTGLB")).show();
                break;
            case 0:
                List<Transaction> transactions = getTransactions(quarterDDL.getValue().getId(), TransactionStatusEnum.SAVED);
                transactionsDc.getMutableItems().addAll(transactions);
                notifications.create().withType(Notifications.NotificationType.HUMANIZED).withCaption(messageBundle.getMessage("msg.transaction.validate.done")).show();

                break;


        }


    }

    private List<Transaction> getTransactions(int quarterId, TransactionStatusEnum status) {
        transactionsDc.getMutableItems().clear();

        FinancialYear fn = dataManager.load(FinancialYear.class).query(" e.active='1'").one();
        List<Transaction> transactions = transactionService.getTransaction(fn.getId(), 0, quarterId, status);
        return transactions;
    }

    public void onSearchSavedBtnClick() {

        Optional<QuartersEnum> quarterOptional = Optional.ofNullable(quarterDDL.getValue());
        Optional<FinancialYear> financialYearOptional = Optional.ofNullable(financialYearDDL.getValue());
        Optional<TransactionStatusEnum> statusEnumOptional = Optional.ofNullable(status.getValue());

        if (!quarterOptional.isPresent()) {
            notifications.create().withCaption(messages.getMainMessage("msg.error.quarter.required")).withHideDelayMs(100).show();
            return;
        }

        if (!financialYearOptional.isPresent()) {
            notifications.create().withCaption(messages.getMainMessage("msg.error.financialYear.required")).withHideDelayMs(100).show();

            return;
        }
        if (!statusEnumOptional.isPresent()) {
            notifications.create().withCaption(messages.getMainMessage("msg.error.transaction.status.required")).withHideDelayMs(100).show();

            return;
        }


        enableSearchForm(false);
        transactionsDl.load();

    }

    @Subscribe("status")
    public void onStatusValueChange(HasValue.ValueChangeEvent<TransactionStatusEnum> event) {

        validateBtn.setVisible(!event.getValue().equals(TransactionStatusEnum.VALIDATED));


    }


    private void enableSearchForm(boolean flag) {
        quarterDDL.setEnabled(flag);
        financialYearDDL.setEnabled(flag);
        searchSavedBtn.setEnabled(flag);
        validateBtn.setEnabled(!flag);
        status.setEnabled(flag);

    }


    public void OnCancelSavedBtnClick() {
        enableSearchForm(true);
    }

    @Install(to = "transactionsTable.create", subject = "afterCommitHandler")
    private void transactionsTableCreateAfterCommitHandler(Transaction transaction) {
        log.info("edit hase been closed ");
      //  transactionsDc.getMutableItems().clear();
    }





}