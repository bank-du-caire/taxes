package com.bdc.from41.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
public class GLBlanceId  implements Serializable {

    @Column(name = "FIN_YEAR", nullable = false)
    @NotNull
    private String finYear;

    @Column(name = "PERIOD_CODE", nullable = false)
    @NotNull
    private String periodCode;

    @Column(name = "BRANCH_CODE", nullable = false)
    @NotNull
    private String branchCode;

}