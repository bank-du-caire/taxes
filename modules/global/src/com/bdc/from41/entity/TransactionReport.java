package com.bdc.from41.entity;

import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.global.DesignSupport;

import javax.persistence.*;
import java.util.Date;

@DesignSupport("{'dbView':true,'generateDdl':false}")

@Table(name = "Transaction_Report")
@Entity(name = "TransactionReport")
public class TransactionReport extends BaseIntegerIdEntity {

    @Column(name = "QUARTER", nullable = false)
    private Integer quarterNum;

    @Column(name = "COMMISSION")
    private Double commission;

    @Column(name = "TOTAL_TAX")
    private Double totalTax;

    @Column(name = "INVESTOR_NAME")
    private String investorName;

    @Column(name = "INVESTOR_ADDRESS")
    private String investorAddress;

    @Column(name = "NATIONAL_ID")
    private String nationalId;

    @Column(name = "CURENCY")
    private Integer currency;

    @Column(name = "USER_ACCOUNT_NO")
    private Long userAccountNo;

    @Column(name = "BRANCH_CODE")
    private String branchCode;

    @Column(name = "FIN_YEAR", nullable = false)
    private String financialYear;

    @Column(name = "FIN_YEAR_ID", nullable = false)
    private Integer financialYearId;

    @Temporal(TemporalType.DATE)
    @Column(name = "TRANSACTION_DATE")
    private Date transactionDate;

    @Column(name = "REGISTERATION_NUMBER")
    private String registertationNumber;

    @Column(name = "FILE_NO")
    private Character fileNo;

    @Column(name = "MASSION_CODE")
    private Character massionCode;

    @Column(name = "CODE")
    private String code;

    @Column(name = "GROSS_AMOUNT")
    private Double grossAmount;

    @Column(name = "DISCOUNT_TYPE")
    private Double discountType;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "NET_AMOUNT")
    private Double netAmount;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public Long getUserAccountNo() {
        return userAccountNo;
    }

    public void setUserAccountNo(Long userAccountNo) {
        this.userAccountNo = userAccountNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Character getFileNo() {
        return fileNo;
    }

    public Integer getCurrency() {
        return currency;
    }

    public String getNationalId() {
        return nationalId;
    }

    public String getInvestorAddress() {
        return investorAddress;
    }

    public String getInvestorName() {
        return investorName;
    }

    public Double getTotalTax() {
        return totalTax;
    }

    public Double getCommission() {
        return commission;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public Double getDiscountType() {
        return discountType;
    }

    public Double getGrossAmount() {
        return grossAmount;
    }

    public String getCode() {
        return code;
    }

    public Character getMassionCode() {
        return massionCode;
    }

    public String getRegistertationNumber() {
        return registertationNumber;
    }

    public String getFinancialYear() {
        return financialYear;
    }

    public Integer getQuarterNum() {
        return quarterNum;
    }

    public void setQuarterNum(Integer quarterNum) {
        this.quarterNum = quarterNum;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public void setTotalTax(Double totalTax) {
        this.totalTax = totalTax;
    }

    public void setInvestorName(String investorName) {
        this.investorName = investorName;
    }

    public void setInvestorAddress(String investorAddress) {
        this.investorAddress = investorAddress;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }

    public void setFinancialYear(String financialYear) {
        this.financialYear = financialYear;
    }

    public Integer getFinancialYearId() {
        return financialYearId;
    }

    public void setFinancialYearId(Integer financialYearId) {
        this.financialYearId = financialYearId;
    }

    public void setRegistertationNumber(String registertationNumber) {
        this.registertationNumber = registertationNumber;
    }

    public void setFileNo(Character fileNo) {
        this.fileNo = fileNo;
    }

    public void setMassionCode(Character massionCode) {
        this.massionCode = massionCode;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setGrossAmount(Double grossAmount) {
        this.grossAmount = grossAmount;
    }

    public void setDiscountType(Double discountType) {
        this.discountType = discountType;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }
}