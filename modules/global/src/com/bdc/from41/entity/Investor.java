package com.bdc.from41.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.Versioned;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import java.util.Date;

@Table(name = "INVESTORS")
@Entity(name = "from41_Investor")
@NamePattern("%s|name")

//@SequenceGenerator(name = Investor.Investor_SEQUENCE_NAME, sequenceName = Investor.Investor_SEQUENCE_NAME, initialValue = 10,allocationSize = 30000)
public class Investor extends BaseIntegerIdEntity implements Creatable, Updatable, Versioned {
    private static final long serialVersionUID = -4667068120112912722L;

    public static final String Investor_SEQUENCE_NAME = "SEQ_ID_FROM41_INVESTOR";

    @Column(name = "NAME", unique = true)
    private String name;

    @Column(name = "PHONE", length = 14)
    private String phone;

    @Column(name = "ADDRESS", length = 1024)
    private String address;

    @Column(name = "NATIONAL_ID", columnDefinition = "varchar2(14) not null",unique = true)
    @Digits(message = "{msg://from41_Investor.nationalId.validation.Min}", integer = 15, fraction = 0)
    private Long nationalId;

    @Column(name = "TAXES_NUMBER", columnDefinition = "varchar2(9)",unique = true)
    private Integer taxesNumber;

    @Column(name = "CREATE_TS")
    private Date createTs;

    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;

    @Column(name = "UPDATE_TS")
    private Date updateTs;

    @Column(name = "UPDATED_BY", length = 50)
    private String updatedBy;

    @Version
    @Column(name = "VERSION", nullable = false)
    private Integer version;

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String getUpdatedBy() {
        return updatedBy;
    }

    @Override
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdateTs() {
        return updateTs;
    }

    @Override
    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public Integer getTaxesNumber() {
        return taxesNumber;
    }

    public void setTaxesNumber(Integer taxesNumber) {
        this.taxesNumber = taxesNumber;
    }

    public void setNationalId(Long nationalId) {
        this.nationalId = nationalId;
    }

    public Long getNationalId() {
        return nationalId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}