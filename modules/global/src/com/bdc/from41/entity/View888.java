package com.bdc.from41.entity;

import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.global.DbView;

import javax.persistence.*;
import java.util.Date;

@DbView
@Table(name = "VIEW888")
@Entity(name = "View888")
public class View888 extends BaseIntegerIdEntity {
    private static final long serialVersionUID = 502379316038826485L;

    @Column(name = "ACCOUNT_NUMBER")
    private String accountNumber;

    @Column(name = "LCY_AMOUNT")
    private String amount;

    @Temporal(TemporalType.DATE)
    @Column(name = "TRN_DT")
    private Date transactionDate;

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}