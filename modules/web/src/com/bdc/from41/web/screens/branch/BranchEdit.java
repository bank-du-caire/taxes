package com.bdc.from41.web.screens.branch;

import com.bdc.from41.entity.GLAccount;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.PersistenceHelper;
import com.haulmont.cuba.gui.components.TextField;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.Branch;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.Optional;

@UiController("Branch.edit")
@UiDescriptor("branch-edit.xml")
@EditedEntityContainer("branchDc")
@LoadDataBeforeShow
public class BranchEdit extends StandardEditor<Branch> {
    private Branch branch;
    @Inject
    private TextField<Long> glAccountField;
    @Inject
    private DataContext dataContext;
    @Inject
    private Logger log;
    @Inject
    private DataManager dataManager;


    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {
        Optional<Long> accountNumber = Optional.ofNullable(glAccountField.getValue());
        GLAccount glAccount;
        if (accountNumber.isPresent()) {


            try {
                glAccount = dataManager.load(GLAccount.class)
                        .query("e.glAccountNo = :acountNo")
                        .parameter("acountNo", accountNumber.get()).one();
            } catch (Exception e) {
                log.info("account number not Found");
                glAccount = dataContext.create(GLAccount.class);
                glAccount.setGlAccountNo(accountNumber.get());
            }

            this.getEditedEntity().setGlAccount(glAccount);
            dataContext.merge(glAccount);


        }


    }


}