package com.bdc.from41.web.screens.transactionreport;

import com.bdc.from41.core.enums.QuartersEnum;
import com.bdc.from41.core.enums.UserTypeEnum;
import com.bdc.from41.entity.Branch;
import com.bdc.from41.entity.FinancialYear;
import com.bdc.from41.entity.TaxesUser;
import com.bdc.from41.service.ITransactionService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.components.LookupPickerField;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.TransactionReport;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@UiController("TransactionReport.Montior.browse")
@UiDescriptor("transaction-report-montior.xml")
@LookupComponent("transactionReportsTable")
@LoadDataBeforeShow
public class TransactionReportMontior extends StandardLookup<TransactionReport> {

    @Inject
    private Button searchBtn;
    @Inject
    private LookupField<QuartersEnum> quarterDDL;

    @Inject
    private CollectionContainer<TransactionReport> transactionReportsDc;
    @Inject
    private DataManager dataManager;
    @Inject
    private Logger log;
    @Inject
    private Notifications notifications;
    @Inject
    private Messages messages;
    @Inject
    private LookupField<FinancialYear> financialYearDDL;

    @Inject
    private CollectionLoader<TransactionReport> transactionReportsDl;
    @Inject
    private ITransactionService iTransactionService;
    @Inject
    private Button excelBtn;
    @Inject
    private LookupField<TaxesUser> users;
    @Inject
    private LookupPickerField<Branch> branchesLF;
    @Inject
    private CollectionLoader<TaxesUser> taxesUsersDl;

    @Subscribe
    public void onInit(InitEvent event) {
        transactionReportsDc.getMutableItems().clear();
    }

    @Install(to = "transactionReportsDl", target = Target.DATA_LOADER)
    private List<TransactionReport> transactionReportsDlLoadDelegate(LoadContext<TransactionReport> loadContext) {
        transactionReportsDl.setMaxResults(50);
        Optional<FinancialYear> finOpt = Optional.ofNullable(financialYearDDL.getValue());
        Optional<QuartersEnum> quarterOpt = Optional.ofNullable(quarterDDL.getValue());
        TaxesUser user = users.getValue();
        Long accountNo=(user!=null)? user.getAccountNo():null;
        String branchCode=(branchesLF.getValue()!=null)?branchesLF.getValue().getId():null;
        UserTypeEnum userType =(user!=null)?UserTypeEnum.valueOf(user.getGroup().getName()):UserTypeEnum.BRANCH;


        if (quarterOpt.isPresent() && finOpt.isPresent()) {


            List result=this.iTransactionService.getReportTransactions(finOpt.get().getId(),quarterOpt.get().getId(),userType,branchCode,accountNo,transactionReportsDc.getView());
            if(result.size()==0)
                notifications.create().withType(Notifications.NotificationType.WARNING).withCaption(messages.getMainMessage("msg.noResult")).show();
            return result;
        }

        return new ArrayList<>();
    }

    @Install(to = "taxesUsersDl", target = Target.DATA_LOADER)
    private List<TaxesUser> taxesUsersDlLoadDelegate(LoadContext<TaxesUser> loadContext) {
        users.clear();
        LoadContext.Query query = loadContext.getQuery();
        Branch branch = branchesLF.getValue();
        if (branch != null) {
            query.setQueryString(" select e from TaxesUser e where e.branch.id= :branchId")
                    .setParameter("branchId", branch.getId());
            return dataManager.loadList(loadContext);
        }
        return new ArrayList<>();
    }


    @Subscribe("branchesLF")
    public void onBranchesLFValueChange(HasValue.ValueChangeEvent<Branch> event) {
        taxesUsersDl.load();

    }


    public void onSearchBtnClick() {

        Optional<QuartersEnum> quarterOptional = Optional.ofNullable(quarterDDL.getValue());
        Optional<FinancialYear> financialYearOptional = Optional.ofNullable(financialYearDDL.getValue());
        if (!quarterOptional.isPresent()) {
            notifications.create().withCaption(messages.getMainMessage("msg.error.quarter.required")).withHideDelayMs(100).show();
            return;
        }

        if (!financialYearOptional.isPresent()) {
            notifications.create().withCaption(messages.getMainMessage("msg.error.financialYear.required")).withHideDelayMs(100).show();

            return;
        }


        enableSearchForm(false);
        transactionReportsDl.load();

    }

    public void OnCancelBtnClick() {
        enableSearchForm(true);
    }

    private void enableSearchForm(boolean flag) {
        quarterDDL.setEnabled(flag);
        financialYearDDL.setEnabled(flag);
        searchBtn.setEnabled(flag);
        excelBtn.setEnabled(!flag);


    }



}