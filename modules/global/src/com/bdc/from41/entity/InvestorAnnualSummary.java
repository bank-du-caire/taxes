package com.bdc.from41.entity;

import com.bdc.from41.core.enums.QuartersEnum;
import com.bdc.from41.entity.embedded.InvestorAnnualSummaryId;
import com.haulmont.cuba.core.entity.BaseGenericIdEntity;
import com.haulmont.cuba.core.global.DbView;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@DbView
@Table(name = "Investor_Annual_summary")
@Entity(name = "InvestorAnnualSummary")
public class InvestorAnnualSummary extends BaseGenericIdEntity<InvestorAnnualSummaryId> {
    private static final long serialVersionUID = -6117736782654560605L;

    @EmbeddedId
    private InvestorAnnualSummaryId id;

    @Column(name = "TOTAL_PAID_AMOUNT")
    private BigDecimal totalPaidAmount;

    @Column(name = "total_commission")
    private BigDecimal totoalCommission;

    @Column(name = "QUARTER_ID")
    private Integer quarterId;

    @Column(name = "SUPPLY_NO")
    private Long suupplyNo;


    @Column(name = "BRANCH_CODE")
    private String branchCode;

    @Column(name = "BRANCH_LOCATION")
    private String branchLocation;

    public void setQuarterId(Integer quarterId) {
        this.quarterId = quarterId;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchLocation() {
        return branchLocation;
    }

    public void setBranchLocation(String branchLocation) {
        this.branchLocation = branchLocation;
    }

    public Long getSuupplyNo() {
        return suupplyNo;
    }

    public void setSuupplyNo(Long suupplyNo) {
        this.suupplyNo = suupplyNo;
    }

    public QuartersEnum getQuarterId() {
        return quarterId == null ? null : QuartersEnum.fromId(quarterId);
    }

    public void setQuarterId(QuartersEnum quarterId) {
        this.quarterId = quarterId == null ? null : quarterId.getId();
    }

    public BigDecimal getTotoalCommission() {
        return totoalCommission;
    }

    public void setTotoalCommission(BigDecimal totoalCommission) {
        this.totoalCommission = totoalCommission;
    }

    public BigDecimal getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(BigDecimal totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

    @Override
    public void setId(InvestorAnnualSummaryId id) {
        this.id = id;
    }

    @Override
    public InvestorAnnualSummaryId getId() {
        return id;
    }
}