package com.bdc.from41.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.annotation.Extends;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Digits;


@Entity(name = "TaxesUser")
@Extends(User.class)
@NamePattern("%s %s|name,accountNo")
public class TaxesUser extends User {


    @Lookup(type = LookupType.SCREEN, actions = "lookup")
    @ManyToOne
    @JoinColumn(name = "BRANCH_CODE")
    private Branch branch;

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }


    @Column(name = "ACCOUNT_NO")
    @Digits(message = "{msg://TaxesUser.accountNo.validation.Digits}", integer = 10, fraction = 0)
    private Long accountNo;

    public Long getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(Long accountNo) {
        this.accountNo = accountNo;
    }
}