package com.bdc.from41.entity.embedded;

import com.haulmont.chile.core.annotations.MetaClass;
import com.haulmont.cuba.core.entity.EmbeddableEntity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;
import java.util.UUID;

@MetaClass(name = "BranchAccountId")
@Embeddable
public class BranchUsersId extends EmbeddableEntity {
    private static final long serialVersionUID = 402762747294352091L;

    @Column(name = "BRANCH_CODE")
    private String branchCode;

    @Column(name = "USER_ID")
    private UUID userId;

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getUserId() {
        return userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(branchCode, userId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BranchUsersId entity = (BranchUsersId) o;
        return Objects.equals(this.branchCode, entity.branchCode) &&
                Objects.equals(this.userId, entity.userId);
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
}