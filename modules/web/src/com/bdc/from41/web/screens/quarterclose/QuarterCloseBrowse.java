package com.bdc.from41.web.screens.quarterclose;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.QuarterClose;

@UiController("QuarterClose.browse")
@UiDescriptor("quarter-close-browse.xml")
@LookupComponent("quarterClosesTable")
@LoadDataBeforeShow
public class QuarterCloseBrowse extends StandardLookup<QuarterClose> {
}