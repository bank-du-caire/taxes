package com.bdc.from41.web.screens.investor;

import com.bdc.from41.entity.*;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.screen.LookupComponent;
import com.haulmont.cuba.security.global.UserSession;
import com.haulmont.reports.app.service.ReportService;
import com.haulmont.reports.entity.Report;
import com.haulmont.reports.gui.actions.list.RunReportAction;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@UiController("from41_Investor_certifaction.browse")
@UiDescriptor("investor-certifacte.xml")
@LookupComponent("investorsTable")
@LoadDataBeforeShow
public class InvestorCertifacteBrowse extends StandardLookup<InvestorAnnualSummary> {


    @Inject
    private FlowBoxLayout searchParamtersPanel;
    @Inject
    private LookupPickerField<Investor> investorNameDLL;

    @Inject
    private LookupField<FinancialYear> financialYearDDL;
    @Inject
    private CollectionLoader<InvestorAnnualSummary> InvestorAnnualSummaryDl;
    @Inject
    private Notifications notifications;
    @Inject
    private Messages messages;
    @Inject
    private DataManager dataManager;
    @Inject
    private Button searchBtn;
    @Inject
    private Button printReportBtn;

    @Inject
    protected Actions actions;

    private TaxesUser user;
    @Inject
    private UserSession userSession;


    @Subscribe
    public void onInit(InitEvent event) {

        LoadContext<Report> lContext = new LoadContext<>(Report.class);




        user = (TaxesUser)  userSession.getUser();

        Action runReportAction = actions.create(RunReportAction.class, "runReport");
        printReportBtn.setAction(runReportAction);
    }
    private void enableSearchForm(boolean flag) {

        searchParamtersPanel.setEnabled(flag);
        printReportBtn.setEnabled(!flag);
        searchBtn.setEnabled(flag);

    }

    @Install(to = "InvestorAnnualSummaryDl", target = Target.DATA_LOADER)
    private List<InvestorAnnualSummary> investorAnnualSummaryDlLoadDelegate(LoadContext<InvestorAnnualSummary> loadContext)
        {
            InvestorAnnualSummaryDl.setMaxResults(50);

            LoadContext.Query query=loadContext.getQuery();
            Optional <FinancialYear> financialYearOptional=Optional.ofNullable(financialYearDDL.getValue());
            Optional <Investor> investorOptional=Optional.ofNullable(investorNameDLL.getValue());



            if(financialYearOptional.isPresent()&&financialYearOptional.isPresent()){
                String queryString="select e from InvestorAnnualSummary  e where 1=1  " +
                        "and e.id.financialYearId = :finId and e.id.investorId = :investorId" +
                        " and e.branchCode = :branch";
                query.setQueryString(queryString)
                        .setParameter("finId",financialYearOptional.get().getId())
                        .setParameter("investorId",investorOptional.get().getId())
                         .setParameter("branch",user.getBranch().getId());

                List result= dataManager.loadList(loadContext);
                if(result.size()==0)
                    notifications.create().withType(Notifications.NotificationType.WARNING).withCaption(messages.getMainMessage("msg.noResult")).show();
                return result;


            }
            return new ArrayList<>();
        }

    public void onSearchBtnClick() {

        Optional<Investor> quarterOptional = Optional.ofNullable(investorNameDLL.getValue());
        Optional<FinancialYear> financialYearOptional = Optional.ofNullable(financialYearDDL.getValue());
        if (!quarterOptional.isPresent()) {
            notifications.create().withCaption(messages.getMainMessage("msg.error.investor.required")).withHideDelayMs(100).show();
            return;
        }

        if (!financialYearOptional.isPresent()) {
            notifications.create().withCaption(messages.getMainMessage("msg.error.financialYear.required")).withHideDelayMs(100).show();

            return;
        }


        enableSearchForm(false);
        InvestorAnnualSummaryDl.load();

    }

    public void OnCancelBtnClick() {
        enableSearchForm(true);
    }
}