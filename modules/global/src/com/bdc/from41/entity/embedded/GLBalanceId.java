package com.bdc.from41.entity.embedded;

import com.haulmont.chile.core.annotations.MetaClass;
import com.haulmont.cuba.core.entity.EmbeddableEntity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@MetaClass(name = "GLBalanceId")
@Embeddable
public class GLBalanceId extends EmbeddableEntity {
    private static final long serialVersionUID = -1878678920050226667L;


        @Column(name = "FIN_YEAR", nullable = false)
        @NotNull
        private String finYear;

        @Column(name = "PERIOD_CODE", nullable = false)
        @NotNull
        private String periodCode;

        @Column(name = "BRANCH_CODE", nullable = false)
        @NotNull
        private String branchCode;

    public String getFinYear() {
        return finYear;
    }

    public void setFinYear(String finYear) {
        this.finYear = finYear;
    }

    public String getPeriodCode() {
        return periodCode;
    }

    public void setPeriodCode(String periodCode) {
        this.periodCode = periodCode;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(branchCode, finYear, periodCode);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GLBalanceId entity = (GLBalanceId) o;
        return Objects.equals(this.branchCode, entity.branchCode) &&
                Objects.equals(this.finYear, entity.finYear) &&
                Objects.equals(this.periodCode, entity.periodCode);
    }


}